<?php
	/* 
		Diese Funktion erzeugt eine neue Session oder setzt eine vorhandene Session fort.
		-----
		Diese Funktion erzeugt eine Session oder nimmt die aktuelle wieder auf, 
		die auf der Session-Kennung basiert,
		die mit einer GET- oder POST-Anfrage oder mit einem Cookie übermittelt wurde.
	*/
	session_start();

	/*
		Diese Funktion aktiviert die Ausgabepufferung. 
		-----
		Während die Ausgabepufferung aktiv ist werden Scriptausgaben
		(mit Ausnahme von Headerinformationen) nicht direkt an den Client weitergegeben,
		sondern in einem internen Puffer gesammelt. 
	*/
	ob_start();			
	
	/*
		Diese Funktion legt den Wert einer Konfigurationsoption fest.
		-----
		Diese Funktion legt den Wert der angegebenen Konfigurationsoption fest.
		Die Konfigurationsoption behält diesen neuen Wert während der Ausführung des Skripts bei
		und wird am Ende des Skripts wiederhergestellt.
	*/	
	ini_set('display_errors', 0);
	
	/*
		Diese Funktion gibt an, welche PHP-Fehlermeldungen gemeldet werden.
		-----
		Mit dieser Funktion wird die error_reporting Direktive zur Laufzeit des Programms gesetzt. 
		In PHP gibt es viele Stufen für die Anzeige von Fehlermeldungen, 
		die mit dieser Funktion für die Dauer der Programmausführung eingestellt werden kann. 
		Wenn das optionale level Argument nicht übergeben wird, 
		gibt error_reporting() das aktuelle Error Level zurück.
	*/
	error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
?>

<!doctype html>
<html>
	<?php
		/*
			Diese Funktion entspricht im Wesentlichen der Funktion "include",
			welche eine angegebene Datei einbindet und diese ausführt.
			Dabei werden Dateien unter dem angegebenen Pfad gesucht, oder, 
			wenn keiner gegeben ist, im include_path. 
			Wenn die Datei auch im include_path nicht gefunden werden kann, 
			sucht include noch in dem Verzeichnis der aufrufenden Datei
			und dem aktuellen Arbeitsverzeichnis. 
			Wenn keine Datei gefunden wurde, erzeugt include ein E_WARNING,
			im Gegensatz dazu erzeugt require in diesem Fall ein E_ERROR (E_COMPILE_ERROR).
			Das heißt, diese Funktion beendet also die Programmausführung,
			während include nur eine Warnung (E_WARNING) generiert 
			und so die weitere Programmausführung gestattet.
		*/
		require('database/database_login.php');
		require('content/layout/head.html');
		require('content/management.php');
		require('content/layout/body.php');
		require('database/database_logout.php');
	?>
</html>