-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für snowboard-webshop
DROP DATABASE IF EXISTS `snowboard-webshop`;
CREATE DATABASE IF NOT EXISTS `snowboard-webshop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `snowboard-webshop`;

-- Exportiere Struktur von Tabelle snowboard-webshop.administrator
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE IF NOT EXISTS `administrator` (
  `administrator_id` int(11) NOT NULL,
  `benutzer_id` int(11) NOT NULL,
  PRIMARY KEY (`administrator_id`,`benutzer_id`) USING BTREE,
  KEY `FK_administrator_benutzer` (`benutzer_id`) USING BTREE,
  CONSTRAINT `FK_administrator_benutzer` FOREIGN KEY (`benutzer_id`) REFERENCES `benutzer` (`benutzer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.administrator: ~4 rows (ungefähr)
/*!40000 ALTER TABLE `administrator` DISABLE KEYS */;
INSERT INTO `administrator` (`administrator_id`, `benutzer_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4);
/*!40000 ALTER TABLE `administrator` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.artikel
DROP TABLE IF EXISTS `artikel`;
CREATE TABLE IF NOT EXISTS `artikel` (
  `artikel_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_bezeichnung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `artikelkategorie_id` int(11) NOT NULL,
  PRIMARY KEY (`artikel_id`) USING BTREE,
  KEY `FK_artikel_artikelkategorie` (`artikelkategorie_id`) USING BTREE,
  CONSTRAINT `FK_artikel_artikelkategorie` FOREIGN KEY (`artikelkategorie_id`) REFERENCES `artikelkategorie` (`artikelkategorie_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.artikel: ~16 rows (ungefähr)
/*!40000 ALTER TABLE `artikel` DISABLE KEYS */;
INSERT INTO `artikel` (`artikel_id`, `artikel_bezeichnung`, `artikelkategorie_id`) VALUES
	(1, 'Snowboard_1', 4),
	(2, 'Snowboard_2', 7),
	(3, 'Snowboard X35 Rev. 2', 1),
	(4, 'Snowboard A12', 4),
	(5, 'DS 7', 2),
	(6, 'DS 8', 2),
	(9, 'RV-M', 4),
	(12, 'RV-W', 5),
	(13, 'RV-C', 6),
	(14, 'SB - Children Fun', 6),
	(15, 'KDR2 - Racing Edition', 7),
	(16, 'Elv. 4 Rev. 2', 8),
	(17, 'R45 - entry level', 9),
	(18, 'R45 - professional level', 9),
	(19, 'BTR - entry level', 10),
	(20, 'BTR - professional level', 10),
	(21, 'Zubehörset', 3);
/*!40000 ALTER TABLE `artikel` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.artikelkategorie
DROP TABLE IF EXISTS `artikelkategorie`;
CREATE TABLE IF NOT EXISTS `artikelkategorie` (
  `artikelkategorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikelkategorie_bezeichnung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `artikelkategorie_vorgänger` int(11) DEFAULT NULL,
  PRIMARY KEY (`artikelkategorie_id`) USING BTREE,
  KEY `FK_artikelkategorie_artikelkategorie` (`artikelkategorie_vorgänger`) USING BTREE,
  CONSTRAINT `FK_artikelkategorie_artikelkategorie` FOREIGN KEY (`artikelkategorie_vorgänger`) REFERENCES `artikelkategorie` (`artikelkategorie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.artikelkategorie: ~10 rows (ungefähr)
/*!40000 ALTER TABLE `artikelkategorie` DISABLE KEYS */;
INSERT INTO `artikelkategorie` (`artikelkategorie_id`, `artikelkategorie_bezeichnung`, `artikelkategorie_vorgänger`) VALUES
	(1, 'Normalboards', NULL),
	(2, 'Spezialboards', NULL),
	(3, 'Zubehör', NULL),
	(4, 'Männer', 1),
	(5, 'Frauen', 1),
	(6, 'Kinder', 1),
	(7, 'Racing', 2),
	(8, 'Freeride', 2),
	(9, 'Bindungen', 3),
	(10, 'Protektoren', 3);
/*!40000 ALTER TABLE `artikelkategorie` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.artikel_hat_attribut
DROP TABLE IF EXISTS `artikel_hat_attribut`;
CREATE TABLE IF NOT EXISTS `artikel_hat_attribut` (
  `artikel_id` int(11) NOT NULL,
  `attribut_id` int(11) NOT NULL,
  PRIMARY KEY (`artikel_id`,`attribut_id`) USING BTREE,
  KEY `FK_artikel_hat_attribut_attribut` (`attribut_id`),
  KEY `FK_artikel_hat_attribut_artikel` (`artikel_id`),
  CONSTRAINT `FK_artikel_hat_attribut_artikel` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`),
  CONSTRAINT `FK_artikel_hat_attribut_attribut` FOREIGN KEY (`attribut_id`) REFERENCES `attribut` (`attribut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.artikel_hat_attribut: ~80 rows (ungefähr)
/*!40000 ALTER TABLE `artikel_hat_attribut` DISABLE KEYS */;
INSERT INTO `artikel_hat_attribut` (`artikel_id`, `attribut_id`) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 4),
	(2, 5),
	(2, 6),
	(3, 7),
	(3, 8),
	(3, 9),
	(3, 10),
	(3, 11),
	(3, 12),
	(3, 19),
	(4, 13),
	(4, 14),
	(4, 15),
	(4, 16),
	(4, 17),
	(4, 18),
	(4, 20),
	(5, 21),
	(5, 22),
	(5, 23),
	(5, 24),
	(5, 25),
	(5, 26),
	(6, 27),
	(6, 28),
	(6, 29),
	(6, 30),
	(6, 31),
	(6, 32),
	(9, 33),
	(9, 34),
	(9, 35),
	(9, 36),
	(9, 37),
	(9, 38),
	(12, 39),
	(12, 40),
	(12, 41),
	(12, 42),
	(12, 43),
	(12, 44),
	(13, 45),
	(13, 46),
	(13, 47),
	(13, 48),
	(13, 49),
	(13, 50),
	(14, 51),
	(14, 52),
	(14, 53),
	(14, 54),
	(14, 55),
	(14, 56),
	(15, 57),
	(15, 58),
	(15, 59),
	(15, 60),
	(15, 61),
	(15, 62),
	(16, 63),
	(16, 64),
	(16, 65),
	(16, 66),
	(16, 67),
	(16, 68),
	(17, 69),
	(17, 70),
	(17, 71),
	(18, 72),
	(18, 73),
	(18, 74),
	(19, 75),
	(19, 76),
	(19, 77),
	(20, 78),
	(20, 79),
	(20, 80),
	(21, 81),
	(21, 82),
	(21, 83);
/*!40000 ALTER TABLE `artikel_hat_attribut` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.attribut
DROP TABLE IF EXISTS `attribut`;
CREATE TABLE IF NOT EXISTS `attribut` (
  `attribut_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribut_bezeichnung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `attribut_einheit` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `attribut_wert` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`attribut_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.attribut: ~83 rows (ungefähr)
/*!40000 ALTER TABLE `attribut` DISABLE KEYS */;
INSERT INTO `attribut` (`attribut_id`, `attribut_bezeichnung`, `attribut_einheit`, `attribut_wert`) VALUES
	(1, 'Preis', '300,00', '300,00'),
	(2, 'Länge', 'Zentimeter', '160'),
	(3, 'Fahrlevel', NULL, 'Beginner'),
	(4, 'Preis', 'Euro', '500,00'),
	(5, 'Länge', 'Zentimeter', '150'),
	(6, 'Fahrlevel', NULL, 'Advanced'),
	(7, 'Preis', 'Euro', '649,99'),
	(8, 'Beschreibung', NULL, 'Das Board für den echten Profi! Hier bleiben wirklich keine wünsche offen. Aufgrund der extra starken Kantenschleifung ist das Board bestens für schnelle Manöver geschaffen und richtet sich ganz klar an Profis!'),
	(9, 'Länge', 'Zentimeter', '150'),
	(10, 'Breite', 'Zentimeter', '20'),
	(11, 'Muster', NULL, 'ROXY'),
	(12, 'Bild', NULL, 'images/snowboard_eins.jpg'),
	(13, 'Preis', 'Euro', '99,99'),
	(14, 'Beschreibung', NULL, 'Bei diesem Board handelt es sich um unseren beliebten Klassiker für Einsteiger und Allrounder. Das Board ist besonders Robust und so ausgelegt das es möglichst viele Fehler während der Fahrt verzeiht. Unsere Klassiker schlechthin für Neueinsteiger!'),
	(15, 'Länge', 'Zentimeter', '140'),
	(16, 'Breite', 'Zentimeter', '30'),
	(17, 'Muster', NULL, 'solid blue'),
	(18, 'Bild', NULL, 'images/snowboard_zwei.jpg'),
	(19, 'Beschreibung_Übersicht', NULL, 'Das Profi-Snowboard für echte Kerle!'),
	(20, 'Beschreibung_Übersicht', NULL, 'Gutes Board, welches gut für Einsteiger und Allrounder geeignet ist.'),
	(21, 'Preis', 'Euro', '499'),
	(22, 'Beschreibung', NULL, 'Spezialboard - besonders geeignet für Kurvenreiches fahren\r\n'),
	(23, 'Länge', 'Zentimeter', '160'),
	(24, 'Breite', 'Zentimeter', '25'),
	(25, 'Muster', NULL, 'Tarnfarben'),
	(26, 'Beschreibung_Übersicht', NULL, 'Dieses Board richtet sich an Kunden welche besonders gerne Engstellen befahren möchten. Durch die besondere Beschaffung des Boards ist es möglich, viele schnelle und enge Kurven zu fahren.'),
	(27, 'Preis', 'Euro', '899'),
	(28, 'Beschreibung', NULL, 'Spezialboard - besonders geeignet für das Fahren in Tiefschnee'),
	(29, 'Länge', 'Zentimeter', '160'),
	(30, 'Breite', 'Zentimeter', '25'),
	(31, 'Muster', NULL, 'Carmoflage'),
	(32, 'Beschreibung_Übersicht', NULL, 'Dieses Board lässt bei Tiefschneeträumen wirklich keine Wünsche offen, egal wie tief der Schnee ist. Mit diesem Board werden Sie auf jeden Fall viel Spaß haben.'),
	(33, 'Preis', 'Euro', '549'),
	(34, 'Beschreibung', NULL, 'Männerboard für wilde Abfahrten'),
	(35, 'Länge', 'Zentimeter', '170'),
	(36, 'Breite', 'Zentimeter', '35'),
	(37, 'Muster', NULL, 'Weißbier'),
	(38, 'Beschreibung_Übersicht', NULL, 'Hierbei handelt es sich um ein Board für alle Bedürfnisse - also einen klassischen Allrounder. Aufgrund des Musters ist dieses Board jedoch besonders attraktiv für die männlichen Fahrer.'),
	(39, 'Preis', 'Euro', '549'),
	(40, 'Beschreibung', NULL, 'Stylisches Designerboard'),
	(41, 'Länge', 'Zentimeter', '170'),
	(42, 'Breite', 'Zentimeter', '35'),
	(43, 'Muster', NULL, 'Moderne Kunst'),
	(44, 'Beschreibung_Übersicht', NULL, 'Dieses Board ist vor allem eines - eine wahre Augenweide. Wer neben dem eigentlichen Fahren noch viel Wert auf Optik legt ist mit diesem Board bestens bedient!'),
	(45, 'Preis', 'Euro', '299'),
	(46, 'Beschreibung', NULL, 'Ducktales-Board'),
	(47, 'Länge', 'Zentimeter', '120'),
	(48, 'Breite', 'Zentimeter', '20'),
	(49, 'Muster', NULL, 'Ducktales'),
	(50, 'Beschreibung_Übersicht', NULL, 'Ein ganz klassisches Kinderboard für Einsteiger. Das Board verzeiht Fehler und sieht für Kinder sehr ansprechend aus.'),
	(51, 'Preis', 'Euro', '499'),
	(52, 'Beschreibung', NULL, 'Kunderbuntes Kinderboard für Pistenwütige'),
	(53, 'Länge', 'Zentimeter', '120'),
	(54, 'Breite', 'Zentimeter', '20'),
	(55, 'Muster', NULL, 'Kunderbunt'),
	(56, 'Beschreibung_Übersicht', NULL, 'Dieses Board ist für Kinder designed und konzipiert. Es richtet sich hierbei jedoch an schon etwas erfahrenere Kinder.'),
	(57, 'Preis', 'Euro', '1299'),
	(58, 'Beschreibung', NULL, 'Süchtig nach Adrenalin? Dann ist dieses Snowboard genau was du suchst!'),
	(59, 'Länge', 'Zentimeter', '180'),
	(60, 'Breite', 'Zentimeter', '35'),
	(61, 'Muster', NULL, 'Flammen'),
	(62, 'Beschreibung_Übersicht', NULL, 'Wer kennt das nicht? Den Adrenalinrausch wenn man die Piste ins Tal hinab schießt und dabei unglaubliche Geschwindigkeiten aufnimmt. Immer schneller als die anderen, immer auf der Überholspur. Mit diesem Board holst du wirklich das allerletzte aus dem Schnee und aus dir! Achtung - nichts für schwache Nerven!'),
	(63, 'Preis', 'Euro', '999'),
	(64, 'Beschreibung', NULL, 'Freeride ist dein Leben? Dann schau dir das Board an!'),
	(65, 'Länge', 'Zentimeter', '180'),
	(66, 'Breite', 'Zentimeter', '35'),
	(67, 'Muster', NULL, 'Wolken'),
	(68, 'Beschreibung_Übersicht', NULL, 'Dieses Snowboard wurde explizit an die Wünsche und Anforderungen von Freeridern entworfen und entwickelt. Hier bleibt wirklich kein Wunsch offen.'),
	(69, 'Preis', 'Euro', '49,99'),
	(70, 'Beschreibung', NULL, 'Wer kennt Sie nicht, die R45 Bindung?'),
	(71, 'Beschreibung_Übersicht', NULL, 'Der absolute Klassiker aller Bindungen, die R45. Der defakte Standard am Bindungsmarkt findet sich quasi an alles Boards jeglicher Beschaffenheit - hier kann man einfach nichts falsch machen.'),
	(72, 'Preis', 'Euro', '89,99'),
	(73, 'Beschreibung', NULL, 'Die R45 - Profi Variante'),
	(74, 'Beschreibung_Übersicht', NULL, 'Die Piste ödet dich an? Du springst lieber aus dem Helikopter und nimmst den Berg wie er ist? Dann bist du mit dieser Bindung an der richtigen Adresse! Die Klassische R45 mit extrem robusten Materialien ist ein Begleiter in allen extremen Situationen!'),
	(75, 'Preis', 'Euro', '199'),
	(76, 'Beschreibung', NULL, 'Waghalsig ist dein zweiter Vorname? Dann solltest du an deinen Schutz denken!'),
	(77, 'Beschreibung_Übersicht', NULL, 'Dir kann es nicht riskant und schnell genug Bergab gehen? Du gehst gerne an deine Fahrerischen Grenzen und darüber hinaus? Dann solltest du an deinen Schutz denken! Mit diese Protektoren brauchst du beim Brettern keine Gedanken mehr um Verletzungen machen. Denk nur noch an die Abfahrt - du hast es dir verdient!'),
	(78, 'Preis', 'Euro', '499'),
	(79, 'Beschreibung', NULL, 'Abseits der Piste ist dein Leben? Dann schütze es!'),
	(80, 'Beschreibung_Übersicht', NULL, 'Dieses Protektoren-Paket lässt keine Wünsche offen! Egal ob auf der Piste oder Abseits - mit diesem Paket hat meinen seinen Schutzengel integriert immer dabei!'),
	(81, 'Preis', 'Euro', '20'),
	(82, 'Beschreibung', NULL, 'Das umfassend Zubehörset hilft nicht auf bei der Pflege und Wartung, sondern auch auf der Piste!'),
	(83, 'Beschreibung_Übersicht', NULL, 'Das allumfassende Zubehörset!');
/*!40000 ALTER TABLE `attribut` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.benutzer
DROP TABLE IF EXISTS `benutzer`;
CREATE TABLE IF NOT EXISTS `benutzer` (
  `benutzer_id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzer_anrede` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_vorname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_nachname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_benutzername_unverschlüsselt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_benutzername` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_passwort` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_email_adresse` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_geburtsdatum` date DEFAULT NULL,
  PRIMARY KEY (`benutzer_id`) USING BTREE,
  UNIQUE KEY `benutzer_benutzername` (`benutzer_benutzername`),
  UNIQUE KEY `benutzer_benutzername_unverschlüsselt` (`benutzer_benutzername_unverschlüsselt`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.benutzer: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `benutzer` DISABLE KEYS */;
INSERT INTO `benutzer` (`benutzer_id`, `benutzer_anrede`, `benutzer_vorname`, `benutzer_nachname`, `benutzer_benutzername_unverschlüsselt`, `benutzer_benutzername`, `benutzer_passwort`, `benutzer_email_adresse`, `benutzer_geburtsdatum`) VALUES
	(1, 'Herr', 'Florian', 'Jänisch', 'jaenischf.tin18', '5df1c13572391052ee26f18fb44fff2d', '25d55ad283aa400af464c76d713c07ad', 'jaenischf.tin18@student.dhbw-heidenheim.de', NULL),
	(2, 'Herr', 'Andreas', 'Greiff', 'greiffa.tin18', '3051703c88fa8599ef37a3e87011589f', '25d55ad283aa400af464c76d713c07ad', 'greiffa.tin18@student.dhbw-heidenheim.de', NULL),
	(3, 'Herr', 'Philipp', 'Kindermann', 'kindermannp.tin18', 'a5122aec33e6d8e87f2e2de78b433504', '25d55ad283aa400af464c76d713c07ad', 'kindermannp.tin18@student.dhbw-heidenheim.de', NULL),
	(4, 'Herr', 'Enes', 'Sen', 'sene.tin18', 'ddc4e23dc0dfd48c4e5c7b220eb147e5', '25d55ad283aa400af464c76d713c07ad', 'sene.tin18@student.dhbw-heidenheim.de', NULL),
	(5, 'Herr', 'Herrmann', 'Dummy', 'dummy_herrmann', 'aa4f2f2f728109fef7b78d71b15ddad9', '25d55ad283aa400af464c76d713c07ad', 'dummy.herrmann@snowboarder.de', '2012-12-12');
/*!40000 ALTER TABLE `benutzer` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.benutzer_adresse
DROP TABLE IF EXISTS `benutzer_adresse`;
CREATE TABLE IF NOT EXISTS `benutzer_adresse` (
  `benutzer_adresse_id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzer_id` int(11) NOT NULL,
  `benutzer_adresse_bezeichnung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_adresse_straße` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_adresse_hausnummer` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_adresse_postleitzahl` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_adresse_ortschaft` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `benutzer_adresse_land` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`benutzer_adresse_id`) USING BTREE,
  KEY `FK_benutzer_adresse_benutzer` (`benutzer_id`),
  CONSTRAINT `FK_benutzer_adresse_benutzer` FOREIGN KEY (`benutzer_id`) REFERENCES `benutzer` (`benutzer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- Exportiere Daten aus Tabelle snowboard-webshop.benutzer_adresse: ~11 rows (ungefähr)
/*!40000 ALTER TABLE `benutzer_adresse` DISABLE KEYS */;
INSERT INTO `benutzer_adresse` (`benutzer_adresse_id`, `benutzer_id`, `benutzer_adresse_bezeichnung`, `benutzer_adresse_straße`, `benutzer_adresse_hausnummer`, `benutzer_adresse_postleitzahl`, `benutzer_adresse_ortschaft`, `benutzer_adresse_land`) VALUES
	(1, 1, 'Privat', 'Bleibtreustraße', '24', '97009', 'Würzburg', 'Deutschland'),
	(2, 1, 'Geschäftlich', 'Schneestraße', '12', '97009', 'Würzburg', 'Deutschland'),
	(3, 2, 'Privat', 'Kantstrasse', '29', '95406', 'Bayreuth', 'Deutschland'),
	(4, 2, 'Geschäftlich', 'Schillerstrasse', '77', '82055', 'Icking', 'Deutschland'),
	(5, 3, 'Privat', 'Kieler Strasse', '3', '84550', 'Feichten', 'Deutschland'),
	(6, 3, 'Geschäftlich', 'Rudolstaedter Strasse', '88', '63778', 'Obernburg', 'Deutschland'),
	(7, 4, 'Privat', 'Koepenicker Str.', '15', '56357', 'Oelsberg', 'Deutschland'),
	(8, 4, 'Geschäftlich', 'Rudower Chaussee', '60', '96030', 'Bamberg', 'Deutschland'),
	(9, 5, 'Privat', 'Schönwalder Allee', '61', '24783', 'Osterrönfeld', 'Deutschland'),
	(10, 5, 'Geschäftlich', 'Chausseestr.', '51', '22902', 'Ahrensburg', 'Deutschland');
/*!40000 ALTER TABLE `benutzer_adresse` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.benutzer_hat_verwendet_passwort
DROP TABLE IF EXISTS `benutzer_hat_verwendet_passwort`;
CREATE TABLE IF NOT EXISTS `benutzer_hat_verwendet_passwort` (
  `benutzer_verwendetes_passwort_id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzer_id` int(11) NOT NULL,
  `benutzer_verwendetes_passwort` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`benutzer_verwendetes_passwort_id`) USING BTREE,
  KEY `FK_benutzer_hat_verwendet_passwort_benutzer` (`benutzer_id`) USING BTREE,
  CONSTRAINT `FK_benutzer_hat_verwendet_passwort_benutzer` FOREIGN KEY (`benutzer_id`) REFERENCES `benutzer` (`benutzer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Exportiere Daten aus Tabelle snowboard-webshop.benutzer_hat_verwendet_passwort: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `benutzer_hat_verwendet_passwort` DISABLE KEYS */;
INSERT INTO `benutzer_hat_verwendet_passwort` (`benutzer_verwendetes_passwort_id`, `benutzer_id`, `benutzer_verwendetes_passwort`) VALUES
	(1, 1, '25d55ad283aa400af464c76d713c07ad'),
	(2, 2, '25d55ad283aa400af464c76d713c07ad'),
	(3, 3, '25d55ad283aa400af464c76d713c07ad'),
	(4, 4, '25d55ad283aa400af464c76d713c07ad'),
	(5, 5, '25d55ad283aa400af464c76d713c07ad');
/*!40000 ALTER TABLE `benutzer_hat_verwendet_passwort` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.benutzer_login_details
DROP TABLE IF EXISTS `benutzer_login_details`;
CREATE TABLE IF NOT EXISTS `benutzer_login_details` (
  `benutzer_id` int(11) NOT NULL,
  `benutzer_aktivierung` int(1) NOT NULL DEFAULT 0,
  `benutzer_aktivierungscode` varchar(50) DEFAULT NULL,
  `benutzer_aktivierungscode_gueltigkeitsdatum` varchar(10) DEFAULT NULL,
  `benutzer_aktivierungscode_gueltigkeitsuhrzeit` varchar(10) DEFAULT NULL,
  `benutzer_verbleibende_anmeldeversuche` int(2) DEFAULT 10,
  `benutzer_status` tinyint(1) DEFAULT NULL,
  `benutzer_letzte_anmeldung_datum` varchar(10) DEFAULT NULL,
  `benutzer_letzte_anmeldung_uhrzeit` varchar(10) DEFAULT NULL,
  `benutzer_login_datum` varchar(10) DEFAULT NULL,
  `benutzer_login_uhrzeit` varchar(10) DEFAULT NULL,
  `benutzer_logout_datum` varchar(10) DEFAULT NULL,
  `benutzer_logout_uhrzeit` varchar(10) DEFAULT NULL,
  `benutzer_letzte_seite` varchar(50) DEFAULT 'white',
  PRIMARY KEY (`benutzer_id`) USING BTREE,
  KEY `FK_benutzer_login_details_benutzer` (`benutzer_id`) USING BTREE,
  CONSTRAINT `FK_benutzer_login_details_benutzer` FOREIGN KEY (`benutzer_id`) REFERENCES `benutzer` (`benutzer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle snowboard-webshop.benutzer_login_details: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `benutzer_login_details` DISABLE KEYS */;
INSERT INTO `benutzer_login_details` (`benutzer_id`, `benutzer_aktivierung`, `benutzer_aktivierungscode`, `benutzer_aktivierungscode_gueltigkeitsdatum`, `benutzer_aktivierungscode_gueltigkeitsuhrzeit`, `benutzer_verbleibende_anmeldeversuche`, `benutzer_status`, `benutzer_letzte_anmeldung_datum`, `benutzer_letzte_anmeldung_uhrzeit`, `benutzer_login_datum`, `benutzer_login_uhrzeit`, `benutzer_logout_datum`, `benutzer_logout_uhrzeit`, `benutzer_letzte_seite`) VALUES
	(1, 0, NULL, NULL, NULL, 10, 0, '01.06.2020', '13:28:43', '01.06.2020', '13:26:35', '01.06.2020', '13:28:43', 'user_home'),
	(2, 0, NULL, NULL, NULL, 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 0, NULL, NULL, NULL, 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 0, NULL, NULL, NULL, 10, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 0, NULL, NULL, NULL, 10, 0, '27.05.2020', '13:56:52', '27.05.2020', '13:56:07', '27.05.2020', '13:56:52', 'user_home');
/*!40000 ALTER TABLE `benutzer_login_details` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.bestellung
DROP TABLE IF EXISTS `bestellung`;
CREATE TABLE IF NOT EXISTS `bestellung` (
  `bestellung_id` int(11) NOT NULL AUTO_INCREMENT,
  `kunde_id` int(11) DEFAULT NULL,
  `benutzer_adresse_id` int(11) DEFAULT NULL,
  `bezahlungsart_id` int(11) DEFAULT NULL,
  `bestellung_bestellungswert` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestellung_datum` date DEFAULT NULL,
  `bestellung_uhrzeit` time DEFAULT NULL,
  `bestellung_bezahlung_erfolgreich` tinyint(1) DEFAULT 0,
  `paypal_payment_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_payer_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`bestellung_id`),
  KEY `FK_bestellung_kunde` (`kunde_id`),
  KEY `FK_bestellung_bezahlungsart_id` (`bezahlungsart_id`),
  KEY `FK_bestellung_benutzer_adresse` (`benutzer_adresse_id`),
  CONSTRAINT `FK_bestellung_benutzer_adresse` FOREIGN KEY (`benutzer_adresse_id`) REFERENCES `benutzer_adresse` (`benutzer_adresse_id`),
  CONSTRAINT `FK_bestellung_bezahlungsart` FOREIGN KEY (`bezahlungsart_id`) REFERENCES `bezahlungsart` (`bezahlungsart_id`),
  CONSTRAINT `FK_bestellung_kunde` FOREIGN KEY (`kunde_id`) REFERENCES `kunde` (`kunde_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.bestellung: ~17 rows (ungefähr)
/*!40000 ALTER TABLE `bestellung` DISABLE KEYS */;
INSERT INTO `bestellung` (`bestellung_id`, `kunde_id`, `benutzer_adresse_id`, `bezahlungsart_id`, `bestellung_bestellungswert`, `bestellung_datum`, `bestellung_uhrzeit`, `bestellung_bezahlung_erfolgreich`, `paypal_payment_id`, `paypal_token`, `paypal_payer_id`) VALUES
	(1, 3, NULL, 1, '99.99', '2020-05-28', '11:43:48', 0, 'PAYID-L3HYPQQ7EP687393E393730A', 'EC-99603440FL691002W', 'D5KC4FDDTDCT2'),
	(2, 1, 1, 2, '649.99', '2020-05-29', '16:59:12', 1, 'PAYID-KEFRWBXTJSAQ7MWUPJDAKEPZ', 'EC-billbuy', 'buyer'),
	(3, 1, 2, 2, '2297', '2020-05-29', '17:51:51', 1, 'PAYID-74XUJGE1IB9W583ROPUY42NG', 'EC-billbuy', 'buyer'),
	(4, 5, NULL, 1, '2146', '2020-05-29', '22:47:45', 0, 'PAYID-L3IXJZQ3DN76924V35492108', 'EC-5JR99315U74011222', 'D5KC4FDDTDCT2'),
	(5, 5, 2, 2, '5490', '2020-05-29', '22:49:27', 0, 'PAYID-Q3Y8OEYTVVZGZTG5SU69JSGD', 'EC-billbuy', 'buyer'),
	(10, 1, NULL, 3, '49.99', '2020-05-30', '14:09:40', 1, 'PAYID-QUO7M3VK7UY9ORP5OVSXHS2L', 'BAR-buy', 'buyer'),
	(11, 1, NULL, 1, '2697.98', '2020-05-30', '14:15:25', 0, 'PAYID-L3JE4GQ4HK35681WM8112106', 'EC-2R426921KV294020B', 'D5KC4FDDTDCT2'),
	(12, 1, 2, 2, '3193.98', '2020-05-30', '14:17:21', 1, 'PAYID-GKYR9EJ0H8XN9J50C118VB2K', 'EC-billbuy', 'buyer'),
	(13, 1, NULL, 3, '2117', '2020-05-30', '14:18:58', 0, 'PAYID-MYOETR3BNNTOMZRLX6D3T4BU', 'BAR-buy', 'buyer'),
	(15, 1, NULL, 1, '1299', '2020-05-30', '14:25:24', 1, 'PAYID-L3JFBJI79Y24774TV166135X', 'EC-9YL55854TP046432K', 'D5KC4FDDTDCT2'),
	(16, 1, NULL, 1, '1647.99', '2020-05-30', '15:53:49', 1, 'PAYID-L3JGKQQ32K39782RR186003Y', 'EC-7168840575586852X', 'D5KC4FDDTDCT2'),
	(17, 1, 2, 2, '8291.97', '2020-05-30', '16:01:26', 1, 'PAYID-6IAE1KVSAZ45BT125UPE50OF', 'EC-billbuy', 'buyer'),
	(18, 1, NULL, 3, '2647.99', '2020-05-30', '16:08:30', 0, 'PAYID-CEOLD7M2IE0JN825WWCJNYA9', 'BAR-buy', 'buyer'),
	(19, 1, NULL, 3, '2647.99', '2020-05-30', '16:12:28', 0, 'PAYID-ZXELQ72H6N4TVSCXSKRZ9NHU', 'BAR-buy', 'buyer'),
	(20, 1, NULL, 1, '2097.99', '2020-05-30', '16:31:06', 1, 'PAYID-L3JG4DA1P537166597632543', 'EC-8WP393660E815142D', 'D5KC4FDDTDCT2'),
	(21, 1, 2, 2, '2097.99', '2020-05-30', '16:34:02', 1, 'PAYID-LNJZ4RG1XEV680WTMR14Q3LW', 'EC-billbuy', 'buyer'),
	(22, 1, NULL, 3, '2097.99', '2020-05-30', '16:35:14', 1, 'PAYID-U1QYCPZPOYYYHMXT4Y5EICDH', 'BAR-buy', 'buyer'),
	(23, 1, NULL, 3, '649.99', '2020-06-01', '13:20:54', 0, 'PAYID-B0PXG94JUKYK8MBUCEG5E2HT', 'BAR-buy', 'buyer');
/*!40000 ALTER TABLE `bestellung` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.bestellung_hat_artikel
DROP TABLE IF EXISTS `bestellung_hat_artikel`;
CREATE TABLE IF NOT EXISTS `bestellung_hat_artikel` (
  `bestellung_id` int(11) NOT NULL,
  `artikel_id` int(11) NOT NULL,
  `bestellung_hat_artikel_menge` int(11) NOT NULL,
  PRIMARY KEY (`artikel_id`,`bestellung_id`) USING BTREE,
  KEY `FK_bestellung_hat_artikel_bestellung` (`bestellung_id`),
  KEY `FK_bestellung_hat_artikel_artikel` (`artikel_id`),
  CONSTRAINT `FK_bestellung_hat_artikel_artikel` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`artikel_id`),
  CONSTRAINT `FK_bestellung_hat_artikel_bestellung` FOREIGN KEY (`bestellung_id`) REFERENCES `bestellung` (`bestellung_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.bestellung_hat_artikel: ~19 rows (ungefähr)
/*!40000 ALTER TABLE `bestellung_hat_artikel` DISABLE KEYS */;
INSERT INTO `bestellung_hat_artikel` (`bestellung_id`, `artikel_id`, `bestellung_hat_artikel_menge`) VALUES
	(2, 3, 1),
	(23, 3, 1),
	(12, 4, 2),
	(17, 4, 3),
	(21, 4, 1),
	(22, 4, 1),
	(5, 9, 10),
	(13, 12, 2),
	(3, 14, 2),
	(12, 14, 6),
	(3, 15, 1),
	(18, 15, 2),
	(19, 15, 2),
	(13, 16, 1),
	(17, 16, 8),
	(21, 16, 2),
	(22, 16, 2),
	(10, 17, 1),
	(18, 17, 1),
	(19, 17, 1),
	(13, 21, 1);
/*!40000 ALTER TABLE `bestellung_hat_artikel` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.bezahlungsart
DROP TABLE IF EXISTS `bezahlungsart`;
CREATE TABLE IF NOT EXISTS `bezahlungsart` (
  `bezahlungsart_id` int(11) NOT NULL AUTO_INCREMENT,
  `bezahlungsart_bezeichnung` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`bezahlungsart_id`) USING BTREE,
  UNIQUE KEY `bezahlungsart_bezeichnung` (`bezahlungsart_bezeichnung`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.bezahlungsart: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `bezahlungsart` DISABLE KEYS */;
INSERT INTO `bezahlungsart` (`bezahlungsart_id`, `bezahlungsart_bezeichnung`) VALUES
	(3, 'Barzahlung'),
	(1, 'Paypal'),
	(2, 'Rechnung');
/*!40000 ALTER TABLE `bezahlungsart` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.kunde
DROP TABLE IF EXISTS `kunde`;
CREATE TABLE IF NOT EXISTS `kunde` (
  `kunde_id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzer_id` int(11) NOT NULL,
  PRIMARY KEY (`kunde_id`) USING BTREE,
  KEY `FK_kunde_benutzer` (`benutzer_id`) USING BTREE,
  CONSTRAINT `FK_kunde_benutzer` FOREIGN KEY (`benutzer_id`) REFERENCES `benutzer` (`benutzer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Exportiere Daten aus Tabelle snowboard-webshop.kunde: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `kunde` DISABLE KEYS */;
INSERT INTO `kunde` (`kunde_id`, `benutzer_id`) VALUES
	(1, 1),
	(2, 2),
	(3, 3),
	(4, 4),
	(5, 5);
/*!40000 ALTER TABLE `kunde` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.menü
DROP TABLE IF EXISTS `menü`;
CREATE TABLE IF NOT EXISTS `menü` (
  `menü_id` int(11) NOT NULL AUTO_INCREMENT,
  `menü_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menü_menüstartpunkt_id` int(11) DEFAULT NULL,
  `menü_modus` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menü_menüleiste` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menü_menüleiste_position` int(11) DEFAULT NULL,
  PRIMARY KEY (`menü_id`) USING BTREE,
  KEY `FK_menü_menüpunkte` (`menü_menüstartpunkt_id`),
  CONSTRAINT `FK_menü_menüpunkte` FOREIGN KEY (`menü_menüstartpunkt_id`) REFERENCES `menüpunkt` (`menüpunkt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.menü: ~12 rows (ungefähr)
/*!40000 ALTER TABLE `menü` DISABLE KEYS */;
INSERT INTO `menü` (`menü_id`, `menü_name`, `menü_menüstartpunkt_id`, `menü_modus`, `menü_menüleiste`, `menü_menüleiste_position`) VALUES
	(1, 'Test-Menü', 1, 'dropdown', 'links', 2),
	(2, 'Menü', 8, 'dropdown', 'links', 3),
	(3, 'Einstellungen', 16, 'dropdown', 'rechts', 1),
	(4, 'Profil', 20, 'presentaation', 'rechts', 3),
	(5, 'Home', 21, 'presentation', 'rechts', 4),
	(6, 'Sonstiges', 22, 'dropdown', 'links', 8),
	(7, 'Entwicklung', 24, 'dropdown', 'links', 1),
	(8, 'Warenkorb <b id="warenkorb_anzahl"></b>', 27, 'presentation', 'rechts', 2),
	(9, 'Normalboards', 28, 'dropdown', 'links', 4),
	(10, 'Spezialboards', 29, 'dropdown', 'links', 5),
	(11, 'Zubehör', 30, 'dropdown', 'links', 6),
	(12, 'Administration', 43, 'dropdown', 'links', 7);
/*!40000 ALTER TABLE `menü` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.menüpunkt
DROP TABLE IF EXISTS `menüpunkt`;
CREATE TABLE IF NOT EXISTS `menüpunkt` (
  `menüpunkt_id` int(11) NOT NULL AUTO_INCREMENT,
  `menüpunkt_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menüpunkt_interne_route` tinyint(1) DEFAULT NULL,
  `menüpunkt_interne_referenz` int(11) DEFAULT NULL,
  `menüpunkt_interne_referenz_ergänzung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menüpunkt_externe_referenz` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `menüpunkt_aktiv` tinyint(1) DEFAULT 0,
  `menüpunkt_nur_bei_anmeldung` tinyint(1) DEFAULT 0,
  `menüpunkt_nur_ohne_anmeldung` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`menüpunkt_id`),
  KEY `FK_menüpunkte_route` (`menüpunkt_interne_referenz`),
  CONSTRAINT `FK_menüpunkte_route` FOREIGN KEY (`menüpunkt_interne_referenz`) REFERENCES `route` (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.menüpunkt: ~40 rows (ungefähr)
/*!40000 ALTER TABLE `menüpunkt` DISABLE KEYS */;
INSERT INTO `menüpunkt` (`menüpunkt_id`, `menüpunkt_name`, `menüpunkt_interne_route`, `menüpunkt_interne_referenz`, `menüpunkt_interne_referenz_ergänzung`, `menüpunkt_externe_referenz`, `menüpunkt_aktiv`, `menüpunkt_nur_bei_anmeldung`, `menüpunkt_nur_ohne_anmeldung`) VALUES
	(1, 'Startseite', 1, 6, NULL, NULL, 0, 1, 1),
	(2, 'Startseite_A', 1, 6, NULL, NULL, 1, 1, 1),
	(3, 'Startseite_B', 1, 6, NULL, NULL, 1, 1, 1),
	(4, 'Startseite_A_a', 1, 6, NULL, NULL, 1, 1, 1),
	(5, 'Startseite_A_b', 1, 6, NULL, NULL, 1, 1, 1),
	(6, 'Startseite_A_a_1', 1, 6, NULL, NULL, 1, 1, 1),
	(7, 'Startseite_C', 0, NULL, NULL, 'https://www.google.de', 1, 1, 1),
	(8, 'Menü', 1, 1, NULL, NULL, 1, 1, 1),
	(9, 'Startseite', 1, 6, NULL, NULL, 1, 1, 1),
	(10, 'Wir über uns', 1, 7, NULL, NULL, 1, 1, 1),
	(11, 'Links', 1, 8, NULL, NULL, 1, 1, 1),
	(12, 'Login', 1, 5, NULL, NULL, 1, 0, 1),
	(13, 'Logout', 1, 5, NULL, NULL, 1, 1, 0),
	(14, 'Passwort vergessen', 1, 9, NULL, NULL, 1, 0, 1),
	(15, 'Funktioniert\'s nicht?', 1, 10, NULL, NULL, 0, 1, 1),
	(16, 'Einstellungen', 1, 11, NULL, NULL, 1, 1, 0),
	(17, 'Hinweise', 1, 11, NULL, NULL, 1, 1, 0),
	(18, 'Passwort ändern', 1, 12, NULL, NULL, 1, 1, 0),
	(19, 'Email-Adresse ändern', 1, 13, NULL, NULL, 0, 1, 0),
	(20, 'Profil', 1, 18, NULL, NULL, 1, 1, 0),
	(21, 'Home', 1, 17, NULL, NULL, 1, 1, 0),
	(22, 'Sonstiges', 1, 19, NULL, NULL, 1, 1, 0),
	(23, 'Hinweise', 1, 19, NULL, NULL, 1, 1, 0),
	(24, 'Entwicklung', 1, 1, NULL, NULL, 0, 1, 0),
	(25, 'Entwicklung-Test-A', 1, 1, NULL, NULL, 1, 1, 0),
	(26, 'Entwicklung-Test-B', 1, 1, NULL, NULL, 1, 1, 0),
	(27, 'Warenkorb', 1, 23, NULL, NULL, 1, 1, 0),
	(28, 'Normalboards', 1, NULL, NULL, NULL, 1, 1, 0),
	(29, 'Spezialboards', 1, NULL, NULL, NULL, 1, 1, 0),
	(30, 'Zubehör', 1, NULL, NULL, NULL, 1, 1, 0),
	(31, 'Alle', 1, 20, '&kategorie=1', NULL, 1, 1, 0),
	(32, 'Männer', 1, 20, '&kategorie=4', NULL, 1, 1, 0),
	(33, 'Frauen', 1, 20, '&kategorie=5', NULL, 1, 1, 0),
	(34, 'Kinder', 1, 20, '&kategorie=6', NULL, 1, 1, 0),
	(35, 'Alle', 1, 20, '&kategorie=2', NULL, 1, 1, 0),
	(36, 'Racing', 1, 20, '&kategorie=7', NULL, 1, 1, 0),
	(37, 'Freeride', 1, 20, '&kategorie=8', NULL, 1, 1, 0),
	(38, 'Alle', 1, 20, '&kategorie=3', NULL, 1, 1, 0),
	(39, 'Bindungen', 1, 20, '&kategorie=9', NULL, 1, 1, 0),
	(40, 'Protektoren', 1, 20, '&kategorie=10', NULL, 1, 1, 0),
	(41, 'Shopverwaltung', 1, 27, NULL, NULL, 1, 1, 0),
	(42, 'Bestellungen', 1, 34, NULL, NULL, 1, 1, 0),
	(43, 'Administration', 1, NULL, NULL, NULL, 1, 1, 0),
	(44, 'Registrieren', 1, 36, NULL, NULL, 1, 0, 1),
	(45, 'Adressen bearbeiten', 1, 37, NULL, NULL, 1, 1, 0);
/*!40000 ALTER TABLE `menüpunkt` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.menüpunkt_für_rolle
DROP TABLE IF EXISTS `menüpunkt_für_rolle`;
CREATE TABLE IF NOT EXISTS `menüpunkt_für_rolle` (
  `menüpunkt_id` int(11) NOT NULL,
  `rolle_id` int(11) NOT NULL,
  PRIMARY KEY (`menüpunkt_id`,`rolle_id`),
  KEY `FK_menüpunkt_für_rolle_rolle` (`rolle_id`),
  KEY `FK_menüpunkt_für_rolle_menüpunkt` (`menüpunkt_id`),
  CONSTRAINT `FK_menüpunkt_für_rolle_menüpunkt` FOREIGN KEY (`menüpunkt_id`) REFERENCES `menüpunkt` (`menüpunkt_id`),
  CONSTRAINT `FK_menüpunkt_für_rolle_rolle` FOREIGN KEY (`rolle_id`) REFERENCES `rolle` (`rolle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.menüpunkt_für_rolle: ~9 rows (ungefähr)
/*!40000 ALTER TABLE `menüpunkt_für_rolle` DISABLE KEYS */;
INSERT INTO `menüpunkt_für_rolle` (`menüpunkt_id`, `rolle_id`) VALUES
	(1, 1),
	(21, 2),
	(24, 1),
	(27, 2),
	(28, 2),
	(29, 2),
	(30, 2),
	(43, 1);
/*!40000 ALTER TABLE `menüpunkt_für_rolle` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.menüpunkt_hat_untermenüpunkt
DROP TABLE IF EXISTS `menüpunkt_hat_untermenüpunkt`;
CREATE TABLE IF NOT EXISTS `menüpunkt_hat_untermenüpunkt` (
  `menüpunkt_id` int(11) NOT NULL,
  `untermenüpunkt_id` int(11) NOT NULL,
  PRIMARY KEY (`menüpunkt_id`,`untermenüpunkt_id`),
  KEY `FK_menüpunkt_hat_untermenüpunkt_untermenüpunkt` (`untermenüpunkt_id`) USING BTREE,
  KEY `FK_menüpunkt_hat_untermenüpunkt_menüpunkt` (`menüpunkt_id`),
  CONSTRAINT `FK_menüpunkt_hat_untermenüpunkt_menüpunkt` FOREIGN KEY (`menüpunkt_id`) REFERENCES `menüpunkt` (`menüpunkt_id`),
  CONSTRAINT `FK_menüpunkt_hat_untermenüpunkt_menüpunkt_2` FOREIGN KEY (`untermenüpunkt_id`) REFERENCES `menüpunkt` (`menüpunkt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.menüpunkt_hat_untermenüpunkt: ~32 rows (ungefähr)
/*!40000 ALTER TABLE `menüpunkt_hat_untermenüpunkt` DISABLE KEYS */;
INSERT INTO `menüpunkt_hat_untermenüpunkt` (`menüpunkt_id`, `untermenüpunkt_id`) VALUES
	(1, 2),
	(1, 3),
	(1, 7),
	(2, 4),
	(2, 5),
	(4, 6),
	(8, 9),
	(8, 10),
	(8, 11),
	(8, 12),
	(8, 13),
	(8, 14),
	(8, 15),
	(8, 44),
	(16, 17),
	(16, 18),
	(16, 19),
	(16, 45),
	(22, 23),
	(24, 25),
	(24, 26),
	(28, 31),
	(28, 32),
	(28, 33),
	(28, 34),
	(29, 35),
	(29, 36),
	(29, 37),
	(30, 38),
	(30, 39),
	(30, 40),
	(43, 41),
	(43, 42);
/*!40000 ALTER TABLE `menüpunkt_hat_untermenüpunkt` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.rolle
DROP TABLE IF EXISTS `rolle`;
CREATE TABLE IF NOT EXISTS `rolle` (
  `rolle_id` int(11) NOT NULL AUTO_INCREMENT,
  `rolle_bezeichnung` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`rolle_id`),
  UNIQUE KEY `rolle_bezeichnung` (`rolle_bezeichnung`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.rolle: ~2 rows (ungefähr)
/*!40000 ALTER TABLE `rolle` DISABLE KEYS */;
INSERT INTO `rolle` (`rolle_id`, `rolle_bezeichnung`) VALUES
	(1, 'administrator'),
	(2, 'kunde');
/*!40000 ALTER TABLE `rolle` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.route
DROP TABLE IF EXISTS `route`;
CREATE TABLE IF NOT EXISTS `route` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_beschreibung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `route_zielpunkt` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `route_zielpunkt_bezugsquelle` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `route_zielpunkt_titelergänzung` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`route_id`),
  UNIQUE KEY `UK_route_zielpunkt` (`route_zielpunkt`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle snowboard-webshop.route: ~32 rows (ungefähr)
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
INSERT INTO `route` (`route_id`, `route_beschreibung`, `route_zielpunkt`, `route_zielpunkt_bezugsquelle`, `route_zielpunkt_titelergänzung`) VALUES
	(1, NULL, '', 'content/white.php', NULL),
	(2, NULL, 'white', 'content/white.php', NULL),
	(3, NULL, 'white2', 'content/white2.php', NULL),
	(4, NULL, 'white3', 'content/white3.php', NULL),
	(5, NULL, 'anme', 'content/anme/anme.php', ': Anmeldung'),
	(6, NULL, 'oefl_home', 'content/oefl/oefl_home.php', ': Startseite'),
	(7, NULL, 'oefl_wueu', 'content/oefl/oefl_wueu.php', ': Wir über uns'),
	(8, NULL, 'oefl_lnks', 'content/oefl/oefl_lnks.php', ': Links'),
	(9, NULL, 'oefl_pwvg', 'content/oefl/oefl_pwvg.php', ': Passwort vergessen?'),
	(10, NULL, 'oefl_fktn', 'content/oefl/oefl_fktn.php', ': Funktioniert\'s nicht?'),
	(11, NULL, 'eins', 'content/eins/eins.php', ': Einstellungen'),
	(12, NULL, 'eins_pwrt_aend', 'content/eins/eins_pwrt_aend.php', ': Einstellungen'),
	(13, NULL, 'eins_emai_aend', 'content/eins/eins_emai_aend.php', ': Einstellungen'),
	(14, NULL, 'kont', 'content/footer/kont.php', ': Kontakt'),
	(15, NULL, 'dtns', 'content/footer/dtns.php', ': Datenschutz'),
	(16, NULL, 'impr', 'content/footer/impr.php', ': Impressum'),
	(17, NULL, 'user_home', 'content/user/user_home.php', ': Home'),
	(18, NULL, 'user_prfl', 'content/user/user_prfl.php', ': Profil'),
	(19, NULL, 'sons', 'content/sons/sons.php', ': Sonstiges'),
	(20, NULL, 'shop_ubst', 'content/shop/shop_ubst_fltr.php', ': Shop-Übersicht'),
	(23, NULL, 'user_wrkb', 'content/user/user_wrkb.php', ': Warenkorb'),
	(24, NULL, 'shop_item', 'content/shop/shop_item.php', ''),
	(26, NULL, 'agb', 'content/footer/agb.php', ': AGB'),
	(27, NULL, 'shop_vwtg', 'content/shop/shop_vwtg.php', ': Verwaltung'),
	(28, NULL, 'user_bzhl', 'content/user/user_bzhl.php', ': Bezahlung'),
	(29, NULL, 'user_bzhl_pypl_exec', 'content/user/user_bzhl_pypl_exec.php', ': Bezahlung'),
	(30, NULL, 'user_bzhl_rchg', 'content/user/user_bzhl_rchg.php', ': Bezahlung'),
	(31, NULL, 'user_bzhl_rchg_exec', 'content/user/user_bzhl_rchg_exec.php', ': Bezahlung'),
	(32, NULL, 'user_bzhl_barz', 'content/user/user_bzhl_barz.php', ': Bezahlung'),
	(33, NULL, 'user_bzhl_barz_exec', 'content/user/user_bzhl_barz_exec.php', ': Bezahlung'),
	(34, NULL, 'shop_bstl', 'content/shop/shop_bstl.php', ': Bestellungen'),
	(35, NULL, 'white4', 'content/white4.php', NULL),
	(36, NULL, 'rgst', 'content/rgst/rgst.php', ': Registrierung'),
	(37, NULL, 'user_adrs', 'content/user/user_adrs.php', ': Adressen');
/*!40000 ALTER TABLE `route` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle snowboard-webshop.route_für_rolle
DROP TABLE IF EXISTS `route_für_rolle`;
CREATE TABLE IF NOT EXISTS `route_für_rolle` (
  `route_id` int(11) NOT NULL,
  `rolle_id` int(11) NOT NULL,
  PRIMARY KEY (`route_id`,`rolle_id`),
  KEY `menüpunkt_id_rolle_id` (`route_id`,`rolle_id`),
  KEY `FK_route_für_rolle_rolle` (`rolle_id`),
  CONSTRAINT `FK_route_für_rolle_rolle` FOREIGN KEY (`rolle_id`) REFERENCES `rolle` (`rolle_id`),
  CONSTRAINT `FK_route_für_rolle_route` FOREIGN KEY (`route_id`) REFERENCES `route` (`route_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Exportiere Daten aus Tabelle snowboard-webshop.route_für_rolle: ~11 rows (ungefähr)
/*!40000 ALTER TABLE `route_für_rolle` DISABLE KEYS */;
INSERT INTO `route_für_rolle` (`route_id`, `rolle_id`) VALUES
	(20, 2),
	(23, 2),
	(24, 2),
	(27, 1),
	(28, 2),
	(29, 2),
	(30, 2),
	(31, 2),
	(32, 2),
	(33, 2),
	(34, 1);
/*!40000 ALTER TABLE `route_für_rolle` ENABLE KEYS */;

-- Exportiere Struktur von Trigger snowboard-webshop.artikelkategorie_before_delete
DROP TRIGGER IF EXISTS `artikelkategorie_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `artikelkategorie_before_delete` BEFORE DELETE ON `artikelkategorie` FOR EACH ROW BEGIN
	DELETE FROM artikel WHERE artikelkategorie_id = old.artikelkategorie_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.benutzer_after_insert
DROP TRIGGER IF EXISTS `benutzer_after_insert`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `benutzer_after_insert` AFTER INSERT ON `benutzer` FOR EACH ROW BEGIN
	INSERT INTO benutzer_login_details (benutzer_id) VALUES (new.benutzer_id);
	INSERT INTO benutzer_hat_verwendet_passwort (benutzer_id, benutzer_verwendetes_passwort) VALUES (new.benutzer_id, new.benutzer_passwort);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.benutzer_before_delete
DROP TRIGGER IF EXISTS `benutzer_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `benutzer_before_delete` BEFORE DELETE ON `benutzer` FOR EACH ROW BEGIN
	DELETE FROM benutzer_login_details WHERE benutzer_id = old.benutzer_id;
	DELETE FROM benutzer_hat_verwendet_passwort WHERE benutzer_id = old.benutzer_id;
	DELETE FROM benutzer_adresse WHERE benutzer_id = old.benutzer_id;
	DELETE FROM administrator WHERE benutzer_id = old.benutzer_id;
	DELETE FROM kunde WHERE benutzer_id = old.benutzer_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.bestellung_before_delete
DROP TRIGGER IF EXISTS `bestellung_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `bestellung_before_delete` BEFORE DELETE ON `bestellung` FOR EACH ROW BEGIN
	DELETE FROM bestellung_hat_artikel WHERE bestellung_id = old.bestellung_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.menüpunkt_before_delete
DROP TRIGGER IF EXISTS `menüpunkt_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `menüpunkt_before_delete` BEFORE DELETE ON `menüpunkt` FOR EACH ROW BEGIN
	/*DELETE FROM menüpunkt_hat_rolle WHERE menüpunkt_id = old.menüpunkt_id;*/
	DELETE FROM menüpunkt_hat_untermenüpunkt WHERE untermenüpunkt_id = old.menüpunkt_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.menüpunkt_hat_untermenüpunkt_before_delete
DROP TRIGGER IF EXISTS `menüpunkt_hat_untermenüpunkt_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `menüpunkt_hat_untermenüpunkt_before_delete` BEFORE DELETE ON `menüpunkt_hat_untermenüpunkt` FOR EACH ROW BEGIN
	/*DELETE FROM menüpunkt WHERE menüpunkt_id = old.untermenüpunkt_id;*/
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.menü_before_delete
DROP TRIGGER IF EXISTS `menü_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `menü_before_delete` BEFORE DELETE ON `menü` FOR EACH ROW BEGIN
	DELETE FROM menüpunkt WHERE menüpunkt_id = old.menü_menüstartpunkt_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Exportiere Struktur von Trigger snowboard-webshop.route_before_delete
DROP TRIGGER IF EXISTS `route_before_delete`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `route_before_delete` BEFORE DELETE ON `route` FOR EACH ROW BEGIN
	/*DELETE FROM menüpunkt WHERE menüpunkt_id = old.menü_menüstartpunkt_id;*/
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
