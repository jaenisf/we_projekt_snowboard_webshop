﻿<?php
	require('content/anme/check_require_anme_beginn.php');
?>

<!-- Platzhalter falls eine Unterseite aktuell noch in Bearbeitung ist -->
<article id="" class="">
	<h2> 
		Funktioniert's nicht?
	</h2>
	<div class="alert alert-danger" role="alert" align="center">
		Diese Seite ist derzeit in Bearbeitung!
	</div>
</article>