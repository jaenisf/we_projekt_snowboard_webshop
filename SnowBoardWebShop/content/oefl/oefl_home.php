﻿<?php
	require('content/anme/check_require_anme_beginn.php');
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/oefl/oefl_home.css"/>

<!-- Willkommensbildschirm mit kleinem Newsfeed-->
<article id="" class="">
	<div class="container">
	<div class="col-md-12">
		<h2>Hals und Beinbruch meine lieben Freunde des Snowboardsports,</h2>
		</br>
		<p> 
			mein Name ist Heidi und ich möchte euch ganz herzlich in meinem 
			taufrischem Snowboardshop begrüßen! Als Leidenschaftliche und 
			langjährige Snowboardfahrerin habe ich mein Hobby zum Beruf gemacht. 
			Nun stelle ich euch meine neuen hochwertigen Snowboards in HeidiSnowBoards Webshop vor. 
			Mein Team und ich brennen schon darauf euch die nagelneuen Boards 
			zuzusenden, also schaut euch am besten gleich einmal um und ganz 
			viel Spaß beim Shoppen!
		</p>
		</br>
		<img id="startseite_img" src="./images/heidi_team.jpg">
		<p><b> Eure Heidi und Team </b></p>
	</div>
	</div>

	</br>
	</br>
	</br>
	
	<div class="container">
	<div class="col-md-12">
		<h2>News</h2>
		
		<h3>Viele neue schöne Boards!</h3>
		<p>Aufgrund einer neuen Partnerschaft kann ich euch nun auch Boards 
		von Refrax anbieten. Für alle die Refrax noch nicht kennen, 
		bekannt geworden ist der Hersteller vor allem durch seine Racing Boards, 
		auf denen schon zahlreiche Turniere gewonnen wurden. Wenn es euch also 
		nicht schnell genug gehen kann, schaut euch die neuen Boards am besten 
		gleich einmal an!</p>
		<div>
		<span class="badge">Eingestellt am 29.05.2020 um 07:28:10</span><div class="pull-right">         
		</div>

		<hr> 
		<h3>Nun auch mit Zubehör!</h3>
		<p>Aufgrund eurer zahlreichen Rückfragen habe ich nun auch Zubehör mit 
		in mein Inventar mit aufgenommen. </p>
		<div>
			<span class="badge">Eingestellt am 28.05.2020 um 16:07:00</span><div class="pull-right">
		</div>

		<hr>
		<h3>Liveschaltung der Webseite</h3>
		<p>Heute ist es endlich soweit, meine neue Webseite wird 
		endlich aus der Taufe gehoben. Ich hoffe Ihr habt genau so viel 
		Spaß am Shoppen wie ich am Erstellen meines neuen Shops. 
		Viel Spaß und Grüzi!</p>
		<div>
			<span class="badge">Eingestellt am 25.05.2020 um 20:47:04</span><div class="pull-right">
		</div>

	</div> 
	</div>

	</br>
	</br>
	</br>
</article>