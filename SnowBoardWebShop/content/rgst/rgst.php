﻿<?php
	require('content/anme/check_require_anme_beginn.php');
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/rgst/rgst.css"/>

<!-- Registrierung -->
<article id="" class="">
	<h2>
		Registrierung
	</h2>
	
	<?php
		//Diese Funktion überprüft ein Passwort auf Gültigkeit.
		//Hierbei wird unter anderem die Länge des Passwortes, aber auch die verwendeten Zeichen geprüft
		//Bei korrekter Eingabe wird das Passwort auch gleich aktualisiert
		function prüfePasswort($passwort)
		{
			$ausgabe = "";
				
			$erlaubte_zeichen = array(
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
				"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
				"ä", "ö", "ü", "ß", 
				"(", ")", "[", "]", "{", "}", "?", "!", "$", "%", "&", "/", "=",
				"*", "+", "~", ",", ".", ";", ":", "<", ">", "-", "_",
				"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
				"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
				"Ä", "Ö", "Ü", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
				
			$sonderzeichen = array(
				"(", ")", "[", "]", "{", "}", "?", "!", "$", "%", "&", "/", "=",
				"*", "+", "~", ",", ".", ";", ":", "<", ">", "-", "_");
					
			$passwort = chop(trim($passwort));
			$länge = strlen($passwort);
				
			if ($länge > 0)
			{
				if ($länge < 8)
				{
					$ausgabe .= "Das Passwort muss aus mindestens 8 Zeichen bestehen.<br />";
				} 
					
				if ($länge > 20) 
				{
					$ausgabe .= "Das Passwort darf aus höchstens 20 Zeichen bestehen.<br />";
				} 
				
				$ok = true;
				
				for ($i = 0; $i <= $länge - 1; $i++)
				{
					if (!in_array(substr($passwort, $i, 1), $erlaubte_zeichen))
					{
						$ok = false;
					}
				}
				
				if ($ok == false) 
				{
					$ausgabe .= "Das Passwort besteht aus unerlaubten Zeichen. <br/>";
				}
				
				$ok = false;
				
				for ($i = 0; $i <= $länge - 1; $i++)
				{
					if (in_array(substr($passwort, $i, 1), $sonderzeichen))
					{
						$ok = true;
					}
				}
				
				if ($ok == false) 
				{
					$ausgabe .= "Das Passwort muss mindestens ein Sonderzeichen enthalten. <br/>";
				}
				
				$ok = false;
				
				for ($i = 0; $i <= $länge - 1; $i++)
				{
					if (is_numeric(substr($passwort, $i, 1))) 
					{
						$ok = true;
					}
				}
				
				if ($ok == false) 
				{
					$ausgabe .= "Das Passwort muss mindestens eine Ziffer enthalten. <br/>";
				}
				
				if (strcmp(strtoupper($passwort), $passwort) == 0) 
				{
					$ausgabe .= "Das Passwort muss mindestens einen Kleinbuchstaben enthalten. <br/>";
				}
				
				if (strcmp(strtolower($passwort), $passwort) == 0) 
				{
					$ausgabe .= "Das Passwort muss mindestens einen Großbuchstaben enthalten. <br/>";
				}
			} 
			else 
			{
			   $ausgabe .= "Das Passwort-Eingabefeld war leer. <br/>";  
			};
			
			return $ausgabe;
		};
		
		if(isset($_POST['passwort_1']) == true && isset($_POST['passwort_2']) == true)
		{
			$success = true;
			$error = false;
			
			$passwort_1 = $_POST['passwort_1'];
			$passwort_2 = $_POST['passwort_2'];
			
			if ($passwort_1 != $passwort_2) 
			{
				$success = false;
				$error = true;
				$error_report = "Die Bestätigung Ihres Passwortes stimmt nicht mit dem Passwort überein.";
			} 
			else 
			{
				if (prüfePasswort($passwort_1) != "") 
				{
					$success = false;
					$error = true;
					$error_report = prüfePasswort($passwort_1);
				}
				
				if ($success != false && $error != true)
				{
					$anrede = $_POST['anrede'];
					$vorname = $_POST['vorname'];
					$nachname = $_POST['nachname'];
					$emailadresse = $_POST['email-adresse'];
					$passwort_1 = $_POST['passwort_1'];
					$passwort_2 = $_POST['passwort_2'];
					$passwort = md5($passwort_1);
					$geburtsdatum = $_POST['geburtsdatum'];
					$benutzername_unverschlüsselt = md5($emailadresse);
					$benutzername = md5($benutzername_unverschlüsselt);
					
					$speichern = "INSERT INTO benutzer 
								  (benutzer_anrede, benutzer_vorname, benutzer_nachname, 
								   benutzer_email_adresse, benutzer_geburtsdatum, benutzer_passwort,
								   benutzer_benutzername, benutzer_benutzername_unverschlüsselt)
								  VALUES
								  ('".$anrede."', '".$vorname."', '".$nachname."', 
								   '".$emailadresse."', '".$geburtsdatum."', '".$passwort."',
								   '".$benutzername."', '".$benutzername_unverschlüsselt."');";
						
					$verbindung->query($speichern);
					
					$abfrage_1 = "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES 
								  WHERE TABLE_SCHEMA = (SELECT database()) 
								  AND TABLE_NAME = 'benutzer';";
									   
					$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
							
					while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
					{
						$datensatz_auto_increment = ($datensatz_1->AUTO_INCREMENT);
					}
					
					$speichern = "INSERT INTO kunde 
								  (benutzer_id) 
								  VALUES ('".($datensatz_auto_increment-1)."');";
						
					$verbindung->query($speichern);
					
					header("Location: index.php?page=anme");
				}
			}
		}
	?>

	<div id="registrierung" class="container">
		<?php
			if ($error_report != "")
			{
		?>
		<div class="alert alert-danger" role="alert">
			<?php echo $error_report; ?>
		</div>
		<?php
			}
		?>
		<form action="index.php?page=rgst" method="post" class="form-horizontal" role="form">
			<div class="form-group">
				<label for="anrede" class="col-sm-3 control-label">
					Anrede
				</label>
				<div class="col-sm-9">
					<div class="row">
						<div class="col-sm-4">
							<label class="radio-inline">
								<select name="anrede" id="anrede" required style="width: 80px; margin-left: -20px; padding: 2px 5px; border: 1px solid #ccc;">
									<option value="" <?php if ($_POST['anrede'] == "") { echo "selected"; } ?>> </option>
									<option value="Herr" <?php if ($_POST['anrede'] == "Herr") { echo "selected"; } ?>> Herr </option>
									<option value="Frau" <?php if ($_POST['anrede'] == "Frau") { echo "selected"; } ?>> Frau </option>
								--></select>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="vorname" class="col-sm-3 control-label">
					Vorname
				</label>
				<div class="col-sm-9">
					<input type="text" name="vorname" id="vorname" placeholder="Vorname" minlength="2" maxlength="50" value="<?php echo $_POST['vorname']; ?>" class="form-control" required autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="nachname" class="col-sm-3 control-label">
					Nachname
				</label>
				<div class="col-sm-9">
					<input type="text" name="nachname" id="nachname" placeholder="Nachname" minlength="2" maxlength="50" value="<?php echo $_POST['nachname']; ?>" class="form-control" required autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="email-adresse" class="col-sm-3 control-label">
					Email-Adresse
				</label>
				<div class="col-sm-9">
					<input type="email" name="email-adresse" id="email-adresse"  value="<?php echo $_POST['email-adresse']; ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Zeichen gefolgt von einem @-Zeichen gefolgt von mehreren Zeichen und dann einem Puntk '.' gefolgt von mindestens 2 Zeichen von a bis z" placeholder="Email-Adresse" minlength="6" maxlength="50" class="form-control" required autofocus>
				</div>
			</div>
			<div class="form-group">
				<label for="passwort_1" class="col-sm-3 control-label">
					Passwort*
				</label>
				<div class="col-sm-9">
					<input type="password" name="passwort_1" id="passwort_1" value="<?php echo $_POST['passwort_1']; ?>"
						   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}" title="Es muss mindestens eine Ziffer, mindestens einen Klein-, mindestens einen Großbuchstaben und mindestens ein Sonderzeichen enthalten."
						   placeholder="Passwort" minlength="8" maxlength="20" class="form-control" required autofocus
					>
				</div>
			</div>
			<div class="form-group">
				<label for="passwort_2" class="col-sm-3 control-label">
					Passwortbestätigung*
				</label>
				<div class="col-sm-9">
					<input type="password" name="passwort_2" id="passwort_2" value="<?php echo $_POST['passwort_2']; ?>"
						   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}" title="Es muss mindestens eine Ziffer, mindestens einen Klein-, mindestens einen Großbuchstaben und mindestens ein Sonderzeichen enthalten."
						   placeholder="Passwort" minlength="8" maxlength="20" class="form-control" required autofocus
					>
				</div>
			</div>
			<div class="form-group">
				<label for="geburtsdatum" class="col-sm-3 control-label">
					Geburtsdatum
				</label>
				<div class="col-sm-9">
					<input type="date" name="geburtsdatum" id="geburtsdatum" value="<?php echo $_POST['geburtsdatum']; ?>" class="form-control" required autofocus>
				</div>
			</div>
			<button type="submit" name="registrieren" class="btn btn-primary btn-block" style="width: 120px; margin: 0 auto;">
				Registrieren
			</button>
		</form> <!-- /form -->
		<div id="registrierungs_regeln">
			* Hinweis:
			<br>
			Das neue Passwort muss aus 8 bis 20 Zeichen bestehen. 
			Es muss mindestens eine Ziffer, mindestens einen Klein-, mindestens einen Großbuchstaben und mindestens ein Sonderzeichen enthalten.
			<br>
			<br>
			Als Zeichen sind somit erlaubt: 
			<br>
			Alle lateinischen Ziffern, alle deutschen Groß- und Kleinbuchstaben (mit Umlaute), 
			sowie ( ) [ ] { } ? ! $ % & / = * + ~ , . ; : < > - _ (keine Leerzeichen).
		</div>
	</div> <!-- ./container -->
	
</article>