<style>
	.not-active {
	   pointer-events: none;
	   cursor: default;
	   background-color: lightgray;
	   tranperency: 0.8;
	   text-decoration: line-through;
	}
</style>

<?php
	function get_menü($verbindung, $menü_id, $benutzer_id)
	{
		$abfrage_1 = "SELECT * FROM menü WHERE menü_id = '".$menü_id."';";
		$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
					
		while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
		{
			$datensatz_menü_id = ($datensatz_1->menü_id);
			$datensatz_menü_name = ($datensatz_1->menü_name);
			$datensatz_menü_menüstartpunkt_id = ($datensatz_1->menü_menüstartpunkt_id);
			$datensatz_menü_modus = ($datensatz_1->menü_modus);
		}
		
		if (get_benutzerberechtigung_menüpunkt($verbindung, $datensatz_menü_menüstartpunkt_id, $benutzer_id) == true)
		{
			$abfrage_2 = "SELECT * FROM menüpunkt WHERE menüpunkt_id = '".$datensatz_menü_menüstartpunkt_id."';";
			$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
							
			while($datensatz_2 = $datenbank_ergebnis_2->fetch_object()) {
				$datensatz_menüpunkt_id = ($datensatz_2->menüpunkt_id);
				$datensatz_menüpunkt_name = ($datensatz_2->menüpunkt_name);
				$datensatz_interne_route = ($datensatz_2->menüpunkt_interne_route);
				$datensatz_interne_referenz = ($datensatz_2->menüpunkt_interne_referenz);
				$datensatz_interne_referenz_ergänzung = ($datensatz_2->menüpunkt_interne_referenz_ergänzung);
				$datensatz_externe_referenz = ($datensatz_2->menüpunkt_externe_referenz);
				$datensatz_aktiv = ($datensatz_2->menüpunkt_aktiv);
				
				if ($datensatz_interne_route == 1)
				{
					// Abfrage, ob eingebene Route vorhanden ist
					// wenn ja --> Route wird abgerufen
					// wenn nein --> Default-Route wird abgerufen
					
					$abfrage_3 = "SELECT COUNT(*) AS anzahl FROM route WHERE route_id = '".$datensatz_interne_referenz."';";
					$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
							
					while($datensatz_3 = $datenbank_ergebnis_3->fetch_object()) {
						$datensatz_anzahl = ($datensatz_3->anzahl);
							
						if ($datensatz_anzahl > 0)
						{
							// herausfinden des Routenzielpunktes für die URL mittel der Routen-ID
							
							$abfrage_4 = "SELECT * FROM route WHERE route_id = '".$datensatz_interne_referenz."';";
							$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
									
							while($datensatz_4 = $datenbank_ergebnis_4->fetch_object()) {
								$datensatz_route_id = ($datensatz_4->route_id);
								$datensatz_route_beschreibung = ($datensatz_4->route_beschreibung);
								$datensatz_route_zielpunkt = ($datensatz_4->route_zielpunkt);
								$datensatz_route_zielpunkt_bezugsquelle = ($datensatz_4->route_zielpunkt_bezugsquelle);
								$datensatz_route_titelergänzung = ($datensatz_4->route_zielpunkt_titelergänzung);
							};
							
							$route_zielpunkt = $datensatz_route_zielpunkt;
						} 
						else 
						{
							$route_zielpunkt = "white3";
						};
					};
					
					if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
					{
						$url = "https";
					}
					else
					{
						$url = "http";
					};
					
					// Here append the common URL characters.
					$url .= "://";
					
					// Append the host(domain name, ip) to the URL. 
					$url .= $_SERVER['HTTP_HOST'];
						
					// Append the requested resource location to the URL
					$url .= $_SERVER['REQUEST_URI'];
					
					// Remove GET-Variable
					$url = explode("?", $url)[0];
					
					//  Append the GET-Variable 'page' with the corresponding value
					$url .= "?page=".$route_zielpunkt;
					
					// Append other GET-Variabls with the corresponding value
					$url .= $datensatz_interne_referenz_ergänzung;
				}
				else
				{
					$url = $datensatz_externe_referenz;
				}
			}
			
			if ($datensatz_menü_modus == "dropdown")
			{
				if ($_GET["page"] == $datensatz_route_zielpunkt)
				{
					$set_active = 'id="active"';
				}
				else
				{
					$set_active = '';
				}
				
				echo '	
						<li class="dropdown" '.$set_active.'> 
							<a href="'.$url.'" class="dropdown-toggle" data-toggle="dropdown"> 
								'.$datensatz_menü_name.'
							</a>
					';
							
				// i-tes Menuelement einer Hierarchie
				$i = 1;
					
				$abfrage_5 = "SELECT COUNT(*) AS anzahl FROM menüpunkt_hat_untermenüpunkt WHERE menüpunkt_id = '".$datensatz_menü_menüstartpunkt_id."';";
				$datenbank_ergebnis_5 = $verbindung->query( $abfrage_5 );
									
				while($datensatz_5 = $datenbank_ergebnis_5->fetch_object()) {
					$datensatz_anzahl = ($datensatz_5->anzahl);
				};
					
				// Anzahl der Menüelemente einer Hierarchie
				$s = $datensatz_anzahl;
				
				$abfrage_6 = "SELECT * FROM menüpunkt_hat_untermenüpunkt WHERE menüpunkt_id = '".$datensatz_menü_menüstartpunkt_id."';";
				$datenbank_ergebnis_6 = $verbindung->query( $abfrage_6 );
				
				while($datensatz_6 = $datenbank_ergebnis_6->fetch_object()) {
					$datensatz_menüpunkt_id = ($datensatz_6->menüpunkt_id);
					$datensatz_untermenüpunkt_id = ($datensatz_6->untermenüpunkt_id);
					
					$return = get_untermenüpunkt($verbindung, $datensatz_untermenüpunkt_id, 0, $i, $s, $benutzer_id);
					$s = $datensatz_anzahl;
					$i = $return;
					
					if ($i-1 == $s)
					{
						echo '</ul>';
					}
					else
					{
					};
					
					$i++;
				}
				
				echo '</li>';
				
				if ($i-1 == $s)
				{
					echo '</ul>';
				}
			}
			else
			{
				if ($_GET["page"] == $datensatz_route_zielpunkt
					&& strpos($url, $_SERVER['REQUEST_URI']) !== false)
				{
					$set_active = 'id="active"';
				}
				else
				{
					$set_active = '';
				}
				
				echo '	
					<li class="presentation" '.$set_active.'> 
						<a href="'.$url.'"> 
							'.$datensatz_menü_name.'
						</a>
					</li>
				';
			}
		}
	}

	function get_untermenüpunkt($verbindung, $menüpunkt_id, $boolean_ist_untermenüpunkt, $i, $s, $benutzer_id)
	{		
		$return = $i;
		
		if ($boolean_ist_untermenüpunkt == 0 && $i == 1)
		{
			echo '<ul class="dropdown-menu">';
		}
		else if ($boolean_ist_untermenüpunkt == 1 && $i == 1)
		{
			echo '<ul class="dropdown-submenu" style="padding-right: 15px;">';
		}
		else
		{
		};
		
		$abfrage_2 = "SELECT * FROM menüpunkt WHERE menüpunkt_id = '".$menüpunkt_id."';";
		$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
						
		while($datensatz_2 = $datenbank_ergebnis_2->fetch_object()) {
			$datensatz_menüpunkt_id = ($datensatz_2->menüpunkt_id);
			$datensatz_menüpunkt_name = ($datensatz_2->menüpunkt_name);
			$datensatz_interne_route = ($datensatz_2->menüpunkt_interne_route);
			$datensatz_interne_referenz = ($datensatz_2->menüpunkt_interne_referenz);
			$datensatz_interne_referenz_ergänzung = ($datensatz_2->menüpunkt_interne_referenz_ergänzung);
			$datensatz_externe_referenz = ($datensatz_2->menüpunkt_externe_referenz);
			$datensatz_aktiv = ($datensatz_2->menüpunkt_aktiv);
			
			if ($datensatz_interne_route == 1)
			{
				// Abfrage, ob eingebene Route vorhanden ist
				// wenn ja --> Route wird abgerufen
				// wenn nein --> Default-Route wird abgerufen
				
				$abfrage_3 = "SELECT COUNT(*) AS anzahl FROM route WHERE route_id = '".$datensatz_interne_referenz."';";
				$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
						
				while($datensatz_3 = $datenbank_ergebnis_3->fetch_object()) {
					$datensatz_anzahl = ($datensatz_3->anzahl);
						
					if ($datensatz_anzahl > 0)
					{
						// herausfinden des Routenzielpunktes für die URL mittel der Routen-ID
						
						$abfrage_4 = "SELECT * FROM route WHERE route_id = '".$datensatz_interne_referenz."';";
						$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
								
						while($datensatz_4 = $datenbank_ergebnis_4->fetch_object()) {
							$datensatz_route_id = ($datensatz_4->route_id);
							$datensatz_route_beschreibung = ($datensatz_4->route_beschreibung);
							$datensatz_route_zielpunkt = ($datensatz_4->route_zielpunkt);
							$datensatz_route_zielpunkt_bezugsquelle = ($datensatz_4->route_zielpunkt_bezugsquelle);
							$datensatz_route_titelergänzung = ($datensatz_4->route_zielpunkt_titelergänzung);
						};
						
						$route_zielpunkt = $datensatz_route_zielpunkt;
					} 
					else 
					{
						$route_zielpunkt = "white3";
					};
				};
				
				if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
				{
					$url = "https";
				}
				else
				{
					$url = "http";
				};
				
				// Here append the common URL characters.
				$url .= "://";
				
				// Append the host(domain name, ip) to the URL. 
				$url .= $_SERVER['HTTP_HOST'];
					
				// Append the requested resource location to the URL
				$url .= $_SERVER['REQUEST_URI'];
				
				// Remove GET-Variable
				$url = explode("?", $url)[0];
				
				//  Append the GET-Variable 'page' with the corresponding value
				$url .= "?page=".$route_zielpunkt;
				
				// Append other GET-Variabls with the corresponding value
				$url .= $datensatz_interne_referenz_ergänzung;
			}
			else
			{
				$url = $datensatz_externe_referenz;
			}
			
			if (get_benutzerberechtigung_menüpunkt($verbindung, $menüpunkt_id, $benutzer_id) == true)
			{
				echo '
						<li>
							<a href="'.$url.'"
					';
									
								if ($_GET["page"] == $datensatz_route_zielpunkt
									&& strpos($url, $_SERVER['REQUEST_URI']) !== false)
								{
									echo "id='active'";
								}
				echo '		
							> 
								'.$datensatz_menüpunkt_name.'
							</a>
					';
			}
			
			// i-tes Menuelement einer Hierarchie
			$i = 1;
			
			$abfrage_4 = "SELECT COUNT(*) AS anzahl FROM menüpunkt_hat_untermenüpunkt WHERE menüpunkt_id = '".$datensatz_menüpunkt_id."';";
			$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
							
			while($datensatz_4 = $datenbank_ergebnis_4->fetch_object()) {
				$datensatz_anzahl = ($datensatz_4->anzahl);
			};
			
			// Anzahl der Menüelemente einer Hierarchie
			$s = $datensatz_anzahl;
							
			$abfrage_5 = "SELECT * FROM menüpunkt_hat_untermenüpunkt WHERE menüpunkt_id = '".$datensatz_menüpunkt_id."';";
			$datenbank_ergebnis_5 = $verbindung->query( $abfrage_5 );
							
			while($datensatz_5 = $datenbank_ergebnis_5->fetch_object()) {
				$datensatz_menüpunkt_id = ($datensatz_5->menüpunkt_id);
				$datensatz_untermenüpunkt_id = ($datensatz_5->untermenüpunkt_id);
				
				$return = get_untermenüpunkt($verbindung, $datensatz_untermenüpunkt_id, 1, $i, $s, $benutzer_id);
				$s = $datensatz_anzahl;
				$i = $return;
				
				if ($i == $s)
				{
					echo '</ul>';
				}
				else
				{
				};
				
				$i++;
			};
			
			if (get_benutzerberechtigung_menüpunkt($verbindung, $menüpunkt_id, $benutzer_id) == true)
			{
				echo '
						</li>
					';
			}
		};
		
		if ($i == $s)
		{
			echo '</ul>';
		}
		else
		{
		};
		
		return $return;
	}
	
	function get_benutzerberechtigung_menüpunkt($verbindung, $menüpunkt_id, $benutzer_id)
	{
		$abfrage_0 = "SELECT * FROM menüpunkt WHERE menüpunkt_id = '".$menüpunkt_id."';";
		$datenbank_ergebnis_0 = $verbindung->query( $abfrage_0 );
						
		while($datensatz_0 = $datenbank_ergebnis_0->fetch_object()) {
			$datensatz_menüpunkt_aktiv = ($datensatz_0->menüpunkt_aktiv);
			$datensatz_menüpunkt_nur_mit_anmeldung = ($datensatz_0->menüpunkt_nur_bei_anmeldung);
			$datensatz_menüpunkt_nur_ohne_anmeldung = ($datensatz_0->menüpunkt_nur_ohne_anmeldung);
		}
		
		if ($datensatz_menüpunkt_aktiv == 0
			|| ($datensatz_menüpunkt_nur_mit_anmeldung == 0
				&& $datensatz_menüpunkt_nur_ohne_anmeldung == 0))
		{
			return false;
		}
		
		if (($benutzer_id == 0 || $benutzer_id == "")
			&& $datensatz_menüpunkt_nur_mit_anmeldung == 1
			&& $datensatz_menüpunkt_nur_ohne_anmeldung == 0)
		{
			return false;
		}
		
		if (($benutzer_id != 0 || $benutzer_id != "")
			&& $datensatz_menüpunkt_nur_mit_anmeldung == 0
			&& $datensatz_menüpunkt_nur_ohne_anmeldung == 1)
		{
			return false;
		}
		
		$abfrage_1 = "SELECT COUNT(*) AS anzahl FROM menüpunkt 
					  INNER JOIN menüpunkt_für_rolle 
					  ON menüpunkt.menüpunkt_id = menüpunkt_für_rolle.menüpunkt_id 
					  WHERE menüpunkt.menüpunkt_id = '".$menüpunkt_id."';";
		$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
					
		while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
		{
			$datensatz_anzahl = ($datensatz_1->anzahl);
		}
		
		if ($datensatz_anzahl > 0)
		{
			$abfrage_2 = "SELECT * FROM menüpunkt_für_rolle 
						  INNER JOIN rolle 
						  ON menüpunkt_für_rolle.rolle_id = rolle.rolle_id 
						  WHERE menüpunkt_id = '".$menüpunkt_id."';";
			$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
							
			while($datensatz_2 = $datenbank_ergebnis_2->fetch_object()) {
				$datensatz_menüpunkt_id = ($datensatz_2->menüpunkt_id);
				$datensatz_rolle_id = ($datensatz_2->rolle_id);
				$datensatz_rolle_bezeichnung = ($datensatz_2->rolle_bezeichnung);
				
				$abfrage_3 = "SELECT COUNT(*) AS anzahl FROM INFORMATION_SCHEMA.TABLES 
							  WHERE TABLE_SCHEMA = (SELECT database()) 
							  AND TABLE_NAME = '".$datensatz_rolle_bezeichnung."';";
				$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
								
				while($datensatz_3 = $datenbank_ergebnis_3->fetch_object()) {
					$datensatz_anzahl = ($datensatz_3->anzahl);
				}
				
				if ($datensatz_anzahl > 0)
				{
					$abfrage_4 = "SELECT COUNT(*) AS anzahl FROM ".$datensatz_rolle_bezeichnung." WHERE benutzer_id = '".$benutzer_id."';";
					$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
									
					while($datensatz_4 = $datenbank_ergebnis_4->fetch_object()) {
						$datensatz_anzahl = ($datensatz_4->anzahl);
					}
					
					if ($datensatz_anzahl > 0)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}

			return false;
		}
		else
		{
			$boolean = true;
			
			$abfrage_5 = "SELECT * FROM menüpunkt_hat_untermenüpunkt WHERE untermenüpunkt_id = '".$menüpunkt_id."';";
			$datenbank_ergebnis_5 = $verbindung->query( $abfrage_5 );
							
			while($datensatz_5 = $datenbank_ergebnis_5->fetch_object()) {
				$datensatz_menüpunkt_id = ($datensatz_5->menüpunkt_id);
				$datensatz_untermenüpunkt_id = ($datensatz_5->untermenüpunkt_id);
				
				if (get_benutzerberechtigung_menüpunkt($verbindung, $datensatz_menüpunkt_id, $benutzer_id) == false)
				{
					$boolean = false;
				}
			}		
			
			return $boolean;
		}
	}
?>

<nav class="navbar navbar-default" style="z-index: 1;">
	<div class="container-fluid">
		<div class="navbar-header">
		    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
		    </button>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<!-- Navbar left -->
			<ul class="nav navbar-nav">
				<?php 
					$abfrage_1 = "SELECT * FROM menü
								  WHERE menü_menüleiste = 'links'
								  ORDER BY menü_menüleiste_position ASC;";
					$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
									
					while($datensatz_1 = $datenbank_ergebnis_1->fetch_object()) {
						$datensatz_menü_id = ($datensatz_1->menü_id);
						
						get_menü($verbindung, $datensatz_menü_id, $benutzer_id);
					}
				?>
			</ul>
			
			<!--<li id="trenner"> <hr> </li>-->
			
			<!-- Navbar right -->
			<ul class="nav navbar-nav navbar-right">
				<li align="center" style="margin-top: 11px; margin-bottom: 11px;"> 
					<span id="date_time"> 
						<span id="uhrzeit"> </span> <span class="badge"> </span> <span id="datum"> </span> <span class="badge"> </span> KW: <span id="kalenderwoche"> </span>
					</span>
				</li>
				<li id="trenner"> 
					<hr>
				</li>
				<?php 
					$abfrage_1 = "SELECT * FROM menü
								  WHERE menü_menüleiste = 'rechts'
								  ORDER BY menü_menüleiste_position DESC;";
					$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
									
					while($datensatz_1 = $datenbank_ergebnis_1->fetch_object()) {
						$datensatz_menü_id = ($datensatz_1->menü_id);
						
						get_menü($verbindung, $datensatz_menü_id, $benutzer_id);
					}
				?>
			</ul>
		</div>
	</div>
</nav>

<?php 
	if (isset($_SESSION['warenkorb']) == true)
	{
		$anzahl = 0;
		
		foreach ($_SESSION['warenkorb'] as $key => $value)
		{
			$anzahl += $value['menge'];
		}
		
		if ($anzahl != 0)
		{
?>
	<script>
		document.getElementById('warenkorb_anzahl').innerHTML = "&emsp;<div style='display: inline-block; background-color: #FFFFFF; color: #03045E; border: 1.5px solid #03045E; border-radius: 50%; padding: 5px; width: 22px; height: 22px; margin-top: -4px; text-align: center; vertical-align: middle; padding-top: 0.5px; padding-left: 2.5px; font-weight: bold;'>"+<?php echo $anzahl; ?>+"</div>";
	</script>
<?php	
		}
	}
?>