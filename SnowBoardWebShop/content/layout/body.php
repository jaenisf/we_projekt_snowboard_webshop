<body>
	<div class="container-fluid">
		<div class="row" id="kopfbereich">
			<div class="snowflakes">
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i><i></i>
			<i></i><i></i><i></i><i></i>
			</div>
			<?php include('content/layout/kopfbereich.php'); ?>
		</div>
		<div class="row" id="menübereich">
			<?php include('content/layout/menübereich.php'); ?>
		</div>
		<div class="row" id="content_bereich">
			<div class="row" id="content">
				<?php include $seiteninhalt; ?>
			</div>
		</div>
		<div class="row" id="footer">  
			<?php include('content/layout/footer.php'); ?>
		</div>
	</div>
</body>