<!-- Hier wird die Fußzeile mit dem Kontaktformular, AGBs, Impressum und Datenschutz definiert -->
<ul>
	<li> 
		<a href="index.php?page=kont"
			<?php 
				if ($_GET["page"] == "kont")
				{ 
					echo "style='text-decoration: underline;'"; 
				}
			?>
		> 
			Kontakt  
		</a>
	</li>
	<li>
		<a href="index.php?page=dtns"
			<?php 
				if ($_GET["page"] == "dtns")
				{ 
					echo "style='text-decoration: underline;'"; 
				}
			?>
		>
			Datenschutz
		</a>
	</li>
	<li>
		<a href="index.php?page=impr"
			<?php 
				if ($_GET["page"] == "impr")
				{ 
					echo "style='text-decoration: underline;'"; 
				}
			?>
		>
			Impressum
		</a>
	</li>
	<li>
		<a href="index.php?page=agb"
			<?php 
				if ($_GET["page"] == "agb")
				{ 
					echo "style='text-decoration: underline;'"; 
				}
			?>
		>
			AGB
		</a>
	</li>
	<li id="abstand"> 
		&copy; 2020 &ensp; Team Angry-Nerds 
	</li>
</ul>