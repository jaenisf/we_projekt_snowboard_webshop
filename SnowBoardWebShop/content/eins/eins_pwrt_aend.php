﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	//Diese Funktion überprüft ein Passwort auf Gültigkeit.
	//Hierbei wird unter anderem die Länge des Passwortes, aber auch die verwendeten Zeichen geprüft
	//Bei korrekter Eingabe wird das Passwort auch gleich aktualisiert
	function prüfePasswort($passwort)
	{
		$ausgabe = "";
			
		$erlaubte_zeichen = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
			"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
			"ä", "ö", "ü", "ß", 
			"(", ")", "[", "]", "{", "}", "?", "!", "$", "%", "&", "/", "=",
			"*", "+", "~", ",", ".", ";", ":", "<", ">", "-", "_",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"Ä", "Ö", "Ü", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
			
		$sonderzeichen = array(
			"(", ")", "[", "]", "{", "}", "?", "!", "$", "%", "&", "/", "=",
			"*", "+", "~", ",", ".", ";", ":", "<", ">", "-", "_");
				
		$passwort = chop(trim($passwort));
		$länge = strlen($passwort);
			
		if ($länge > 0)
		{
			if ($länge < 8)
			{
				$ausgabe .= "Das Passwort muss aus mindestens 8 Zeichen bestehen.<br />";
			} 
				
			if ($länge > 20) 
			{
				$ausgabe .= "Das Passwort darf aus höchstens 20 Zeichen bestehen.<br />";
			} 
			
			$ok = true;
			
			for ($i = 0; $i <= $länge - 1; $i++)
			{
				if (!in_array(substr($passwort, $i, 1), $erlaubte_zeichen))
				{
					$ok = false;
				}
			}
			
			if ($ok == false) 
			{
				$ausgabe .= "Das Passwort besteht aus unerlaubten Zeichen. <br/>";
			}
			
			$ok = false;
			
			for ($i = 0; $i <= $länge - 1; $i++)
			{
				if (in_array(substr($passwort, $i, 1), $sonderzeichen))
				{
					$ok = true;
				}
			}
			
			if ($ok == false) 
			{
				$ausgabe .= "Das Passwort muss mindestens ein Sonderzeichen enthalten. <br/>";
			}
			
			$ok = false;
			
			for ($i = 0; $i <= $länge - 1; $i++)
			{
				if (is_numeric(substr($passwort, $i, 1))) 
				{
					$ok = true;
				}
			}
			
			if ($ok == false) 
			{
				$ausgabe .= "Das Passwort muss mindestens eine Ziffer enthalten. <br/>";
			}
			
			if (strcmp(strtoupper($passwort), $passwort) == 0) 
			{
				$ausgabe .= "Das Passwort muss mindestens einen Kleinbuchstaben enthalten. <br/>";
			}
			
			if (strcmp(strtolower($passwort), $passwort) == 0) 
			{
				$ausgabe .= "Das Passwort muss mindestens einen Großbuchstaben enthalten. <br/>";
			}
		} 
		else 
		{
		   $ausgabe .= "Das Passwort-Eingabefeld war leer. <br/>";  
		};
		
		return $ausgabe;
	};
		
	$altes_passwort = $_POST['altes_passwort'];
	$neues_passwort_1 = $_POST['neues_passwort_1'];
	$neues_passwort_2 = $_POST['neues_passwort_2'];
	
	if ($altes_passwort == "" OR $neues_passwort_1 == "" OR $neues_passwort_2 == "" OR $weiter == false) 
	{
		$success = false;
		$error = false;
	} 
	else 
	{
		if ($neues_passwort_1 != $neues_passwort_2) 
		{
			$success = false;
			$error = true;
			$error_report = "Die Bestätigung Ihres neuen Passwortes stimmt nicht mit dem neuen Passwort überein.";
		} 
		else if ($neues_passwort_1 == $altes_passwort) 
		{
			$success = false;
			$error = true;
			$error_report = "Sie können nicht ihr momentan gültiges Passwort als neues Passwort verwenden.";
		} 
		else 
		{
			if (prüfePasswort($neues_passwort_1) != "") 
			{
				$success = false;
				$error = true;
				$error_report = prüfePasswort($neues_passwort_1);
			}
			else 
			{
				if ($weiter = true) 
				{
					$abfrage_1 = "SELECT * FROM benutzer WHERE benutzer_id='".$benutzer_id."';";
					$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
							
					while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
					{
						$datensatz_benutzer_passwort = ($datensatz_1->benutzer_passwort);
					}
				}
				else
				{
					$weiter = false;
				};
				
				if ($datensatz_benutzer_passwort != md5($altes_passwort))
				{
					$success = false;
					$error = true;
					$error_report = "Sie haben Ihr momentan gültiges Passwort falsch eingegeben.";
				} 
				else
				{	
					if ($weiter = true) 
					{
						$abfrage_2 = "SELECT COUNT(*) AS anzahl FROM benutzer_hat_verwendet_passwort 
									  WHERE (benutzer_id = '".$benutzer_id."' 
									  AND benutzer_verwendetes_passwort = '".md5($neues_passwort_1)."');";
						$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
							
						while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
						{
							$datensatz_anzahl = ($datensatz_2->anzahl);
						}
					}
					else 
					{
						$weiter = false;
					};
									
					if ($datensatz_anzahl > 0)
					{
						$success = false;
						$error = true;
						$error_report = "Sie haben dieses Passwort schon einmal verwendet.";
					}
					else 
					{
						$speichern = "UPDATE benutzer 
									  SET benutzer_passwort = '".md5($neues_passwort_1)."' 
									  WHERE benutzer_id = '".$benutzer_id."';";
						$verbindung->query($speichern);
						
						$einfügen = "INSERT INTO benutzer_hat_verwendet_passwort 
									 (benutzer_id, benutzer_verwendetes_passwort) 
									 VALUES ('".$benutzer_id."', '".md5($neues_passwort_1)."');";
						$verbindung->query($einfügen);
						
						$success = true;
						$error = false;
					};
				};
			};
		};
	};
?>

<!-- HTML - Content -->
<article id="" class="">
	<h2>
		Passwort ändern
	</h2>
	
	<style>
		#formular {
			text-align: center;
			font-family: Arial;
		}

		#fieldset {
			border: 2px solid black;
			margin-top: -10px;
		}

		#legend {
			display: block;
			width: 15%;
			padding: 0;
			border: 2px solid black;
			background-color: #D0CCCC;
			padding: 2.5px;
			padding-top: 5px;
			padding-bottom: 5px;
			font-size: 16px;
		}

		#formular table {
			text-align: center;
			margin-left: auto;
			margin-right: auto;
			margin-top: -5px;
		}

		#formular table tr {
			text-align: left;
			display: block;
			margin: 10px 0px;
		}

		#formular table td {
			text-align: left;
			display: inline;
			padding: 5px;
		}
		
		#margin_right_1 {
			margin-right: 21.2%;
		}
		
		#margin_right_2 {
			margin-right: 3.8%;
		}
		
		#formular table td input {
			text-align: center;
		}

		#formular table td .submit {
			margin-top: 10px;
			padding: 1.5% 15%;
		}

		#response {
			text-align: center;
			color: black;
			margin: 1% 2%;
		}

		#response .alert {
			padding: 10px;
		}
		
		@media screen and (max-width:1200px) {
			#legend {
				width: 30%;
			}
		}

		@media screen and (max-width:900px) {
			#legend {
				width: 30%;
			}
			
			#response {
				margin: 1% 3%;
			}
		}

		@media screen and (max-width:550px) {
			#legend {
				width: 50%;
			}
		}
		
		@media screen and (max-width:520px) {
			#formular table tr {
				text-align: center;
				display: block;
			}

			#formular table td {
				text-align: center;
				display: block;
				margin-right: 0px;
			}
			
			#margin_right_1 {
				margin-right: 0px;
			}
			
			#margin_right_2 {
				margin-right: 0px;
			}
			
			#response {
				margin: 1% 5%;
			}
		}
		
		@media screen and (max-width:360px) {
			#legend {
				width: 60%;
			}
		}

		@media screen and (max-width:320px) {
			#legend {
				width: 60%;
			}
		}
	</style>
	
	<!-- Formular zur Änderung des Passwortes des Benutzers -->
	<div id="">
		<form id='formular' method='POST' action="index.php?page=eins_pwrt_aend">
			<fieldset id="fieldset">	
				<legend id="legend">
					Passwort ändern
				</legend>
				<div style="margin: 10px; margin-top: 0px; text-align: justify;">
					Das neue Passwort muss aus 8 bis 20 Zeichen bestehen. 
					Es muss mindestens eine Ziffer, mindestens einen Klein-, mindestens einen Großbuchstaben und mindestens ein Sonderzeichen enthalten.
					Als Zeichen sind somit erlaubt: Alle lateinischen Ziffern, alle deutschen Groß- und Kleinbuchstaben (mit Umlaute), 
					sowie ( ) [ ] { } ? ! $ % & / = * + ~ , . ; : < > - _ (keine Leerzeichen).
					Das Passwort darf von Ihnen im Oberstufenportal nicht schon einmal verwendet worden sein.
				</div>
				<table>
					<tr>  
						<td>
							Momentan gültiges Passwort: ** 
						</td> 
						<td>
							<input type='password' name='altes_passwort' id="passwort" minlength="8" maxlength="20" required/>
							</td>
					</tr>
					<tr>  
						<td id="margin_right_1"> 
							Neues Passwort: * 
						</td> 
						<td> 
							<input type='password' name='neues_passwort_1' id="passwort" minlength="8" maxlength="20" required/>
						</td>
					</tr>
					<tr>  
						<td id="margin_right_2"> 
							Neues Passwort bestätigen: * 
						</td> 
						<td> 
							<input type='password' name='neues_passwort_2' id="passwort" minlength="8" maxlength="20" required/>
						</td>
					</tr>
					<tr style="text-align: center;">  
						<td colspan="2"> 
							<input type='submit' name='submit' value='Übernehmen' id='submit' class="submit"/>
						</td>
					</tr>
				</table>
				<div id="response">
					<?php 
						if ($success == true) 
						{
					?>
						<div class="alert alert-success" role="alert">
							Passwort erfolgreich geändert! 
						</div> 
					<?php 
						} 
						
						if ($error == true) 
						{
					?>
						<div class="alert alert-danger" role="alert">
							<?php echo $error_report; ?>
						</div> 
					<?php
						}
					?>
				</div>
				<div style="margin: 10px; text-align: center;">
					*  &ensp;Felder müssen ausgefüllt werden. 
					<br> 
					** &ensp;Dieses Feld dann und nur dann löschen und ausfüllen, wenn Sie sich schon erfolgreich mit Passwort angemeldet haben, 
						also nicht über "Passwort vergessen" auf diese Seite gekommen sind.
				</div>
			</fieldset>
		</form>
	</div>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>