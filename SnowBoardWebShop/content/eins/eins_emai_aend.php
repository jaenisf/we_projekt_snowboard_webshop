﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Seite zur Änderung der E-Mail-Adresse -->
<article id="" class="">
	<h2>
		E-Mail-Adresse ändern
	</h2>
	<div class="alert alert-danger" role="alert" align="center">
		Diese Seite ist derzeit in Bearbeitung!
	</div>
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>