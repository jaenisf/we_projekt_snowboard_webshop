<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/shop/shop_item.css"/>

<?php
	if(isset($_GET['action']) && $_GET['action']== "add_to_chart")
	{ 
		$artikel_id = intval($_GET['artikel_id']);
		
		if(isset($_SESSION['warenkorb'][$artikel_id]))
		{ 	
			$_SESSION['warenkorb'][$artikel_id]['menge']++;
        }
		else
		{ 
			$abfrage_1 = "SELECT COUNT(*) AS anzahl 
						  FROM artikel 
						  WHERE artikel_id='".$artikel_id."';";
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
						
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_anzahl = ($datensatz_1->anzahl);
			}
		
			if($datensatz_anzahl > 0)
			{  
				//Lesen der Artikel mit deren Attributen,
				//wobei das Filterkriterium darin liegt, alle Artikel zu lesen
				//welche ein Attribut Preis enthalten
				//Die Ausgabe wird nach der Artikelbezeichnung sortiert
				$abfrage_1 = "SELECT * FROM (artikel
							  INNER JOIN artikel_hat_attribut
							  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
							  INNER JOIN attribut
							  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
							  WHERE attribut.attribut_bezeichnung = 'Preis'
							  ORDER BY artikel.artikel_bezeichnung ASC";
						  
				$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
						
				while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
				{
					$datensatz_artikel_id = ($datensatz_1->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
					$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
				}
				
				if($_SESSION['warenkorb'][$id]['menge'] > 0)
				{
					$_SESSION['warenkorb'][$artikel_id]=array( 
							"menge" => $_SESSION['warenkorb'][$id]['menge']++, 
							"price" => $datensatz_attribut_wert 
						);
				}
				else
				{
					$_SESSION['warenkorb'][$artikel_id]=array( 
							"menge" => 1, 
							"price" => $datensatz_attribut_wert 
						);
				}
			}
			else
			{ 
				$message = "Diese Produkt-ID ist ungültig!"; 
			}
		}
		
		header("Location: index.php?page=shop_item&artikel_id=".$artikel_id);
	}
?>

<!-- HTML - Content -->
<article id="" class="">
	<?php
		if(isset($_GET['artikel_id']) == true
			&& $_GET['artikel_id'] == htmlspecialchars($_GET['artikel_id']) 
			&& is_numeric($_GET['artikel_id']) == true)
		{
			//Holen der Anzahl als count aus der Tabelle Artikel 
			//bei denen die artikel_id mit der übergebenen artikel_id übereinstimmt
			$abfrage_1 = "SELECT COUNT(*) AS anzahl FROM artikel
						  WHERE artikel_id = '".$_GET['artikel_id']."';";
				
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
							
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_anzahl = ($datensatz_1->anzahl);
			}
			
			if($datensatz_anzahl > 0)
			{
				//Lesen aller Artikel bei denen die artikel_id mit der übergebenen artikel_id übereinstimmt
				$abfrage_2 = "SELECT * FROM artikel
							  WHERE artikel_id = '".$_GET['artikel_id']."';";
					
				$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
								
				while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
				{
					$datensatz_artikel_id = ($datensatz_2->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz_2->artikel_bezeichnung);
				}
	?>
	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<!-- /.col-lg-9 -->
			<div class="col-lg-9">
				<div class="card mt-4">
					<?php
						$abfrage_3 = "SELECT * FROM (artikel
									  INNER JOIN artikel_hat_attribut
									  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
									  INNER JOIN attribut
									  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
									  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
									  AND attribut.attribut_bezeichnung = 'Bild'
									  LIMIT 1;";
								
						$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
											
						while($datensatz_3 = $datenbank_ergebnis_3->fetch_object())
						{
							$datensatz_attribut_bezeichnung_bild = ($datensatz_3->attribut_bezeichnung);
							$datensatz_attribut_wert_bild = ($datensatz_3->attribut_wert);
						}
						
						if ($datensatz_attribut_wert_bild == "")
						{
							$datensatz_attribut_wert_bild = "https://via.placeholder.com/700x700?text=Snowboard";
						}
					?>
					<img id="shop_item_img" class="card-img-top img-fluid" alt=""
						 src="<?php echo $datensatz_attribut_wert_bild; ?>" height="400px"
					>
					<div class="card-body">
						<h3 class="card-title">
							<?php echo $datensatz_artikel_bezeichnung; ?>
						</h3>
						<?php
							$abfrage_4 = "SELECT * FROM (artikel
										  INNER JOIN artikel_hat_attribut
										  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										  INNER JOIN attribut
										  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
										  AND attribut.attribut_bezeichnung = 'Preis'
										  LIMIT 1;";
								
							$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
											
							while($datensatz_4 = $datenbank_ergebnis_4->fetch_object())
							{
								$datensatz_attribut_bezeichnung_preis = ($datensatz_4->attribut_bezeichnung);
								$datensatz_attribut_wert_preis = ($datensatz_4->attribut_wert);
							}
						?>
						<h4>
							<?php echo number_format(floatval($datensatz_attribut_wert_preis) * 1.19, 2, ',', '.')."€ &emsp; inkl. Mwst., zzgl. Versandkosten"; ?>
						</h4>
						<?php
							$abfrage_5 = "SELECT * FROM (artikel
										  INNER JOIN artikel_hat_attribut
										  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										  INNER JOIN attribut
										  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
										  AND attribut.attribut_bezeichnung = 'Beschreibung'
										  LIMIT 1;";
								
							$datenbank_ergebnis_5 = $verbindung->query( $abfrage_5 );
											
							while($datensatz_5 = $datenbank_ergebnis_5->fetch_object())
							{
								$datensatz_attribut_bezeichnung_beschreibung = ($datensatz_5->attribut_bezeichnung);
								$datensatz_attribut_wert_beschreibung = ($datensatz_5->attribut_wert);
							}
						?>
						<p class="card-text">
							<?php echo $datensatz_attribut_wert_beschreibung; ?>
						</p>
						<!--<span class="text-warning">
							&#9733; &#9733; &#9733; &#9733; &#9734;
						</span>
						4.0 stars-->
						</br>
						<!--<a href="index.php?page=shop_item&artikel_id=<?php echo $datensatz_artikel_id; ?>&action=add_to_chart">
							zum Warenkorb hinzufügen
						</a>-->
						<a href="index.php?page=shop_item&artikel_id=<?php echo $datensatz_artikel_id; ?>&action=add_to_chart">
							<img class="card-img-top img-fluid" alt="" src="images/baseline_add_shopping_cart_black_18dp.png">
						</a>
						</br>
						<?php
							$abfrage_6_0 = "SELECT COUNT(*) AS anzahl FROM (artikel
										    INNER JOIN artikel_hat_attribut
								  		    ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										    INNER JOIN attribut
										    ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										    WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
										    AND attribut.attribut_bezeichnung != 'Bild'
										    AND attribut.attribut_bezeichnung != 'Preis'
										    AND attribut.attribut_bezeichnung != 'Beschreibung'
										    AND attribut.attribut_bezeichnung != 'Beschreibung_Übersicht'
										    ORDER BY artikel.artikel_bezeichnung ASC";
								
							$datenbank_ergebnis_6_0 = $verbindung->query( $abfrage_6_0 );
											
							while($datensatz_6_0 = $datenbank_ergebnis_6_0->fetch_object())
							{
								$datensatz_anzahl = ($datensatz_6_0->anzahl);
							}
							
							if ($datensatz_anzahl > 0)
							{
						?>
						<h3 class="card-title">
							Technische Daten
						</h3>
						<table>
							<?php
								$abfrage_6 = "SELECT * FROM (artikel
											  INNER JOIN artikel_hat_attribut
											  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
											  INNER JOIN attribut
											  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
											  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
											  AND attribut.attribut_bezeichnung != 'Bild'
											  AND attribut.attribut_bezeichnung != 'Preis'
											  AND attribut.attribut_bezeichnung != 'Beschreibung'
											  AND attribut.attribut_bezeichnung != 'Beschreibung_Übersicht'
											  ORDER BY artikel.artikel_bezeichnung ASC";
									
								$datenbank_ergebnis_6 = $verbindung->query( $abfrage_6 );
												
								while($datensatz_6 = $datenbank_ergebnis_6->fetch_object())
								{
									$datensatz_attribut_bezeichnung = ($datensatz_6->attribut_bezeichnung);
									$datensatz_attribut_wert = ($datensatz_6->attribut_wert);
									$datensatz_attribut_einheit = ($datensatz_6->attribut_einheit);
							?>
							<tr>
								<td width="100px">
									<?php echo $datensatz_attribut_bezeichnung; ?>
								</td>
								<td width="250px">
									<?php echo $datensatz_attribut_wert." ".$datensatz_attribut_einheit; ?>
								</td>
							</tr>
							<?php
								}
							?>
						</table>
						<?php
							}
						?>
					</div>
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col-lg-9 -->
		</div>
	</div>
	<!-- /.container -->	
	<?php
			}
		}
	?>
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>