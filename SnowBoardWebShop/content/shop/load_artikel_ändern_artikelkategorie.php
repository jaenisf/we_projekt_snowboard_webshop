﻿<?php
	require('../../content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	require('../../database/database_login.php');
?>

<?php
	$artikel_id = $_POST['artikel_id'];
	
	if (mysqli_connect_errno() == 0 && $artikel_id != "")
	{
		echo "<option value='NULL' selected> </option>";
		
		//holen aller Artikel die zu der artikel_id passen
		$abfrage = "SELECT * FROM artikel 
					WHERE artikel_id = '".$artikel_id."';";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_artikel_id = ($datensatz->artikel_id);
			$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
			$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
		};
		
		$artikelkategorie_id = $datensatz_artikelkategorie_id;
		
		//holen alles Artikelkategorien, sortiert nach der Bezeichnung
		$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung ASC;";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
			$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
			
			if ($artikelkategorie_id == $datensatz_artikelkategorie_id)
			{
				$selected = "selected";
			}
			else
			{
				$selected = "";
			}
			
			echo "<option value='".$datensatz_artikelkategorie_id."' ".$selected.">".$datensatz_artikelkategorie_bezeichnung."</option>";
		};
	}
?>
					
<?php
	require('../../database/database_logout.php');
?>

<?php
	}
	
	require('../../content/anme/check_require_anme_end.php');
?>