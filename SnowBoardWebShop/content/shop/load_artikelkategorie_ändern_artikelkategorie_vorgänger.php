﻿<?php
	require('../../content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	require('../../database/database_login.php');
?>

<?php
	$artikelkategorie_id = $_POST['artikelkategorie_id'];
	
	if (mysqli_connect_errno() == 0 && $artikelkategorie_id != "")
	{
		echo "<option value='NULL' selected> </option>";
		
		//Holen aller Artikelkategorien bei denen die artikelkategorie_id zu der übergebenen
		//artikelkategorie_id passt
		//Die Ausgabe wird dabei nach der artikelkategorie_bezeichnung sortiert
		$abfrage = "SELECT * FROM artikelkategorie
					WHERE artikelkategorie_id = '".$artikelkategorie_id."'
					ORDER BY artikelkategorie_bezeichnung;";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
			$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
			$datensatz_artikelkategorie_vorgänger = ($datensatz->artikelkategorie_vorgänger);
		}
		
		$artikelkategorie_vorgänger = $datensatz_artikelkategorie_vorgänger;
		
		//Holen aller Artikelkategorien sortiert nach der artikelkategorie_bezeichnung
		$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
			$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
			$datensatz_artikelkategorie_vorgänger = ($datensatz->artikelkategorie_vorgänger);
			
			if ($artikelkategorie_vorgänger == $datensatz_artikelkategorie_id)
			{
				$selected = "selected";
			}
			else
			{
				$selected = "";
			}
			
			if ($artikelkategorie_id != $datensatz_artikelkategorie_id)
			{
				echo "<option value='".$datensatz_artikelkategorie_id."' ".$selected.">".$datensatz_artikelkategorie_bezeichnung."</option>";
			}
		};
	}
?>
					
<?php
	require('../../database/database_logout.php');
?>

<?php
	}
	
	require('../../content/anme/check_require_anme_end.php');
?>