<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/shop/shop_ubst_fltr.css"/>

<!-- HTML - Content -->
<article id="" class="">
	<?php
		//Lesen aller artikelkategorien bei denen die artikelkategorie_id der
		//übergebenen id entspricht
		$abfrage_0 = "SELECT * FROM artikelkategorie 
					  WHERE artikelkategorie_id = '".$_GET['kategorie']."';";
		
		$datenbank_ergebnis_0 = $verbindung->query( $abfrage_0 );
						
		while($datensatz_0 = $datenbank_ergebnis_0->fetch_object())
		{
			$datensatz_artikelkategorie_id = ($datensatz_0->artikelkategorie_id);
			$datensatz_artikelkategorie_bezeichnung = ($datensatz_0->artikelkategorie_bezeichnung);
		}
		
		function get_unterkategorien($verbindung, $kategorie)
		{
			$return = "";
			
			$abfrage_0 = "SELECT * FROM artikelkategorie 
						  WHERE artikelkategorie_vorgänger = '".$kategorie."';";
			
			$datenbank_ergebnis_0 = $verbindung->query( $abfrage_0 );
							
			while($datensatz_0 = $datenbank_ergebnis_0->fetch_object())
			{
				$datensatz_artikelkategorie_id = ($datensatz_0->artikelkategorie_id);
				
				$return .= ", ".$datensatz_artikelkategorie_id;
				
				get_unterkategorien($verbindung, $datensatz_artikelkategorie_id);
			}
			
			return $return;
		}
		
		$kategorien = $datensatz_artikelkategorie_id;
		$kategorien .= get_unterkategorien($verbindung, $_GET['kategorie']);
	?>	
	<h2>
		Produktübersicht: <?php echo $datensatz_artikelkategorie_bezeichnung; ?>
	</h2>

	<div class="filterbox">
		<?php
			$abfrage_1_ohne_kategorie = "SELECT (MIN(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_min,
										 (MAX(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_max
										 FROM (artikel
										 INNER JOIN artikel_hat_attribut
										 ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										 INNER JOIN attribut
										 ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										 WHERE attribut.attribut_bezeichnung = 'Preis';";
		
			$abfrage_1_mit_kategorie = "SELECT (MIN(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_min,
										(MAX(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_max
										FROM (artikel
										INNER JOIN artikel_hat_attribut
										ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										INNER JOIN attribut
										ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										WHERE attribut.attribut_bezeichnung = 'Preis'
										AND artikel.artikelkategorie_id IN (".$kategorien.");";
						  
			if (isset($_GET['kategorie']) && $datensatz_artikelkategorie_id != "")
			{
				$abfrage_1 = $abfrage_1_mit_kategorie;
			}
			else
			{
				$abfrage_1 = $abfrage_1_ohne_kategorie;
			}
			
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
										
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_attribut_wert_min = ($datensatz_1->attribut_wert_min);
				$datensatz_attribut_wert_max = ($datensatz_1->attribut_wert_max);
			}
		?>

   		<p id="filter_überschrift">Preis:</p>
		<?php
			echo "<p id=\"filter_min_wert\">".number_format(floatval($datensatz_attribut_wert_min) * 1.19, 2, ',', '.')."€</p>"
			."<p id=\"filter_max_wert\">".number_format(floatval($datensatz_attribut_wert_max) * 1.19, 2, ',', '.')."€</p>";
		?>	
		<input type="range" class="slider" id="preis" 
			min="<?php echo number_format(floatval($datensatz_attribut_wert_min) * 1.19, 2, '.', ''); ?>" 
			max="<?php echo number_format(floatval($datensatz_attribut_wert_max) * 1.19, 2, '.', ''); ?>" 
			value="<?php echo number_format(floatval($datensatz_attribut_wert_max) * 1.19, 2, '.', ''); ?>" 
			step="0.01"
			onchange="item_search()"
			oninput="document.getElementById('preis_value').innerHTML = (document.getElementById('preis').value * 1.00).toLocaleString('de-DE', { minimumFractionDigits: 2 }) + '€';"
		>
		<div id="preis_value">
			<?php
				echo number_format(floatval($datensatz_attribut_wert_max) * 1.19, 2, ',', '.')."€";
			?>
		</div>
		
	</div>
	
	<?php
		if (isset($_GET['kategorie']) == true && $_GET['kategorie'] != ""
			&& $datensatz_artikelkategorie_bezeichnung != "Zubehör"
			&& $datensatz_artikelkategorie_bezeichnung != "Bindungen"
			&& $datensatz_artikelkategorie_bezeichnung != "Protektoren")
		{
	?>
	<div class="filterbox">
		<?php
			$abfrage_1_ohne_kategorie = "SELECT (MIN(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_min,
										 (MAX(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_max
										 FROM (artikel
										 INNER JOIN artikel_hat_attribut
										 ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										 INNER JOIN attribut
										 ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										 WHERE attribut.attribut_bezeichnung = 'Länge';";
										 
			$abfrage_1_mit_kategorie = "SELECT (MIN(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_min,
										(MAX(CAST(attribut_wert AS DOUBLE))) AS attribut_wert_max
										FROM (artikel
										INNER JOIN artikel_hat_attribut
										ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
										INNER JOIN attribut
										ON artikel_hat_attribut.attribut_id = attribut.attribut_id
										WHERE attribut.attribut_bezeichnung = 'Länge'
										AND artikel.artikelkategorie_id IN (".$kategorien.");";
			
			if (isset($_GET['kategorie']) && $datensatz_artikelkategorie_id != "")
			{
				$abfrage_1 = $abfrage_1_mit_kategorie;
			}
			else
			{
				$abfrage_1 = $abfrage_1_ohne_kategorie;
			}
			
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
										
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_attribut_wert_min = ($datensatz_1->attribut_wert_min);
				$datensatz_attribut_wert_max = ($datensatz_1->attribut_wert_max);
			}
		?>
		<p id="filter_überschrift">Länge:</p>
		<?php
			echo "<p id=\"filter_min_wert\">".$datensatz_attribut_wert_min."cm</p>"
				."<p id=\"filter_max_wert\">".$datensatz_attribut_wert_max."cm</p>";
		?>	
		<input type="range" class="slider" id="länge" 
			   min="<?php echo $datensatz_attribut_wert_min; ?>" 
			   max="<?php echo $datensatz_attribut_wert_max; ?>" 
			   value="<?php echo $datensatz_attribut_wert_max; ?>" 
			   onchange="item_search()"
			   oninput="document.getElementById('länge_value').innerHTML = document.getElementById('länge').value + 'cm';"
		>
		<div id="länge_value">
			<?php
				echo $datensatz_attribut_wert_max."cm";
			?>
		</div>
	</div>
	<?php
		}
		else
		{
	?>
		<input type="hidden" id="länge" value="">
	<?php
		}
	?>
	
	<hr style="border: 1px solid lightgray; border-radius: 2px;">
	<br>
	
	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<!-- /.col-lg-12 -->
			<div class="col-lg-12">
				<div class="row equal-height-panels" id="produktübersicht">	
				</div>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
	
	<script src="js/shop_items_matchHeight.js"></script>

	<script>
        function item_search()
        {
			var xmlhttp = new XMLHttpRequest();

            xmlhttp.onreadystatechange = function()
            {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById("produktübersicht").innerHTML = xmlhttp.responseText;
				}
				
				$(document).ready(function() {
				$('.equal-height-panels .panel').matchHeight();
			});
            }
			
			var kategorie = <?php
								if (isset($_GET['kategorie']) == true && $_GET['kategorie'] != "")
								{
									echo $_GET['kategorie'];
								}
								else
								{
									echo 0;
								}
							?>;
							
			var preis = document.getElementById("preis").value;
			var länge = document.getElementById("länge").value;
			
            xmlhttp.open("GET", "content/shop/shop_ubst_fltr_search.php?kategorie="+kategorie+"&preis="+preis+"&länge="+länge, true);
			xmlhttp.send();
        }

		item_search();

	</script>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>