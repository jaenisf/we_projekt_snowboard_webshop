<?php
	require('../../content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	require('../../database/database_login.php');
?>

<?php
	function get_unterkategorien($verbindung, $kategorie)
	{
		$return = "";
		
		$abfrage_0 = "SELECT * FROM artikelkategorie 
					  WHERE artikelkategorie_vorgänger = '".$kategorie."';";
		
		$datenbank_ergebnis_0 = $verbindung->query( $abfrage_0 );
						
		while($datensatz_0 = $datenbank_ergebnis_0->fetch_object())
		{
			$datensatz_artikelkategorie_id = ($datensatz_0->artikelkategorie_id);
			
			$return .= ", ".$datensatz_artikelkategorie_id;
			
			get_unterkategorien($verbindung, $datensatz_artikelkategorie_id);
		}
		
		return $return;
	}
	
	$kategorien = $_GET['kategorie'];
	$kategorien .= get_unterkategorien($verbindung, $_GET['kategorie']);

	$abfrage_1_ohne_kategorie_ohne_länge = "
				  SELECT DISTINCT(artikel.artikel_id), artikel.artikel_bezeichnung
				  FROM (artikel
				  INNER JOIN artikel_hat_attribut
				  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
				  INNER JOIN attribut
				  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
				  WHERE (attribut.attribut_bezeichnung = 'Preis'
				  AND attribut.attribut_wert <= CAST('".$_GET['preis']."' AS DOUBLE)) 
				  GROUP BY artikel.artikel_id
				  HAVING COUNT(artikel.artikel_id) = 1;
				";
	
	$abfrage_1_ohne_kategorie_mit_länge = "
				  SELECT DISTINCT(artikel.artikel_id), artikel.artikel_bezeichnung
				  FROM (artikel
				  INNER JOIN artikel_hat_attribut
				  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
				  INNER JOIN attribut
				  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
				  WHERE (attribut.attribut_bezeichnung = 'Preis'
				  AND attribut.attribut_wert <= CAST('".$_GET['preis']."' AS DOUBLE)) 
				  OR (attribut.attribut_bezeichnung = 'Länge'
				  AND attribut.attribut_wert <= CAST('".$_GET['länge']."' AS DOUBLE))
				  GROUP BY artikel.artikel_id
				  HAVING COUNT(artikel.artikel_id) = 2;
				";
				  
	$abfrage_1_mit_kategorie_mit_länge = "
				  SELECT DISTINCT(artikel.artikel_id), artikel.artikel_bezeichnung
				  FROM (artikel
				  INNER JOIN artikel_hat_attribut
				  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
				  INNER JOIN attribut
				  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
				  WHERE artikel.artikelkategorie_id IN (".$kategorien.")
				  AND ((attribut.attribut_bezeichnung = 'Preis'
				  AND attribut.attribut_wert <= CAST('".$_GET['preis']."' AS DOUBLE)) 
				  OR (attribut.attribut_bezeichnung = 'Länge'
				  AND attribut.attribut_wert <= CAST('".$_GET['länge']."' AS DOUBLE)))
				  GROUP BY artikel.artikel_id
				  HAVING COUNT(artikel.artikel_id) = 2;
				";
				
	$abfrage_1_mit_kategorie_ohne_länge = "
				  SELECT DISTINCT(artikel.artikel_id), artikel.artikel_bezeichnung
				  FROM (artikel
				  INNER JOIN artikel_hat_attribut
				  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
				  INNER JOIN attribut
				  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
				  WHERE artikel.artikelkategorie_id IN (".$kategorien.")
				  AND (attribut.attribut_bezeichnung = 'Preis'
				  AND attribut.attribut_wert <= CAST('".$_GET['preis']."' AS DOUBLE)) 
				  GROUP BY artikel.artikel_id
				  HAVING COUNT(artikel.artikel_id) = 1;
				";
						
	if (isset($_GET['kategorie']) == true)
	{
		if ($_GET['kategorie'] == 0)
		{
			
			if ($_GET['länge'] == "")
			{
				$abfrage_1 = $abfrage_1_ohne_kategorie_ohne_länge;
			}
			else
			{
				$abfrage_1 = $abfrage_1_ohne_kategorie_mit_länge;
			}
		}
		else
		{
			if ($_GET['länge'] == "")
			{
				$abfrage_1 = $abfrage_1_mit_kategorie_ohne_länge;
			}
			else
			{
				$abfrage_1 = $abfrage_1_mit_kategorie_mit_länge;
			}
		}
	}
	
	$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
					
	while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
	{
		$datensatz_artikel_id = ($datensatz_1->artikel_id);
		$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
		
		$abfrage_2 = "SELECT * FROM (artikel
					  INNER JOIN artikel_hat_attribut
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
					  AND attribut.attribut_bezeichnung = 'Preis'
					  LIMIT 1;";
			
		$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
						
		while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
		{
			$datensatz_attribut_bezeichnung_preis = ($datensatz_2->attribut_bezeichnung);
			$datensatz_attribut_wert_preis = ($datensatz_2->attribut_wert);
		}
		
		$abfrage_3 = "SELECT * FROM (artikel
					  INNER JOIN artikel_hat_attribut
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
					  AND attribut.attribut_bezeichnung = 'Beschreibung_Übersicht'
					  LIMIT 1;";
			
		$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
						
		while($datensatz_3 = $datenbank_ergebnis_3->fetch_object())
		{
			$datensatz_attribut_bezeichnung_beschreibung_übersicht = ($datensatz_3->attribut_bezeichnung);
			$datensatz_attribut_wert_beschreibung_übersicht = ($datensatz_3->attribut_wert);
		}
		
		if (isset($datensatz_attribut_wert_beschreibung_übersicht) == false 
			|| $datensatz_attribut_wert_beschreibung_übersicht == "")
		{
			$datensatz_attribut_wert_beschreibung_übersicht = "";
		}
		
		$abfrage_4 = "SELECT * FROM (artikel
					  INNER JOIN artikel_hat_attribut
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id = '".$datensatz_artikel_id."'
					  AND attribut.attribut_bezeichnung = 'Bild'
					  LIMIT 1;";
				
		$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );

		while($datensatz_4 = $datenbank_ergebnis_4->fetch_object())
		{
			$datensatz_attribut_bezeichnung_bild = ($datensatz_4->attribut_bezeichnung);
			$datensatz_attribut_wert_bild = ($datensatz_4->attribut_wert);
		}
		
		if (isset($datensatz_attribut_wert_bild) == false 
			|| $datensatz_attribut_wert_bild == "")
		{
			$datensatz_attribut_wert_bild = "https://via.placeholder.com/700x700?text=Snowboard";
		}
?>
	<div class="col-lg-4 col-md-6 col-mb-4">
		<div class="panel">
			<a href="#">
				<img class="card-img-top w-auto" src="<?php echo $datensatz_attribut_wert_bild; ?>"
					 alt="" height="110px" width="auto"
				>
			</a>
			<div class="panel-body">
				<h4 class="panel-title">
					<a href="index.php?page=shop_item&artikel_id=<?php echo $datensatz_artikel_id; ?>">
						<?php echo $datensatz_artikel_bezeichnung; ?>
					</a>
				</h4>
				<h5>
					<?php echo number_format(floatval($datensatz_attribut_wert_preis) * 1.19, 2, ',', '.')."€"; ?>
				</h5>
				<p class="panel-text">
					<?php echo $datensatz_attribut_wert_beschreibung_übersicht; ?>
				</p>
			</div>
			<!--<div class="card-footer">
				<small class="text-muted">
					&#9733; &#9733; &#9733; &#9733; &#9734;
				</small>
			</div>-->
		</div>
	</div>
<?php
	}
?>
					
<?php
	require('../../database/database_logout.php');
?>

<?php
	}
	
	require('../../content/anme/check_require_anme_end.php');
?>