<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/shop/shop_vwtg.css"/>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Webshop - Verwaltung
	</h2>
	<p>
	</p>
	
	<!-- Formular-Bereich -->
	<div id="verwaltungsraster">
	
		<!-- Ausgabe bei erfolgreicher Speicherung -->
		<div id="success" style="display: none;">
			<fieldset id='verwaltungselement'>
				<div class='alert alert-success' role='alert' id='' style="margin-bottom: 0px; text-align: center;"> Erfolgreich gespeichert! </div>		
			</fieldset>
		</div>
		
		<!-- Ausgabe bei falscher Einagbe -->
		<div id="information" style="display: none;">
			<fieldset id='verwaltungselement'>
				<div class='alert alert-danger' role='alert' id='verwaltungselementbezeichnung' style="margin-bottom: 0px;">
				</div>		
			</fieldset>
		</div>
		
		<!-- Neue Artikelkategorie hinzufügen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_1'])) 
				{
					$artikelkategoriebezeichnung = $_POST['artikelkategoriebezeichnung'];
					$vorgängerartikelkategorie = $_POST['vorgängerartikelkategorie'];
					
					if ($vorgängerartikelkategorie == "NULL")
					{
						$speichern = "INSERT INTO artikelkategorie 
									  (artikelkategorie_bezeichnung) 
									  VALUES 
									  ('".$artikelkategoriebezeichnung."')";
					}
					else
					{
						$speichern = "INSERT INTO artikelkategorie 
									  (artikelkategorie_bezeichnung, artikelkategorie_vorgänger) 
									  VALUES 
									  ('".$artikelkategoriebezeichnung."', '".$vorgängerartikelkategorie."')";
					}
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelkategorieliste -->	
			<?php
				$artikelkategorie_liste = "<option value='' selected> </option>";
				$artikelkategorie_liste .= "<option value='NULL'> Neue Oberkategorie </option>";
				
				$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
					$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
									
					$artikelkategorie_liste .= "<option value='".$datensatz_artikelkategorie_id."'>".$datensatz_artikelkategorie_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Neue Artikelkategorie hinzufügen -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Neue Artikelkategorie hinzufügen
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelkategoriebezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="artikelkategoriebezeichnung" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Vorgängerartikelkategorie:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='vorgängerartikelkategorie' id='' size='' required>
									<?php echo $artikelkategorie_liste; ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Hinzufügen" id="speichern" name="speichern_1">
			</fieldset>
		</form>
		
		<!-- Neuen Artikel hinzufügen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_2'])) 
				{
					$artikelbezeichnung = $_POST['artikelbezeichnung'];
					$artikelkategorie = $_POST['artikelkategorie'];
					
					$speichern = "INSERT INTO artikel
								  (artikel_bezeichnung, artikelkategorie_id)
								  VALUES
								  ('".$artikelbezeichnung."', '".$artikelkategorie."')";
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelkategorieliste -->	
			<?php
				$artikelkategorie_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
					$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
									
					$artikelkategorie_liste .= "<option value='".$datensatz_artikelkategorie_id."'>".$datensatz_artikelkategorie_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Neuen Artikel hinzufügen -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Neuen Artikel hinzufügen
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="artikelbezeichnung" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Artikelkategorie:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelkategorie' id='' size='' required>
									<?php echo $artikelkategorie_liste; ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Hinzufügen" id="speichern" name="speichern_2">	
			</fieldset>
		</form>
		
		<!-- Neues Attribut hinzufügen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_3'])) 
				{
					$artikel = $_POST['artikel'];
					$attributbezeichnung = $_POST['attributbezeichnung'];
					$attributeinheit = $_POST['attributeinheit'];
					$attributwert = $_POST['attributwert'];
					
					$abfrage = "SELECT AUTO_INCREMENT FROM information_schema.tables
								WHERE TABLE_NAME = 'attribut' AND table_schema = DATABASE();";
					$datenbank_ergebnis = $verbindung->query($abfrage);

					while ($datensatz = $datenbank_ergebnis->fetch_object()){
						$datensatz_auto_increment = ($datensatz->AUTO_INCREMENT);
					};
					
					$speichern = "INSERT INTO attribut
								  (attribut_id, attribut_bezeichnung, attribut_einheit, attribut_wert)
								  VALUES
								  ('".$datensatz_auto_increment."', '".$attributbezeichnung."', '".$attributeinheit."', '".$attributwert."')";
					
					$verbindung->query($speichern);
					
					$speichern = "INSERT INTO artikel_hat_attribut
								  (artikel_id, attribut_id)
								  VALUES
								  ('".$artikel."', '".$datensatz_auto_increment."')";
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelliste -->	
			<?php
				$artikel_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikel ORDER BY artikel_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
									
					$artikel_liste .= "<option value='".$datensatz_artikel_id."'>".$datensatz_artikel_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Neues Attribut hinzufügen -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Neues Attribut hinzufügen
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikel:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikel' id='' size='' required>
									<?php echo $artikel_liste; ?>
								</select>
							</td>
							<td>
								Attributbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="attributbezeichnung" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Attributeinheit:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="attributeinheit" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Attributwert:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="attributwert" value="" minlength="0" maxlength="22" required>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Hinzufügen" id="speichern" name="speichern_3">	
			</fieldset>
		</form>
	
		<!-- Artikelkategorie ändern -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_3'])) 
				{
					$artikelkategorie = $_POST['artikelkategorie'];
					$artikelkategoriebezeichnung = $_POST['artikelkategoriebezeichnung'];
					$artikelkategorievorgänger = $_POST['artikelkategorievorgänger'];
					
					if ($artikelkategorievorgänger == "NULL")
					{
						$speichern = "UPDATE artikelkategorie 
									  SET artikelkategorie_bezeichnung = '".$artikelkategoriebezeichnung."', 
										  artikelkategorie_vorgänger = NULL
									  WHERE artikelkategorie_id = '".$artikelkategorie."';";
					}
					else
					{
						$speichern = "UPDATE artikelkategorie 
									  SET artikelkategorie_bezeichnung = '".$artikelkategoriebezeichnung."', 
										  artikelkategorie_vorgänger = '".$artikelkategorievorgänger."'
									  WHERE artikelkategorie_id = '".$artikelkategorie."';";
					}
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelkategorieliste -->	
			<?php
				$artikelkategorie_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
					$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
									
					$artikelkategorie_liste .= "<option value='".$datensatz_artikelkategorie_id."'>".$datensatz_artikelkategorie_bezeichnung."</option>";
				};
			?>
			
			<!-- Ermittlung der Artikelkategorieparameter -->
			<script type='text/javascript'>
				function load_artikelkategorie_ändern_artikelkategorie_bezeichnung() {
					var x = document.getElementById("artikelkategorie_artikelkategorie_ändern").selectedIndex;
					var artikelkategorie = document.getElementById("artikelkategorie_artikelkategorie_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_artikelkategorie_ändern_artikelkategorie_bezeichnung.php",
						data: "artikelkategorie_id=" + artikelkategorie,
						success: function(artikelkategoriebezeichnung)
							{
								document.getElementById("artikelkategoriebezeichnung_artikelkategorie_ändern").value = artikelkategoriebezeichnung.trim();
							}
					});
				};
				
				function load_artikelkategorie_ändern_artikelkategorie_vorgänger() {
					var x = document.getElementById("artikelkategorie_artikelkategorie_ändern").selectedIndex;
					var artikelkategorie = document.getElementById("artikelkategorie_artikelkategorie_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_artikelkategorie_ändern_artikelkategorie_vorgänger.php",
						data: "artikelkategorie_id=" + artikelkategorie,
						success: function(artikelkategorievorgänger)
							{
								document.getElementById("artikelkategorievorgänger_artikelkategorie_ändern").innerHTML = artikelkategorievorgänger;
							}
					});
				};
				
				function load_artikelkategorie_ändern() {
					load_artikelkategorie_ändern_artikelkategorie_bezeichnung();
					load_artikelkategorie_ändern_artikelkategorie_vorgänger();
				};
			</script>
			
			<!-- HTML-Formular - Artikelkategorie ändern -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Artikelkategorie ändern
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelkategorie:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelkategorie' id='artikelkategorie_artikelkategorie_ändern' size='' onchange="load_artikelkategorie_ändern()" required>
									<?php echo $artikelkategorie_liste; ?>
								</select>
							</td>
							<td>
								Artikelkategoriebezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" name="artikelkategoriebezeichnung" id="artikelkategoriebezeichnung_artikelkategorie_ändern" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Vorgängerartikelkategorie:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelkategorievorgänger' id='artikelkategorievorgänger_artikelkategorie_ändern' size='' required>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Ändern" id="speichern" name="speichern_3">
			</fieldset>
		</form>
		
		<!-- Artikel ändern -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_5'])) 
				{
					$artikel = $_POST['artikel'];
					$artikelbezeichnung = $_POST['artikelbezeichnung'];
					$artikelkategorie = $_POST['artikelkategorie'];
					
					if ($artikelkategorie == "NULL")
					{
						$speichern = "UPDATE artikel 
									  SET artikel_bezeichnung = '".$artikelbezeichnung."', 
										  artikelkategorie_id = NULL
									  WHERE artikel_id = '".$artikel."';";
					}
					else
					{
						$speichern = "UPDATE artikel 
									  SET artikel_bezeichnung = '".$artikelbezeichnung."', 
										  artikelkategorie_id = '".$artikelkategorie."'
									  WHERE artikel_id = '".$artikel."';";
					}
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelliste -->	
			<?php
				$artikel_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikel ORDER BY artikel_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
									
					$artikel_liste .= "<option value='".$datensatz_artikel_id."'>".$datensatz_artikel_bezeichnung."</option>";
				};
			?>
			
			<!-- Erstellung einer Artikelkategorieliste -->	
			<?php
				$artikelkategorie_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
					$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
									
					$artikelkategorie_liste .= "<option value='".$datensatz_artikelkategorie_id."'>".$datensatz_artikelkategorie_bezeichnung."</option>";
				};
			?>
			
			<!-- Ermittlung der Artikelparameter -->
			<script type='text/javascript'>
				function load_artikel_ändern_artikelbezeichnung() {
					var x = document.getElementById("artikel_artikel_ändern").selectedIndex;
					var artikel = document.getElementById("artikel_artikel_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_artikel_ändern_artikelbezeichnung.php",
						data: "artikel_id=" + artikel,
						success: function(artikelbezeichnung)
							{
								document.getElementById("artikelbezeichnung_artikel_ändern").value = artikelbezeichnung.trim();
							}
					});
				};
				
				function load_artikel_ändern_artikelkategorie() {
					var x = document.getElementById("artikel_artikel_ändern").selectedIndex;
					var artikel = document.getElementById("artikel_artikel_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_artikel_ändern_artikelkategorie.php",
						data: "artikel_id=" + artikel,
						success: function(artikelkategorie)
							{
								document.getElementById("artikelkategorie_artikel_ändern").innerHTML = artikelkategorie;
							}
					});
				};
				
				function load_artikel_ändern() {
					load_artikel_ändern_artikelbezeichnung();
					load_artikel_ändern_artikelkategorie();
				};
			</script>
			
			<!-- HTML-Formular - Artikel ändern -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Artikel ändern
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikel:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikel' id='artikel_artikel_ändern' size='' onchange="load_artikel_ändern()" required>
									<?php echo $artikel_liste; ?>
								</select>
							</td>
							<td>
								Artikelbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" name="artikelbezeichnung" id="artikelbezeichnung_artikel_ändern" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Artikelkategorie:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelkategorie' id='artikelkategorie_artikel_ändern' size='' required>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Ändern" id="speichern" name="speichern_5">	
			</fieldset>
		</form>
		
		<!-- Attribut ändern -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_6'])) 
				{
					$artikel = $_POST['artikel'];
					$attribut = $_POST['attribut'];
					$attributbezeichnung = $_POST['attributbezeichnung'];
					$attributeinheit = $_POST['attributeinheit'];
					$attributwert = $_POST['attributwert'];
					
					$speichern = "UPDATE attribut
								  SET attribut_bezeichnung = '".$attributbezeichnung."',
									  attribut_einheit = '".$attributeinheit."',
									  attribut_wert = '".$attributwert."'
									  WHERE attribut_id = '".$attribut."';";
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Artikelliste -->	
			<?php
				$artikel_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikel ORDER BY artikel_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
									
					$artikel_liste .= "<option value='".$datensatz_artikel_id."'>".$datensatz_artikel_bezeichnung."</option>";
				};
			?>
			
			<!-- Ermittlung der in Frage kommenden Attribute -->
			<script type='text/javascript'>
				function load_attribut_ändern_mögliche_attribute() {
					var x = document.getElementById("artikel_attribut_ändern").selectedIndex;
					var artikel = document.getElementById("artikel_attribut_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_attribut_ändern_mögliche_attribute.php",
						data: "artikel_id=" + artikel,
						success: function(attribute)
							{
								document.getElementById("attribut_attribut_ändern").innerHTML = attribute;
							}
					});
					
					document.getElementById("attribut_attribut_ändern").value = '';
					document.getElementById("attributbezeichnung_attribut_ändern").value = '';
					document.getElementById("attributeinheit_attribut_ändern").value = '';
					
				};
				
				function load_attribut_ändern_attributbezeichnung() {
					var y = document.getElementById("attribut_attribut_ändern").selectedIndex;
					var attribut = document.getElementById("attribut_attribut_ändern")[y].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_attribut_ändern_attributbezeichnung.php",
						data: "attribut_id=" + attribut,
						success: function(attributbezeichnung)
							{
								document.getElementById("attributbezeichnung_attribut_ändern").value = attributbezeichnung.trim();
							}
					});
				};
				
				function load_attribut_ändern_attributeinheit() {
					var y = document.getElementById("attribut_attribut_ändern").selectedIndex;
					var attribut = document.getElementById("attribut_attribut_ändern")[y].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_attribut_ändern_attributeinheit.php",
						data: "attribut_id=" + attribut,
						success: function(attributeinheit)
							{
								document.getElementById("attributeinheit_attribut_ändern").value = attributeinheit.trim();
							}
					});
				};
				
				function load_attribut_ändern_attributwert() {
					var y = document.getElementById("attribut_attribut_ändern").selectedIndex;
					var attribut = document.getElementById("attribut_attribut_ändern")[y].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_attribut_ändern_attributwert.php",
						data: "attribut_id=" + attribut,
						success: function(attributwert)
							{
								document.getElementById("attributwert_attribut_ändern").value = attributwert.trim();
							}
					});
				};
				
				function load_attribut_ändern() {
					load_attribut_ändern_attributbezeichnung();
					load_attribut_ändern_attributeinheit();
					load_attribut_ändern_attributwert();
				};
			</script>
			
			<!-- HTML-Formular - Attribut ändern -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Attribut ändern
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikel:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikel' id='artikel_attribut_ändern' size='' onchange="load_attribut_ändern_mögliche_attribute()" required>
									<?php echo $artikel_liste; ?>
								</select>
							</td>
							<td>
								Attribut:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='attribut' id='attribut_attribut_ändern' size='' onchange="load_attribut_ändern()" required>
								</select>
							</td>
							<td>
							<td>
								Attributbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" name="attributbezeichnung" id="attributbezeichnung_attribut_ändern" value="" minlength="3" maxlength="22" required>
							</td>
							<td>
								Attributeinheit:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" name="attributeinheit" id="attributeinheit_attribut_ändern" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Attributwert:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" name="attributwert" id="attributwert_attribut_ändern" value="" minlength="0" maxlength="22" required>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Ändern" id="speichern" name="speichern_6">
			</fieldset>
		</form>
	
		<!-- Artikelkategorie entfernen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php	
				if (isset($_POST['speichern_7']))
				{	
					$artikelkategorie = $_POST['artikelkategoriebezeichnung'];
					
					$abfrage = "SELECT * FROM artikelkategorie
								WHERE artikelkategorie_vorgänger = '".$artikelkategorie."';";
					$datenbank_ergebnis = $verbindung->query($abfrage);

					while ($datensatz = $datenbank_ergebnis->fetch_object()){
						$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
						
						$speichern = "UPDATE artikelkategorie 
									  SET artikelkategorie_vorgänger = NULL
									  WHERE artikelkategorie_id = '".$datensatz_artikelkategorie_id."';";
						$verbindung->query($speichern);
					};
					
					$speichern = "DELETE FROM artikelkategorie
								  WHERE artikelkategorie_id = '".$artikelkategorie."';";
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>
			<?php
				};	
			?>
			
			<!-- Erstellung einer Artikelkategorieliste -->	
			<?php
				$artikelkategorie_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikelkategorie ORDER BY artikelkategorie_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikelkategorie_id = ($datensatz->artikelkategorie_id);
					$datensatz_artikelkategorie_bezeichnung = ($datensatz->artikelkategorie_bezeichnung);
									
					$artikelkategorie_liste .= "<option value='".$datensatz_artikelkategorie_id."'>".$datensatz_artikelkategorie_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Artikelkategorie entfernen -->
			<fieldset id='verwaltungselement'>
			
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Artikelkategorie entfernen
				</div>	
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelkategoriebezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelkategoriebezeichnung' id='' size='' required>
									<?php echo $artikelkategorie_liste; ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Entfernen" id="speichern" name="speichern_7">
			</fieldset>
		</form>
		
		<!-- Artikel entfernen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php	
				if (isset($_POST['speichern_8']))
				{	
					$artikel = $_POST['artikelbezeichnung'];
					
					$löschen_attribute_liste = "";
					
					$abfrage = "SELECT attribut_id FROM artikel_hat_attribut 
								WHERE attribut_id IN 
								(
									SELECT attribut_id FROM artikel_hat_attribut 
									WHERE artikel_id = '".$artikel."'
								)
								GROUP BY attribut_id
								HAVING COUNT(attribut_id) <= 1;";
					$datenbank_ergebnis = $verbindung->query($abfrage);
					
					while ($datensatz = $datenbank_ergebnis->fetch_object()){
						$datensatz_attribut_id = ($datensatz->attribut_id);
						
						$löschen_attribute_liste .= $datensatz_attribut_id.", ";
					};
					
					$speichern = "DELETE FROM artikel_hat_attribut 
								  WHERE artikel_id = '".$artikel."';";
					$verbindung->query($speichern);
					
					$speichern = "DELETE FROM artikel 
								  WHERE artikel_id = '".$artikel."';";
					$verbindung->query($speichern);
					
					if ($löschen_attribute_liste != "")
					{
						$löschen_attribute_liste = substr($löschen_attribute_liste, 0, -2);
					}
					
					$speichern = "DELETE FROM attribut 
								  WHERE attribut_id IN (".$löschen_attribute_liste.");";
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>
			<?php
				};	
			?>
			
			<!-- Erstellung einer Artikelliste -->	
			<?php
				$artikel_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikel ORDER BY artikel_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
									
					$artikel_liste .= "<option value='".$datensatz_artikel_id."'>".$datensatz_artikel_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Artikel entfernen -->
			<fieldset id='verwaltungselement'>
			
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Artikel entfernen
				</div>	
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelbezeichnung' id='' size='' required>
									<?php echo $artikel_liste; ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Entfernen" id="speichern" name="speichern_8">			
			</fieldset>
		</form>
		
		<!-- Attribut entfernen -->
		<form method='post' action="index.php?page=shop_vwtg" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php	
				if (isset($_POST['speichern_9']))
				{	
					$artikel = $_POST['artikelbezeichnung'];
					$attribut = $_POST['attributbezeichnung'];
					
					$speichern = "DELETE FROM artikel_hat_attribut 
								  WHERE artikel_id = '".$artikel."' AND attribut_id = '".$attribut."';";
					$verbindung->query($speichern);
					
					$abfrage = "SELECT COUNT(*) AS anzahl FROM artikel_hat_attribut
								WHERE attribut_id = '".$attribut."';";
					$datenbank_ergebnis = $verbindung->query($abfrage);

					while ($datensatz = $datenbank_ergebnis->fetch_object()){
						$datensatz_anzahl = ($datensatz->anzahl);
					};
					
					if ($datensatz_anzahl == 0)
					{
						$speichern = "DELETE FROM attribut WHERE attribut_id = '".$attribut."';";
						$verbindung->query($speichern);
					}
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>
			<?php
				};	
			?>
			
			<!-- Erstellung einer Artikelliste -->	
			<?php
				$artikel_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM artikel ORDER BY artikel_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
									
					$artikel_liste .= "<option value='".$datensatz_artikel_id."'>".$datensatz_artikel_bezeichnung."</option>";
				};
			?>
			
			<!-- Ermittlung der in Frage kommenden Attribute -->
			<script type='text/javascript'>
				function load_attribut_entfernen_mögliche_attribute() {
					var x = document.getElementById("artikelbezeichnung_attribut_entfernen").selectedIndex;
					var artikel = document.getElementById("artikelbezeichnung_attribut_entfernen")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/shop/load_attribut_entfernen_mögliche_attribute.php",
						data: "artikel_id=" + artikel,
						success: function(attribute)
							{
								document.getElementById("attributbezeichnung_attribut_entfernen").innerHTML = attribute;
							}
					});
				};
			</script>
			
			<!-- HTML-Formular - Attribut entfernen -->
			<fieldset id='verwaltungselement'>
			
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Attribut entfernen
				</div>	
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Artikelbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='artikelbezeichnung' id='artikelbezeichnung_attribut_entfernen' size='' onchange="load_attribut_entfernen_mögliche_attribute()" required>
									<?php echo $artikel_liste; ?>
								</select>
							</td>						
							<td>
								Attributbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='attributbezeichnung' id='attributbezeichnung_attribut_entfernen' size='' required>
									<?php echo $a; ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Entfernen" id="speichern" name="speichern_9">			
			</fieldset>
		</form>
	</div>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>