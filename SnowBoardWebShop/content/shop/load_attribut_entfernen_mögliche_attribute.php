﻿<?php
	require('../../content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	require('../../database/database_login.php');
?>

<?php
	$artikel_id = $_POST['artikel_id'];
	
	if (mysqli_connect_errno() == 0)
	{
		echo "<option value='' selected> </option>";
		
		//Diese Abfrage liest die Daten aus der Verbindung der Tabellen attribut und artikel_hat_attribut
		//wobei die beiden Tabellen über die attribut_id miteinander Verknüpft werden
		//dabei wird noch nach der übergebenen artikel_id gefiltert.
		//Das Ergebnis wird anschließend noch nach der artikel_bezeichnung sortiert
		$abfrage = "SELECT * FROM attribut 
					INNER JOIN artikel_hat_attribut
					ON attribut.attribut_id = artikel_hat_attribut.attribut_id
					WHERE artikel_hat_attribut.artikel_id = '".$artikel_id."'
					ORDER BY attribut_bezeichnung;";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_attribut_id = ($datensatz->attribut_id);
			$datensatz_attribut_bezeichnung = ($datensatz->attribut_bezeichnung);
								
			echo "<option value='".$datensatz_attribut_id."'>".$datensatz_attribut_bezeichnung."</option>";
		};
	}
?>
					
<?php
	require('../../database/database_logout.php');
?>

<?php
	}
	
	require('../../content/anme/check_require_anme_end.php');
?>