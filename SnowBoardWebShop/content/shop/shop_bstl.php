﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Bestellungen -->
<article id="" class="">
	<h2>
		Bestellungen
	</h2>
	
	<?php
		//Lesen aller Bestellungen, sortiert nach Datum und Uhrzeit
		$abfrage = "SELECT * FROM bestellung ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
					
		$datenbank_ergebnis = $verbindung->query($abfrage);

		while ($datensatz = $datenbank_ergebnis->fetch_object())
		{
			$datensatz_bestellung_id = ($datensatz->bestellung_id);
			
			if (isset($_POST['submit_'.$datensatz_bestellung_id]) == true)
			{
				//Update der Tabelle Bestellung, hierbei wird das Feld
				//bestellung_bezahlung_erfolgreich auf 1 gesetzt
				//für alle Bestellungen bei denen die bestellung_id mit der datensatz_bestellung_id
				//übereinstimmt
				$speichern = "UPDATE bestellung SET bestellung_bezahlung_erfolgreich = 1
							  WHERE bestellung_id = '".$datensatz_bestellung_id."';";
						
				$verbindung->query($speichern);
			}
		}
	?>
	
	<!-- Ausgabe der Bestellungen -->
	<h3> <u> Bestellungen: </u> </h3>
	<div style="overflow-x:auto;">
		<table class="table table-dark">
			<thead>
				<tr>
					<th> Bestellungnummer </th>
					<th> Kunde </th>
					<th> Bezahlungsart </th>
					<th> Bestellungsdatum </th>
					<th> Bestellungsuhrzeit </th>
					<th> Gesamtpreis </th>
					<th> Bestellstatus </th>
				</tr>
			</thead>
			<tbody>
		<?php
			//Lesen aller Bestellungen in Komnination mit den Tabellen
			//kunde, benutzer und bezahlungsart
			//Die Daten werden dabei nach Datum und Uhrzeit sortiert
			$abfrage = "SELECT * FROM bestellung
						INNER JOIN kunde
						ON bestellung.kunde_id = kunde.kunde_id
						INNER JOIN benutzer
						ON kunde.benutzer_id = benutzer.benutzer_id
						INNER JOIN bezahlungsart
						ON bestellung.bezahlungsart_id = bezahlungsart.bezahlungsart_id
						ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
						
			$datenbank_ergebnis = $verbindung->query($abfrage);

			while ($datensatz = $datenbank_ergebnis->fetch_object()){
				$datensatz_bestellung_id = ($datensatz->bestellung_id);
				$datensatz_bezahlungsart_bezeichnung = ($datensatz->bezahlungsart_bezeichnung);
				$datensatz_bestellung_datum = ($datensatz->bestellung_datum);
				$datensatz_bestellung_uhrzeit = ($datensatz->bestellung_uhrzeit);
				$datensatz_bestellung_bestellungswert = ($datensatz->bestellung_bestellungswert);
				$datensatz_bestellung_bezahlung_erfolgreich = ($datensatz->bestellung_bezahlung_erfolgreich);
				$datensatz_bestellung_payment_id = ($datensatz->paypal_payment_id);
				$datensatz_bestellung_token = ($datensatz->paypal_token);
				$datensatz_bestellung_payer_id = ($datensatz->paypal_payer_id);
				$datensatz_benutzer_vorname = ($datensatz->benutzer_vorname);
				$datensatz_benutzer_nachname = ($datensatz->benutzer_nachname);
				
				if ($datensatz_bestellung_bezahlung_erfolgreich == 1)
				{
					$datensatz_bestellung_bezahlung_erfolgreich_darstellung = "<b style='color: green;'> &#10003; </b>";
				}
				else
				{
					$datensatz_bestellung_bezahlung_erfolgreich_darstellung = "<b style='color: red;'> &#10007; </b>";
				}
				
				if ($datensatz_bezahlungsart_bezeichnung == "Paypal")
				{
					$route = "user_bzhl_pypl_exec";
				}
				else if ($datensatz_bezahlungsart_bezeichnung == "Rechnung")
				{
					$route = "user_bzhl_rchg_exec";
				}
				else if ($datensatz_bezahlungsart_bezeichnung == "Barzahlung")
				{
					$route = "user_bzhl_barz_exec";
				}
				else
				{
					$route = "";
				}
				
				if ($route == "")
				{
					$url = "";
				}
				else
				{
					$url = "index.php?page=".$route."&success=true&paymentId=".$datensatz_bestellung_payment_id."&token=".$datensatz_bestellung_token."&PayerID=".$datensatz_bestellung_payer_id;
				}
		?>
				<tr>
					<td style="padding-left: 25px;"> 
						<?php 
								echo $datensatz_bestellung_id; 
								echo "&emsp;(<a href='".$url."'>weitere Details</a>)";
						?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_benutzer_vorname." ".$datensatz_benutzer_nachname; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bezahlungsart_bezeichnung; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_datum; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_uhrzeit; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($datensatz_bestellung_bestellungswert * 1.19 + 9.99, 2, ',', '.')."€"; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_bezahlung_erfolgreich_darstellung; ?>
						&emsp;
						&emsp;
						<?php
							if ($datensatz_bestellung_bezahlung_erfolgreich == 0)
							{
						?>
						<form action="index.php?page=shop_bstl" method="post" style="display: inline-block;">
							<input type="submit" name="<?php echo "submit_".$datensatz_bestellung_id; ?>" value="&#10003;" style="padding: 3px; border-radius: 10px;">
						</form>
						<?php
							}
						?>
					</td>
				</tr>
		<?php
				
			};
		?>
			<tbody>
		</table>
	</div>
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>