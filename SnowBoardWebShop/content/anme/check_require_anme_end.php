﻿<?php		
	//Seite die angezeigt wird, wenn sich für den Inhalt zuerst eingeloggt werden muss.
	if ($access == false) {
?>
<div class='alert alert-danger' role='alert' id='aspekt_bezeichnung' align="center"> 
	Sie müssen sich erst anmelden, um diesen Inhalt sehen zu können.
	<br>
	&emsp; &#8594
	<a href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")."://".$_SERVER['HTTP_HOST'].explode('?', $_SERVER['REQUEST_URI'], 2)[0]."?page=anme"; ?>"
	   style="color: #a94442; text-decoration: underline;"
	>
		Anmeldung
	</a>
</div>
<?php
	};
?>

