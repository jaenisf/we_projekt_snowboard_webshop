﻿<?php
	if ($_SESSION['benutzer_id'] > 0) {
		//Ermitteln und setzen des logout datums und der letzten Anmeldung eines Benutzers
		date_default_timezone_set("Europe/Berlin");
		$timestamp = time();
						
		$benutzer_id = $_SESSION['benutzer_id'];
		$benutzer_logout_date = date("d.m.Y",$timestamp);
		$benutzer_logout_time = date("H:i:s",$timestamp);
		
		$speichern = "UPDATE benutzer_login_details 
					  SET benutzer_status = '0',
					  benutzer_logout_datum = '".$benutzer_logout_date."',
					  benutzer_logout_uhrzeit = '".$benutzer_logout_time."',
					  benutzer_letzte_anmeldung_datum = '".$benutzer_logout_date."',
					  benutzer_letzte_anmeldung_uhrzeit = '".$benutzer_logout_time."'
					  WHERE benutzer_id = '".$benutzer_id."';";
		$verbindung->query($speichern);
		
		$weiter = true;
	
		$abfrage_1 = "SELECT * FROM benutzer_login_details 
					  WHERE benutzer_id = '".$benutzer_id."';";																						
		$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
					
		while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
		{
			$datensatz_login_date = ($datensatz_1->login_datum);
			$datensatz_login_time = ($datensatz_1->login_uhrzeit);
			$datensatz_logout_date = ($datensatz_1->logout_datum);
			$datensatz_logout_time = ($datensatz_1->logout_uhrzeit);
		}
		
		$login_date = strtotime($datensatz_login_date);
		$login_time = strtotime($datensatz_login_time);
		$logout_date = strtotime($datensatz_logout_date);
		$logout_time = strtotime($datensatz_logout_time);
		
		$dauerInStunden = floor(($logout_time - $login_time)/3600);
		$dauerInMinuten = floor(($logout_time - $login_time - (3600 * $dauerInStunden))/60);
		$dauerInSekunden = floor(($logout_time - $login_time - (3600 * $dauerInStunden) - (60 * $dauerInMinuten)));
		
		if ($dauerInStunden < 0) {
			
			$dauerInStunden = $dauerInStunden + 24;
		}
		
		$dauerInJahre = floor(($logout_date - $login_date)/31536000);
		$dauerInTage = floor(($logout_date - $login_date - (31536000 * $dauerInJahre))/86400);
		
		$years = $dauerInJahre."a";
		$days = $dauerInTage."d";
		$hours = $dauerInStunden."h";
		$minutes = $dauerInMinuten."min";
		$seconds = $dauerInSekunden."s";
		
		if ($dauerInJahre == 1) { $years = "1a"; }
		if ($dauerInTage == 1) { $days = "1d"; }
		if ($dauerInStunden == 1) { $hours = "1h"; }
		if ($dauerInMinuten == 1) { $minutes = "1min"; }
		if ($dauerInSekunden == 1) { $seconds = "1s"; }
		
		if ($dauerInJahre == 0) { $years = ""; }
		if ($dauerInTage == 0) { $days = ""; }
		if ($dauerInStunden == 0) { $hours = ""; }
		if ($dauerInMinuten == 0) { $minutes = ""; }
		if ($dauerInSekunden == 0) { $seconds = ""; }
		
		$login_duration = $years." ".$days." ".$hours." ".$minutes." ".$seconds;
		
		session_destroy();
		header("Location: index.php?page=anme");
	}
	
	session_destroy();

	session_start();

	if (isset($_GET["page"])) 
	{
		if ($_GET["page"] == "anme")
		{
			//Vorbereiten des Logins für einen Benutzer und Anzeigen des Anmeldeformulars
			$benutzername = strtolower($_POST['benutzername']);
			$passwort = md5($_POST['passwort']);
			
			if ($benutzername == "" OR  $passwort == "")
			{
?>
		
		<!-- Anmeldeformular -->
<div id="login_area">
	<form id='anmeldung_formular' method='POST' action="index.php?page=anme">
		<fieldset id="anmeldung_fieldset">	
			<legend id="anmeldung_legend">
				Zugangsdaten
			</legend>
			<table>
				<tr>  
					<td>
						Benutzername:
					</td>  
					<td> 
						<input type='text' name='benutzername' id="benutzername" minlength="5" required/> 
					</td>
				</tr>
				<tr>  
					<td>
						Passwort: 
					</td> 
					<td>
						<input type='password' name='passwort' id="passwort" minlength="8" maxlength="20" required/> 
					</td>
				</tr>
				<tr>  
					<td colspan="2">
						<input type='submit' name='login' value='Anmelden' id='submit_login' class="submit_login"/>
					</td>
				</tr>
				<tr>  
					<td colspan="2"> 
						<div id="response">
							<?php 
								if ($login_duration != "")
								{
							?>
								<div class="alert alert-success" role="alert">
									Erfolgreich ausgeloggt!
								</div> 
								<div class="alert alert-info" role="alert">
									Login Time:
									<?php
										echo $login_duration;
									?>  
								</div> 
							<?php 
								}
							?>
						</div> 
					</td>
				</tr>
			</table>
		</fieldset>
	</form>
</div>

<?php	
			} 
			else
			{
				if ($weiter == true)
				{
					$abfrage_1 = "SELECT COUNT(*) AS anzahl FROM benutzer
								  WHERE (benutzer_benutzername = '".md5($benutzername)."'
										 OR MD5(benutzer_email_adresse) = '".md5($benutzername)."')
										AND benutzer_passwort = '".$passwort."';";		
					$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
							
					while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
					{
						$datensatz_anzahl = ($datensatz_1->anzahl);
					}
				}
				else 
				{
					$weiter == false;
				};
													
				if ($datensatz_anzahl == 1)
				{
					$abfrage_2 = "SELECT * FROM benutzer
								  WHERE (benutzer_benutzername = '".md5($benutzername)."'
										 OR MD5(benutzer_email_adresse) = '".md5($benutzername)."')
										AND benutzer_passwort = '".$passwort."';";			
					$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
							
					while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
					{
						$datensatz_benutzer_id = ($datensatz_2->benutzer_id);
						$datensatz_benutzer_benutzername = ($datensatz_2->benutzer_benutzername);
						$datensatz_benutzer_passwort = ($datensatz_2->benutzer_passwort);
						$datensatz_benutzer_email_adresse = ($datensatz_2->benutzer_email_adresse);
					}
					
					if ($weiter == true)
					{
						$abfrage_3 = "SELECT * FROM benutzer_login_details 
									  WHERE benutzer_id = '".$datensatz_benutzer_id."';";
						$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
							
						while($datensatz_3 = $datenbank_ergebnis_3->fetch_object()) {
							$datensatz_benutzer_verbleibende_anmeldeversuche = ($datensatz_3->benutzer_verbleibende_anmeldeversuche);
						}
					}
					else
					{
						$weiter == false;
					};
				}
				else
				{	
					$datensatz_benutzername = "";
					$datensatz_passwort = "";
					$datensatz_email_adresse = "";
				};
				
				if (((md5($benutzername) == $datensatz_benutzer_benutzername OR $benutzername == $datensatz_benutzer_email_adresse) AND $passwort == $datensatz_benutzer_passwort) 
					 AND $datensatz_benutzer_verbleibende_anmeldeversuche > 0)
				{	
					//Ermitteln und setzen von Logindatum / Loginuhrzeit für einen Benutzer
					date_default_timezone_set("Europe/Berlin");
					$timestamp = time();
					
					$_SESSION["benutzer_id"] = $datensatz_benutzer_id;
					
					$benutzer_id = $_SESSION['benutzer_id'];
					$benutzer_login_date = date("d.m.Y", $timestamp);
					$benutzer_login_time = date("H:i:s", $timestamp);
					
					$speichern = "UPDATE benutzer_login_details
								  SET benutzer_status = '1', 
								  benutzer_login_datum = '".$benutzer_login_date."', 
								  benutzer_login_uhrzeit = '".$benutzer_login_time."', 
								  benutzer_verbleibende_anmeldeversuche = '10' 
								  WHERE benutzer_id = '".$datensatz_benutzer_id."';";
					$verbindung->query($speichern);
					
					if ($weiter == true)
					{
						$abfrage_4 = "SELECT * FROM benutzer_login_details 
									  WHERE benutzer_id = '".$datensatz_benutzer_id."';";
						$datenbank_ergebnis_4 = $verbindung->query($abfrage_4);
						
						while($datensatz_4 = $datenbank_ergebnis_4->fetch_object())
						{
							$datensatz_benutzer_letzte_seite = ($datensatz_4->benutzer_letzte_seite);
						}
					}
					else
					{
						$weiter == false;
					};
					
					header("Location: index.php?page=".$datensatz_benutzer_letzte_seite);
					exit();
				}
				else
				{
				//In diesem Block wird noch die Eingabe einer falschen Benutzername / Passwort Kombination abgehandelt
					$benutzername = $_POST['benutzername'];
					
					if ($weiter == true)
					{
						$abfrage_5 = "SELECT * FROM benutzer WHERE benutzer_benutzername = '".md5($benutzername)."';";
						$datenbank_ergebnis_5 = $verbindung->query($abfrage_5);
						
						while($datensatz_5 = $datenbank_ergebnis_5->fetch_object())
						{
							$datensatz_benutzer_id = ($datensatz_5->benutzer_id);
						}
					}
					else
					{
						$weiter == false;
					};
					
					if ($weiter == true)
					{
						$abfrage_6 = "SELECT * FROM benutzer_login_details 
									  WHERE benutzer_id = '".$datensatz_benutzer_id."';";
						$datenbank_ergebnis_6 = $verbindung->query($abfrage_6);
						
						while($datensatz_6 = $datenbank_ergebnis_6->fetch_object())
						{
							$datensatz_benutzer_verbleibende_anmeldeversuche = ($datensatz_5->verbleibende_anmeldeversuche);
						}
					}
					else
					{
						$weiter == false;
					};
					
					$datensatz_benutzer_verbleibende_anmeldeversuche--;
					
					$speichern = "UPDATE benutzer_login_details 
								  SET verbleibende_anmeldeversuche = '".$datensatz_benutzer_verbleibende_anmeldeversuche."' 
								  WHERE id = '".$datensatz_benutzer_id."'";
					$verbindung->query($speichern);	
?>

<div id="login_area">
	<form id='anmeldung_formular' method='POST' action="index.php?page=anme">
		<fieldset id="anmeldung_fieldset">	
			<legend id="anmeldung_legend">
			Zugangsdaten
			</legend>
			<table>
					<tr>  
						<td>
							Benutzername:
						</td>  
						<td>
							<input type='text' name='benutzername' id="benutzername" minlength="5" required/>
						</td>
					</tr>
					<tr>  
						<td>
							Passwort:
						</td> 
						<td>
							<input type='password' name='passwort' id="passwort" minlength="5" required/>
						</td>
					</tr>
						<tr>  
						<td colspan="2">
							<input type='submit' name='login' value='Anmelden' id='submit_login' class="submit_login"/>
						</td>
					</tr>
				<?php
					if ($benutzer_id != "" AND $datensatz_login_verbleibende_anmeldeversuche > 1)
					{
				?>
					<tr>  
						<td colspan="2">
							<div id="response"> 
								<div class="alert alert-danger" role="alert"> 
									Anmeldedaten
									<br>
									sind ungültig!
									<hr style="margin: 10px 0px; background-color: #a94442;">
									Falsches Passwort:
									<br>
									Sie haben noch
									<?php echo $datensatz_login_verbleibende_anmeldeversuche; ?>
									<br>
									Anmeldeversuche.
								</div>
							</div> 
						</td>
					</tr>
				<?php
					} 
					else if ($benutzer_id != "" AND $datensatz_login_verbleibende_anmeldeversuche == 1)
					{
				?>
					<tr>  
						<td colspan="2">
							<div id="response">
								<div class="alert alert-danger" role="alert">
									Anmeldedaten
									<br>
									sind ungültig!
									<hr style="margin: 10px 0px; background-color: #a94442;">
									Falsches Passwort:
									<br>
									Sie haben noch einen
									<br>
									Anmeldeversuch.
								</div>
							</div> 
						</td>
					</tr>
				<?php
					} 
					else if ($benutzer_id != "" AND $datensatz_login_verbleibende_anmeldeversuche <= 0)
					{
				?>
					<tr>  
						<td colspan="2"> 
							<div id="response">
								<div class="alert alert-danger" role="alert">
									Anmeldedaten
									<br>
									sind ungültig! 
									<hr style="margin: 10px 0px; background-color: #a94442;">
									Falsches Passwort:
									<br>
									Sie haben alle
									<br>
									Anmeldeversuche
									<br>
									aufgebraucht.
								</div>
							</div> 
						</td>
					</tr>
				<?php
					} 
					else
					{
				?>
					<tr>  
						<td colspan="2"> 
							<div id="response">
								<div class="alert alert-danger" role="alert"> 
									Anmeldedaten 
									<br>
									sind ungültig! 
								</div> 
							</div>
						</td>
					</tr>
				<?php
					};
				?>
			</table>
		</fieldset>
	</form>
</div>

<?php
				};
			};
		};
	};
?>