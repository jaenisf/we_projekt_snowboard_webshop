﻿<?php  
	require('content/anme/check_require_anme_beginn.php');
?>  

<link rel="stylesheet" type="text/css" href="css/footer/addition.css" />

<!-- Kontaktformular -->
<article id="" class="">
	<h2> Kontakt </h2>
	<p> 
	</p>
	
	<fieldset style="border: 2px solid black; padding: 10px; margin-bottom: 10px;">
		<style>
			#table {
				margin-left: auto;
				margin-right: auto;
				width: auto;
			}
			
			#th {
				border: 1.6px solid black;
				text-align: center;
				background-color: lightgray;
				padding: 5px 20px;
			}
			
			#td {
				border: 1px solid black;
				text-align: center;
				padding: 5px 20px;
				color: black;
			}
			
			textarea {
				width: 275%;
				resize: none; 
				overflow-y: scroll; 
				overflow-x: hidden; 
				padding: 5px; 
				margin: 10px; 
				text-align: left; 
				height: 80px;
			}
			
			/* responsive Webdesign */
	
			@media screen and (max-width:750px) {
				#table {
					margin-left: auto;
					margin-right: auto;
					width: 100%;
				}
				
				#th, #td {
					display: block;
					width: 100%;
				}
				
				#tr {
					width: 100%;
				}
				
				textarea {
					width: auto;
				}
			}
		</style>
			
		<div class='alert alert-warning' role='alert' id='aspekt_bezeichnung'>
			Kontaktmöglichkeit
		</div>				
			<table id="table">
				<tr id="tr">
					<th id="th" class='alert alert-info'> 
						
					</th>
					<th id="th" class='alert alert-info'> 
						Kontaktdaten 
					</th>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Firma 
					</td>
					<td id='td'>
						HeidiSnowBoards
					</td>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Anschrift
					</td>
					<td id='td'>
						Musterstraße 1
						<br/>
						D-90598 Musterstadt
					</td>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Telefon
					</td>
					<td id='td'>
						0911/1234
					</td>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Fax
					</td>
					<td id='td'>
						0911/456789
					</td>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Kontaktperson(en)
					</td>
					<td id='td'>
						Heidi Musterfrau
					</td>
				</tr>
				<tr id='tr'>
					<td id='td'>
						Email-Adresse
					</td>
					<td id='td'>
						shop@heidisnowboards.de
					</td>
				</tr>
		</table>			
	</fieldset>
	<form method='post' action="index.php?page=kont">
		<style>
			#tr2 {
				border: 1px solid black;
			}
			
			input {
				text-align: center;
				font-size: 16px;
			}
		</style>
		
		<?php	
			if (isset($_POST['speichern'])) {
				$name = $_POST['name'];
				$email_benutzer = $_POST['email'];
				$mailto = 'shop@heidisnowboards.de';
				$a = 'shop@heidisnowboards.de';
				$nachricht = $_POST['nachricht'];
				
				$kontakte = array("webmaster@localhost.de", "webmaster@localhost.de");
				
				$text_anrede = array("Sehr geehrter Kontakt_1,", "Sehr geehrter Kontakt_2,");				
				$text_notiz = "über das Kontaktformular der Website SG-online (kurslehrerwahl.sg-hdh.de) wurde die folgende Nachricht automatisch an Sie versendet.";	
				$text_grussformel = "Mit freundlichen Grüßen <br><br> Ihr Webmaster";
								
				$empfänger = $kontakte[$a-1];
				$betreff = "Kontaktformular-Nachricht von Snowboard-Webshop";
				$from = "From: 	Webmaster: localhost <webmaster@localhost.de>\r\n";
				$from .= "Reply-To: \r\n";
				$from .= "Content-Type: text/html; charset=utf-8\r\n";
				$text = $text_anrede[$a-1]."<br><br>".$text_notiz."<br><br><hr><br>".$nachricht."<br><br><hr><br>".$text_grussformel;
	 
				mail($empfänger, $betreff, $text, $from);
			};		
		?>	
		
		<fieldset id='bewertungsaspekt'>
			<div class='alert alert-warning' role='alert' id='aspekt_bezeichnung'>
				Kontaktformular
			</div>				
			<table id='prioritaet' style="margin-left: auto; margin-right: auto; width: auto;">
				<tbody>
					<tr id="tr2">
						<td style="border: 0px;">
							Ihr Name: *
						</td>
						<td style="border: 0px; padding-right: 25px;"> 
							<input type="text" id="" name="name" value="" required>
						</td>
						<td style="border: 0px;">
							Ihre Email-Adresse: * 
						</td>
						<td style="border: 0px; padding-right: 25px;"> 
							<input name='email' id='email' required>
						</td>
					</tr>
					<tr id="tr2">
						<td style="border: 0px;">
							Nachricht:
						</td>
						<td style="border: 0px; padding-right: 25px;"> 
							<textarea name="nachricht">
							</textarea>
						</td>
					</tr>	
				</tbody>
			</table>	
			<div style="margin: 10px; margin-left: auto; margin-right: auto; text-align: center;">
				&emsp; *  &ensp;Felder müssen ausgefüllt werden.
			</div>	
			<input type="submit" value="Senden" id="speichern" name="speichern">			
		</fieldset>	
	</form>
</article>