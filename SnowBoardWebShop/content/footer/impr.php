﻿<?php 
	require('content/anme/check_require_anme_beginn.php');
?>   

<link rel="stylesheet" type="text/css" href="css/footer/addition.css" />

<!-- Impressum welches vom Generator datenschutz-generator.de generiert wurde -->
<article id="" class="">
	<h2>
		Impressum
	</h2>
	</br>
	<h3 id="m46">
		Diensteanbieter
	</h3>
	<p>
		HeidiSnowBoards
	</p>
	<p>
		Musterstraße 1
	</p>
	<p>
		90598 Musterstadt
	</p>
	</br>
	<h3 id="m56">
		Kontaktmöglichkeiten
	</h3>
	<p>
		<strong>
			E-Mail-Adresse
		</strong>
		:
		<a href="mailto:shop@heidisnowboards.de">
			shop@heidisnowboards.de
		</a>
	</p>
	<p>
		<strong>
			Telefon
		</strong>
		: 0911 1234
	</p>
	<p>
		<strong>
			Fax
		</strong>
		: 0911 456789
	</p>
	<p class="seal">
		<a href="https://datenschutz-generator.de/?l=de" target="_blank" rel="noopener noreferrer nofollow"
		   title="Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken." 
		>
			Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas Schwenke
		</a>
	</p>
</article>