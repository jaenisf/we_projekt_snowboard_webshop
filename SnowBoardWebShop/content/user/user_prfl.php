﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Profil -->
<article id="" class="">
	<h2>
		Profil
	</h2>
	<?php
		//require('content/white2.php');
	?>
	
	<form class="form" action="##" method="post" id="user_prfl">
		<style>
			.col-xs-6 {
				margin-bottom: 10px;
			}
		</style>
		<?php
			//Holen der Daten zum Benutzer der übergebenen id
			$abfrage_1 = "SELECT * FROM benutzer WHERE benutzer_id = '".$benutzer_id."'";
					
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
								
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_benutzer_id = ($datensatz_1->benutzer_id);
				$datensatz_benuzter_anrede = ($datensatz_1->benutzer_anrede);
				$datensatz_benutzer_vorname = ($datensatz_1->benutzer_vorname);
				$datensatz_benutzer_nachname = ($datensatz_1->benutzer_nachname);
				$datensatz_benutzer_benutzername_unverschlüsselt = ($datensatz_1->benutzer_benutzername_unverschlüsselt);
				$datensatz_benutzer_email_adresse = ($datensatz_1->benutzer_email_adresse);
				$datensatz_benutzer_geburtsdatum = ($datensatz_1->benutzer_geburtsdatum);
			}
		?>		
		<!-- Ausgabe der Daten -->
		<div class="form-group">
			<div class="col-xs-6" >
				<label for="anrede">
					<h4>
						Anrede
					</h4>
				</label>
				<input type="text" class="form-control" name="anrede" id="anrede" 
					   placeholder="Anrede" title="" readonly
					   value="<?php echo $datensatz_benuzter_anrede; ?>"
				>
			</div>
			<div class="col-xs-6" style="height: 78px;">
			</div>
			<div class="col-xs-6">
				<label for="vorname">
					<h4>
						Vorname
					</h4>
				</label>
				<input type="text" class="form-control" name="vorname" id="vorname" 
					   placeholder="Vorname" title="" readonly
					   value="<?php echo $datensatz_benutzer_vorname; ?>"
				>
			</div>
			<div class="col-xs-6">
				<label for="nachname">
					<h4>
						Nachname
					</h4>
				</label>
				<input type="text" class="form-control" name="nachname" id="nachname" 
					   placeholder="Vorname" title="" readonly
					   value="<?php echo $datensatz_benutzer_nachname; ?>"
				>
			</div>
			<br style="float: left;">
			<div class="col-xs-6">
				<label for="benutzername">
					<h4>
						Benutzername
					</h4>
				</label>
				<input type="text" class="form-control" name="benutzername" id="benutzername" 
					   placeholder="Benutzername" title="" readonly
					   value="<?php echo $datensatz_benutzer_benutzername_unverschlüsselt; ?>"
				>
			</div>
			<div class="col-xs-6">
				<label for="email-adresse">
					<h4>
						Email-Adresse
					</h4>
				</label>
				<input type="text" class="form-control" name="email-adresse" id="email-adresse" 
					   placeholder="Email-Adresse" title="" readonly
					   value="<?php echo $datensatz_benutzer_email_adresse; ?>"
				>
			</div>
			<div class="col-xs-6">
				<label for="geburtsdatum">
					<h4>
						Geburtsdatum
					</h4>
				</label>
				<input type="date" class="form-control" name="geburtsdatum" id="vorname" 
					   title="" readonly
					   value="<?php echo $datensatz_benutzer_geburtsdatum; ?>"
				>
			</div>
			<div class="col-xs-6" style="height: 15px;">
			</div>
			<?php
				//Abfrage der Daten von benutzer_adresse zur übergebenen benutzer_id
				$abfrage_2 = "SELECT * FROM benutzer_adresse WHERE benutzer_id = '".$benutzer_id."'";
						
				$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
									
				while($datensatz_2 = $datenbank_ergebnis_2->fetch_object())
				{
					$datensatz_benutzer_id = ($datensatz_2->benutzer_id);
					$datensatz_benutzer_adresse_bezeichnung = ($datensatz_2->benutzer_adresse_bezeichnung);
					$datensatz_benutzer_adresse_straße = ($datensatz_2->benutzer_adresse_straße);
					$datensatz_benutzer_adresse_hausnummer = ($datensatz_2->benutzer_adresse_hausnummer);
					$datensatz_benutzer_adresse_postleitzahl = ($datensatz_2->benutzer_adresse_postleitzahl);
					$datensatz_benutzer_adresse_ortschaft = ($datensatz_2->benutzer_adresse_ortschaft);
					$datensatz_benutzer_adresse_land = ($datensatz_2->benutzer_adresse_land);	
			?>
			<div class="col-xs-6" style="height: 78px;">
			</div>
			<div class="col-xs-6">
				<label for="adresse">
					<h4>
						Adresse: <?php echo $datensatz_benutzer_adresse_bezeichnung; ?>
					</h4>
				</label>
				<input type="text" class="form-control" name="adresse" id="adresse" 
					   placeholder="Adresse" title="" readonly
					   value="<?php 
									echo $datensatz_benutzer_adresse_straße." ";
									echo $datensatz_benutzer_adresse_hausnummer.", "; 
									echo $datensatz_benutzer_adresse_postleitzahl." ";
									echo $datensatz_benutzer_adresse_ortschaft.", ";
									echo $datensatz_benutzer_adresse_land;  									
							 ?>"
				>
			</div>
			<?php
				}
			?>	
		</div>
	</form>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>