<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Bezahlung
	</h2>
	
	<?php
        //error_reporting(E_ALL);
        //ini_set("display_errors", "on");
        //ini_set("display_startip_errors", "on");
 
        $abfrage_1 = "SELECT * FROM (artikel 
					  INNER JOIN artikel_hat_attribut 
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id IN (";
              
        foreach($_SESSION['warenkorb'] as $id => $value) { 
            $abfrage_1 .= $id.","; 
        } 
                        
        $abfrage_1 = substr($abfrage_1, 0, -1);
        $abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
                       ORDER BY artikel.artikel_bezeichnung ASC"; 
        $datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
            
        $items = array();
        $totalprice = 0;
		$shippingset = 0.00;
		$taxset = 0.19;
		
	?>
	<div style="overflow-x:auto;">
		<table class='table table-dark'>
			<thead>
				<tr>
					<th style="width: 20%;"> Artikelnummer </th>
					<th style="width: 20%;"> Artikelbezeichnung </th>
					<th style="width: 20%;"> Stückpreis </th>
					<th style="width: 20%;"> Anzahl </th>
					<th style="width: 20%;"> Gesamtpreis </th>
				</tr>
			</thead>
			<tbody>
		<?php	    
			
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_artikel_id = ($datensatz_1->artikel_id);
				$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
				$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
								
				$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
				$totalprice += $subtotal;
					
				$price = floatval(str_replace(',', '.', $datensatz_attribut_wert));
				$count = intval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge']);
		?>
				<tr>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_id; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_bezeichnung; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format($price, 2, ',', '.')."€"; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $count; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format($price*$count, 2, ',', '.')."€"; ?>
					</td>
				</tr>
		<?php
			}
		?>
			</tbody>
		</table>
	</div>
	<br/>
	<div style="overflow-x:auto;">
		<table class="table table-dark">
			<thead>
				<tr>
					<th style="width: 20%;"> Gesamtbetrag </th>
					<th style="width: 20%;"> Mehrwertsteuer (<?php echo number_format($taxset * 100, 1, ',', '.')."%"; ?>) </th>
					<th style="width: 20%;"> Versankosten </th>
					<th style="width: 40%;"> Gesamtbetrag inkl. Mehrwertsteuer und Versankosten </th>
					<th style="width: 0%;"> </th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($totalprice, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($totalprice*(1+$taxset), 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($shippingset, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($totalprice*(1+$taxset)+$shippingset, 2, ',', '.')."€"; ?>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br/>
	<?php
		// PAYMENT-ID erzeugen:
		function generateRandomString($length = 10) {
			$chars = array_merge(range('A','Z'), range('0','9'));
			$randompayid = 'PAYID-' . substr(str_shuffle(str_repeat(implode('', $chars), $length)), 0, $length);
			return $randompayid;
		}
			
		$payid = generateRandomString(24);
	?>
	<div class="alert alert-info" role="info" align="center"> 
		Nach Abschließen des Bestellvorgangs, können Sie ihre Bestellung in unserem Shop mit der folgenden Adresse abholen.
	</div>
	<!-- Barzahlung-Formular -->
	<form action='index.php?page=user_bzhl_barz_exec&success=true&paymentId=<?php echo $payid; ?>&token=BAR-buy&PayerID=buyer' method='post' name='kontaktformular'>
		<!-- Adresse in Tabelle dargestellt-->
		<div style="overflow-x:auto;">
			<table class="table table-dark">
				<thead>
					<tr>
						<th style="width: 20%;"> Adresse </th>
						<th style="width: 20%;"> Straße </th>
						<th style="width: 20%;"> Wohnort </th>
						<th style="width: 40%;"> Land </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							HeidiSnowBoards-Shop
						</td>
						<td>
							Musterstraße 1
						</td>
						<td>
							90598 Musterstadt
						</td>
						<td>
							Deutschland
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/>
		<br/>
		<input type='checkbox' required name='AGB_Best' value='Ja'>
			&ensp; Hiermit bestätige ich, die 
			<a href='index.php?page=agb' target='_blank'>AGB</a>
			zur Kenntnis genommen zu haben und diese akzeptiere.
		<br/><br/>
		<input type='checkbox' required name='DTNS_Best' value='Ja'>
			&ensp; Ich habe die Hinweise zum 
			<a href='index.php?page=dtns' target='_blank'>Datenschutz</a>
			gelesen und bin damit einverstanden.
		<br/>
		<br/>
		<input type='submit' name='Submit' value='Kauf abschließen'
			   style='padding: 8px 16px; margin-left: 5px;'
		>      
	</form>

</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>