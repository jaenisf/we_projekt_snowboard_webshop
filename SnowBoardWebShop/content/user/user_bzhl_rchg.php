<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Bezahlung
	</h2>
	
	<?php
        //error_reporting(E_ALL);
        //ini_set("display_errors", "on");
        //ini_set("display_startip_errors", "on");
 
        $subTotal = $_POST["Betrag"];
        $taxset = $_POST["MWST"];
        $tax = $_POST["MWSTofValue"];
        $shippingset = $_POST["Versand"];
        $total = $_POST["Gesamt"];
			
        $abfrage_1 = "SELECT * FROM (artikel 
					  INNER JOIN artikel_hat_attribut 
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id IN (";
              
        foreach($_SESSION['warenkorb'] as $id => $value) { 
            $abfrage_1 .= $id.","; 
        } 
                        
        $abfrage_1 = substr($abfrage_1, 0, -1);
        $abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
                       ORDER BY artikel.artikel_bezeichnung ASC"; 
                        
        $datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
            
        $items = array();
        $totalprice = 0;
		
	?>
	<div style="overflow-x:auto;">
		<table class='table table-dark'>
			<thead>
				<tr>
					<th style="width: 20%;"> Artikelnummer </th>
					<th style="width: 20%;"> Artikelbezeichnung </th>
					<th style="width: 20%;"> Stückpreis </th>
					<th style="width: 20%;"> Anzahl </th>
					<th style="width: 20%;"> Gesamtpreis </th>
				</tr>
			</thead>
			<tbody>
		<?php	    
			
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_artikel_id = ($datensatz_1->artikel_id);
				$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
				$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
								
				$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
				$totalprice += $subtotal;
					
				$price = floatval(str_replace(',', '.', $datensatz_attribut_wert));
				$count = intval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge']);
		?>
				<tr>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_id; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_bezeichnung; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format($price, 2, ',', '.')."€"; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $count; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format($price*$count, 2, ',', '.')."€"; ?>
					</td>
				</tr>
		<?php
			}
		?>
			</tbody>
		</table>
	</div>
	<br/>
	<div style="overflow-x:auto;">
		<table class="table table-dark">
			<thead>
				<tr>
					<th style="width: 20%;"> Gesamtbetrag </th>
					<th style="width: 20%;"> Mehrwertsteuer (<?php echo number_format($taxset * 100, 1, ',', '.')."%"; ?>) </th>
					<th style="width: 20%;"> Versandkosten </th>
					<th style="width: 40%;"> Gesamtbetrag inkl. Mehrwertsteuer und Versandkosten </th>
					<th style="width: 0%;"> </th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($subTotal, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($tax, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($shippingset, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($total, 2, ',', '.')."€"; ?>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br/>
	<!-- Ermittlung der Artikelkategorieparameter -->
		<script type='text/javascript'>
			function load_adresse() {
				var x = document.getElementById("adresse_auswahl").selectedIndex;
				var benutzer_adresse_id = document.getElementById("adresse_auswahl")[x].value;
				
				$.ajax({
					type: "POST",
					url: "content/user/load_adresse.php",
					data: "benutzer_adresse_id=" + benutzer_adresse_id,
					success: function(adresse)
						{
							adresse = adresse.trim();
							
							if (adresse == "")
							{
								document.getElementById("adresse_straße").innerHTML = "";
								document.getElementById("adresse_wohnort").innerHTML = "";
								document.getElementById("adresse_land").innerHTML = "";
							}
							else
							{
								var a = adresse.split("|");
								
								document.getElementById("adresse_straße").innerHTML = a[2] + " " + a[3];
								document.getElementById("adresse_wohnort").innerHTML = a[4] + " " + a[5];
								document.getElementById("adresse_land").innerHTML = a[6];
							}
						}
				});
			};
	</script>
	<?php
		// PAYMENT-ID erzeugen:
		function generateRandomString($length = 10) {
			$chars = array_merge(range('A','Z'), range('0','9'));
			$randompayid = 'PAYID-' . substr(str_shuffle(str_repeat(implode('', $chars), $length)), 0, $length);
			return $randompayid;
		}
			
		$payid = generateRandomString(24);
	?>
	<!-- Versanddaten-Formular -->
	<form action='index.php?page=user_bzhl_rchg_exec&success=true&paymentId=<?php echo $payid; ?>&token=EC-billbuy&PayerID=buyer' method='post' name='kontaktformular'>
		<!-- Adresse in Tabelle dargestellt-->
		<div style="overflow-x:auto;">
			<table class="table table-dark">
				<thead>
					<tr>
						<th style="width: 20%;"> Adresse </th>
						<th style="width: 20%;"> Empfänger </th>
						<th style="width: 20%;"> Straße </th>
						<th style="width: 20%;"> Wohnort </th>
						<th style="width: 20%;"> Land </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<!-- Adresse-Auswahlfeld erzeugen -->
							<select name="benutzer_adresse_id" id="adresse_auswahl" onchange="load_adresse()" required>
								<option value='' selected> </option>
								<?php
									$abfrage = "SELECT * FROM benutzer_adresse WHERE benutzer_id = '".$benutzer_id."'";
											
									$datenbank_ergebnis = $verbindung->query( $abfrage );
														
									while($datensatz = $datenbank_ergebnis->fetch_object())
									{
										$datensatz_benutzer_adresse_id = ($datensatz->benutzer_adresse_id);
										$datensatz_benutzer_adresse_bezeichnung = ($datensatz->benutzer_adresse_bezeichnung);
										
										echo "<option value='".$datensatz_benutzer_adresse_id."'>".$datensatz_benutzer_adresse_bezeichnung."</option>";
									}
								?>
							</select>
						</td>
						<td>
							<?php
								$abfrage = "SELECT * FROM benutzer WHERE benutzer_id = '".$benutzer_id."';";
											
								$datenbank_ergebnis = $verbindung->query( $abfrage );
													
								while($datensatz = $datenbank_ergebnis->fetch_object())
								{
									$datensatz_benutzer_id = ($datensatz->benutzer_id);
									$datensatz_benutzer_vorname = ($datensatz->benutzer_vorname);
									$datensatz_benutzer_nachname = ($datensatz->benutzer_nachname);
									
									echo $datensatz_benutzer_vorname." ".$datensatz_benutzer_nachname;
								}
							?>
						</td>
						<td id="adresse_straße"></td>
						<td id="adresse_wohnort"></td>
						<td id="adresse_land"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/>
		<br/>
		<input type='checkbox' required name='AGB_Best' value='Ja'>
			&ensp; Hiermit bestätige ich, die 
			<a href='index.php?page=agb' target='_blank'>AGB</a>
			zur Kenntnis genommen zu haben und diese akzeptiere.
		<br/><br/>
		<input type='checkbox' required name='DTNS_Best' value='Ja'>
			&ensp; Ich habe die Hinweise zum 
			<a href='index.php?page=dtns' target='_blank'>Datenschutz</a>
			gelesen und bin damit einverstanden.
		<br/>
		<br/>
		<input type='submit' name='Submit' value='Kauf abschließen'
			   style='padding: 8px 16px; margin-left: 5px;'
		>      
	</form>

</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>