﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Bezahlung
	</h2>
	
	<?php
		//error reporting in Produktivsystem später ausschalten!
		error_reporting(E_ALL);
		ini_set("display_errors", "on");
		ini_set("display_startip_errors", "on");

		// link zur autoload.ph der PayPal-PHP-SDK
		require __DIR__  . '/PayPal-PHP-SDK/autoload.php';

		$apiContext = new \PayPal\Rest\ApiContext(
				new \PayPal\Auth\OAuthTokenCredential(
					'AcZLno0TV4fEwsBfFLCaFMA41r0O80JXyUcCrSXMDkBt0wR8fWvUbkSS6KURYv-f1HwMuIRGeYSZ7fFd', // ClientID von Paypal
					'EGCLBx0t8oYDNUX49Q4odqieAkt6I4CUY9AeCEMOFkFe80tlH54dEbVoBPtfjWRUd7KyvW16ZGxhrNAQ'  // ClientSecret von Paypal
				)
		);

		/*
			Create Payment erstellt ein Payment Objekt und gibt es an Paypal weiter.
			Diese gibt eine Paypal-URL zurück, welche der Kunde besuchen kann.
			Dort muss die Bestellung vom Kunde nur noch bestätigt werden
		*/
		
		//Create Payment using PayPal as payment method
		//API used: /v1/payments/payment
		/*use PayPal\Api\Amount;
		use PayPal\Api\Details;
		use PayPal\Api\Item;
		use PayPal\Api\ItemList;
		use PayPal\Api\Payer;
		use PayPal\Api\Payment;
		use PayPal\Api\RedirectUrls;
		use PayPal\Api\Transaction;*/
		
		//Payer-Ojekt setPaymentMethod("paypal") setzt Zahlungsmethode auf paypal.
		$payer = new PayPal\Api\Payer();
		$payer->setPaymentMethod("paypal");

		$abfrage_1 = "SELECT * FROM (artikel 
					  INNER JOIN artikel_hat_attribut 
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id IN (";
					
					
		foreach($_SESSION['warenkorb'] as $id => $value) { 
			$abfrage_1 .= $id.","; 
		} 
					
		$abfrage_1 = substr($abfrage_1, 0, -1);
		$abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
						ORDER BY artikel.artikel_bezeichnung ASC"; 
					
		$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
		
		$items = array();
		$totalprice = 0;
		
		while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
		{
			$datensatz_artikel_id = ($datensatz_1->artikel_id);
			$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
			$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
						
			$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
			$totalprice += $subtotal;
			
			$price = floatval(str_replace(',', '.', $datensatz_attribut_wert));
			
			$item = new PayPal\Api\Item();
			$item->setName($datensatz_artikel_bezeichnung)    									// Name des Produkts
				 ->setCurrency('EUR')                    										// Währung
				 ->setQuantity($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])  // Menge
				 ->setSku($datensatz_artikel_id)                      							// Produktnummer
				 ->setPrice($price);

			array_push($items, $item);
		}
		
		// Summe aller Artikel OHNE Steuern und Versandkosten
		$subTotal = floatval($totalprice);
		$subTotal = number_format($subTotal, 2, '.', '');
			
		// Steuersatz
		$taxset = 0.19;

		// Versankosten
		$shippingset = 9.99;
			
		// Erstellung einer Liste, welche die zuvor definierten items enthält
		$itemList = new PayPal\Api\ItemList();
		$itemList->setItems($items);

		// Weitere Angaben:
		$setTax = floatval($subTotal)*floatval($taxset);
		$details = new PayPal\Api\Details();
		$details->setShipping($shippingset)     //Versandkosten
			->setTax($setTax)         //Steuern
			->setSubtotal($subTotal);           //Summe der Artikel
		
		$setTotal = floatval($subTotal) + (floatval($subTotal) * floatval($taxset)) + floatval($shippingset);
		$amount = new PayPal\Api\Amount();
		$amount->setCurrency("EUR")             //Währung
			->setTotal($setTotal)   //Gesamtsumme mit Steuern und Versandkosten    
			->setDetails($details);                 

		/*
			Eine Transaktion (das Transaktions-Objekt) ist der Vertrag, wenn man eine Zahlung durchführt.
			Hier steht wofür die Zahlung ist und wer sie bezahlt.
			Das Transaktionsobjekt beinhaltet die zuvor erstellte Item-List.
		*/
		$transaction = new PayPal\Api\Transaction();
		$transaction->setAmount($amount)
			->setItemList($itemList)
			->setDescription("Payment description")
			->setInvoiceNumber(uniqid());
		
		//url
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
		{
			$url = "https";
		}
		else
		{
			$url = "http";
		};
				
		// Here append the common URL characters.
		$url .= "://";
			
		// Append the host(domain name, ip) to the URL. 
		$url .= $_SERVER['HTTP_HOST'];
						
		// Append the requested resource location to the URL
		$url .= $_SERVER['REQUEST_URI'];
					
		// Remove GET-Variable
		$url = explode("?", $url)[0];
			
		// Nach Abschluss des Kaufes wird der Kunde bedingt weitergeleitet:
		$redirectUrls = new PayPal\Api\RedirectUrls();
		$redirectUrls->setReturnUrl($url."?page=user_bzhl_pypl_exec&success=true")
			->setCancelUrl($url."?page=user_bzhl_pypl_exec&success=false");
				
		/*
			Das Payment-Objekt beinhaltet wiederum das Transaktions-Objekt.
			Das Ziel ist ein Sale --> $payment->setIntent("sale")
			Die Weiterleitungs-URLs sind auch im Objekt enthalten.
		*/

		$payment = new PayPal\Api\Payment();
		$payment->setIntent("sale")
			->setPayer($payer)
			->setRedirectUrls($redirectUrls)
			->setTransactions(array($transaction));
		
		//Nur zum Test!!
		$request = clone $payment;
		
		/*
			Mit $payment->create() wird der Api-Kontext (Zugangsdaten)
			Paypal bereitet nun den Sale vor und sendet eine Bezahl-URL in der Kauf abgeschlossen wird
		*/
		try 
		{
			$payment->create($apiContext);
		} 
		catch (Exception $ex) 
		{
			// RESULTPRINTER CLASS nicht in Produktiv-Code, nur zum Test!!
			// ResultPrinter::printError("Created Payment Using PayPal. 
			// Please visit the URL to Approve.", "Payment", null, $request, $ex);
			exit(1);
		}
		
		//redirect url wird angefordert
		//Die API Antwortet mit der URL, an die der Käufer weitergeleitet werden muss 
		// (Button einbauen?) --> $payment->getApprovalLink() 
		$approvalUrl = $payment->getApprovalLink();

		echo "<h3> <u>Bezahlungsmethoden:<u> </h3>";

		//PAYPAL-Button erzeugen
		echo("<br>
			<form action='$approvalUrl' method='post' target='_top' style='padding-left: 20px;'>
				<input type='hidden' name='cmd' value='_s-xclick'>
				<input type='hidden' name='hosted_button_id' value='D949ADTULCVEC'>
				<input type='image' src='images/PayPal_Button.png' border='0' name='submit' alt='Weiter zu PayPal' width='280' height='50'>
				<!--<img alt='' border='0' src='https://www.paypalobjects.com/de_DE/i/scr/pixel.gif' width='0' height='0'>-->
			</form>
			");

		$decoded = json_decode($payment);
		$itemsget = $decoded->transactions[0]->item_list->items;
		$zaehler = '1';
		$tax = $subTotal * $taxset;
		$tax = number_format($tax, 2, '.', '');
		$total = ($subTotal + ($subTotal * $taxset) +$shippingset);
		$total =  number_format($total, 2, '.', '');
			
		//Erzeugt Button für den Kauf per Rechnung - übergibt Items und Preise 
		echo("<br>
			<form action='index.php?page=user_bzhl_rchg' method='post' target='_top' style='padding-left: 20px;'>
				<input type='hidden' name='cmd' value='_s-xclick'>
				<input type='hidden' name='Betrag' value=$subTotal>
				<input type='hidden' name='MWSTofValue' value=$tax>
				<input type='hidden' name='MWST' value=$taxset>
				<input type='hidden' name='Versand' value=$shippingset>
				<input type='hidden' name='Gesamt' value=$total>
				<input type='hidden' name='zaehler' value=$zaehler>
				<input type='hidden' name='hosted_button_id' value='D949ADTULCVEC'>
				<input type='image' src='images/bill_button.png' border='0' name='submit' alt='Weiter zum Rechnungskauf' width='280' height='50'>
			</form>");
		  
		// Barzahlung bei Abholung
		echo("<br>
			<form action='index.php?page=user_bzhl_barz' method='post' target='_top' style='padding-left: 20px;'>
				<input type='hidden' name='cmd' value='_s-xclick'>
				<input type='hidden' name='Betrag' value=$subTotal>
				<input type='hidden' name='MWSTofValue' value=$tax>
				<input type='hidden' name='MWST' value=$taxset>
				<input type='hidden' name='Versand' value=$shippingset>
				<input type='hidden' name='Gesamt' value=$total>
				<input type='hidden' name='zaehler' value=$zaehler>
				<input type='hidden' name='hosted_button_id' value='F747LDFULCVEC'>
				<input type='image' src='images/cash_button.png' border='0' name='submit' alt='Weiter zum Barkauf' width='280' height='50'>
			</form>
			");
			
		return $payment;
	?>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>