﻿<?php
	require('../../content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<?php
	require('../../database/database_login.php');
?>

<?php
	$benutzer_adresse_id = $_POST['benutzer_adresse_id'];
	
	if (mysqli_connect_errno() == 0 && $benutzer_adresse_id != "")
	{
		//lesen aller benutzer_adressen bei denen die benutzer_adresse_id
		//der übergebenen id entsprechen
		$abfrage = "SELECT * FROM benutzer_adresse 
					WHERE benutzer_adresse_id = '".$benutzer_adresse_id."';";
		$datenbank_ergebnis = $verbindung->query($abfrage);
		
		while ($datensatz = $datenbank_ergebnis->fetch_object()){
			$datensatz_benutzer_adresse_id = ($datensatz->benutzer_adresse_id);
			$datensatz_benutzer_adresse_bezeichnung = ($datensatz->benutzer_adresse_bezeichnung);
			$datensatz_benutzer_adresse_straße = ($datensatz->benutzer_adresse_straße);
			$datensatz_benutzer_adresse_hausnummer = ($datensatz->benutzer_adresse_hausnummer);
			$datensatz_benutzer_adresse_postleitzahl = ($datensatz->benutzer_adresse_postleitzahl);
			$datensatz_benutzer_adresse_ortschaft = ($datensatz->benutzer_adresse_ortschaft);
			$datensatz_benutzer_adresse_land = ($datensatz->benutzer_adresse_land);	
		};
		
		$return = 0;
		$return .= $datensatz_benutzer_adresse_id."|";
		$return .= $datensatz_benutzer_adresse_bezeichnung."|";
		$return .= $datensatz_benutzer_adresse_straße."|";
		$return .= $datensatz_benutzer_adresse_hausnummer."|";
		$return .= $datensatz_benutzer_adresse_postleitzahl."|";
		$return .= $datensatz_benutzer_adresse_ortschaft."|";
		$return .= $datensatz_benutzer_adresse_land;
		
		echo $return;
	}
?>
					
<?php
	require('../../database/database_logout.php');
?>

<?php
	}
	
	require('../../content/anme/check_require_anme_end.php');
?>