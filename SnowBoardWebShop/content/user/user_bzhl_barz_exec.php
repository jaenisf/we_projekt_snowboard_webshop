<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Bezahlung
	</h2>
	
	<?php
        //error_reporting(E_ALL);
		//ini_set("display_errors", "on");
		//ini_set("display_startip_errors", "on");

		if (isset($_SESSION['warenkorb']) == true)
		{
			$abfrage_1 = "SELECT * FROM (artikel 
					  INNER JOIN artikel_hat_attribut 
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id IN (";
			
			foreach($_SESSION['warenkorb'] as $id => $value) { 
				$abfrage_1 .= $id.","; 
			} 
						
			$abfrage_1 = substr($abfrage_1, 0, -1);
			$abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
							ORDER BY artikel.artikel_bezeichnung ASC"; 
						
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
									
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_artikel_id = ($datensatz_1->artikel_id);
				$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
				$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
							
				$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
				$totalprice += $subtotal;
			}
		}
		else
		{
			$abfrage = "SELECT * FROM bestellung 
						WHERE paypal_payment_id = '".$_GET['paymentId']."';";
			$datenbank_ergebnis = $verbindung->query($abfrage);
			
			while ($datensatz = $datenbank_ergebnis->fetch_object()){
				$datensatz_bestellunsgwert = ($datensatz->bestellung_bestellungswert);
			};
			
			if ($datensatz_bestellunsgwert == "")
			{
				$totalprice = 0;
			}
			else
			{
				$totalprice = $datensatz_bestellunsgwert;
			}
		}
		
		$subTotal = $totalprice;
		$taxset = 0.19;
		$shippingset = 0.00;
		
		// Prüfen, ob Kunde die Zahlung genehmigt hat
		if (isset($_GET['success']) && $_GET['success'] == 'true' && $totalprice != 0) 
		{
			$abfrage = "SELECT COUNT(*) AS anzahl FROM bestellung 
						WHERE paypal_payment_id = '".$_GET['paymentId']."';";
			$datenbank_ergebnis = $verbindung->query($abfrage);

			while ($datensatz = $datenbank_ergebnis->fetch_object()){
				$datensatz_anzahl = ($datensatz->anzahl);
			};
			
			if ($datensatz_anzahl > 0)
			{
				$abfrage = "SELECT * FROM bestellung WHERE paypal_payment_id = '".$_GET['paymentId']."';";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_kunde_id = ($datensatz->kunde_id);
				};
			}
			else
			{
				$abfrage = "SELECT * FROM kunde WHERE benutzer_id = '".$_SESSION['benutzer_id']."' LIMIT 1;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_kunde_id = ($datensatz->kunde_id);
				};
			}
			
			if ($datensatz_kunde_id > 0 && $datensatz_anzahl == 0
				&& isset($_GET['paymentId']) == true && $_GET['paymentId'] != ""
				&& isset($_GET['token']) == true && $_GET['token'] != ""
				&& isset($_GET['PayerID']) == true && $_GET['PayerID'] != "")
			{		
				$datum = date("Y-m-d");
				$uhrzeit = date("H:i:sa");
				
				// Bestellung einfügen
				$speichern = "INSERT INTO bestellung
							  (kunde_id, bezahlungsart_id, bestellung_bestellungswert,
							   bestellung_datum, bestellung_uhrzeit, 
							   paypal_payment_id, paypal_token, paypal_payer_id)
							  VALUES
							  ('".$datensatz_kunde_id."', '3', '".$totalprice."', '".$datum."', '".$uhrzeit."',
							   '".$_GET['paymentId']."', '".$_GET['token']."', '".$_GET['PayerID']."');";
						
				$verbindung->query($speichern);
				
				// Bestellung die entsprechenden Artikel hinzufügen
				$abfrage = "SELECT * FROM bestellung
							WHERE bestellung.paypal_payment_id = '".$_GET['paymentId']."'
							ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()) 
				{
					$datensatz_bestellung_id = ($datensatz->bestellung_id);
				}
				
				foreach($_SESSION['warenkorb'] as $id => $value) { 
					// Artikel einer Bestellung hinzufügen
					$speichern = "INSERT INTO bestellung_hat_artikel
								  (bestellung_id, artikel_id, bestellung_hat_artikel_menge)
								  VALUES
								  ('".$datensatz_bestellung_id."', '".$id."', '".$_SESSION['warenkorb'][$id]['menge']."');";
							
					$verbindung->query($speichern);
				}
				
				$insert_into_bestellung = true;
			}
			else
			{
				$insert_into_bestellung = false;
			}
			
			if ($insert_into_bestellung == true)
			{
				echo "Die Bezahlung wird ausgeführt ...";
				unset($_SESSION['warenkorb']);
				echo '<script type="text/javascript">location.reload(true);</script>';
			}
	?>
	<h3> <u> Bestellung: </u> </h3>
	<style>
		table thead tr th {
			width: 20%;
		}
	</style>
	<div style="overflow-x:auto;">
		<table class="table table-dark">
			<thead>
				<tr>
					<th> Bestellungsnummer </th>
					<th> Bezahlungsart </th>
					<th> Bestellungsdatum </th>
					<th> Bestellungsuhrzeit </th>
					<th> Bestellwert </th>
				</tr>
			</thead>
			<tbody>
			<?php
				$abfrage = "SELECT * FROM bestellung
							INNER JOIN kunde
							ON bestellung.kunde_id = kunde.kunde_id
							INNER JOIN bezahlungsart
							ON bestellung.bezahlungsart_id = bezahlungsart.bezahlungsart_id
							WHERE kunde.benutzer_id = '".$datensatz_kunde_id."'
							AND bestellung.paypal_payment_id = '".$_GET['paymentId']."'
							ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_bestellung_id = ($datensatz->bestellung_id);
					$datensatz_bezahlungsart_bezeichnung = ($datensatz->bezahlungsart_bezeichnung);
					$datensatz_bestellung_datum = ($datensatz->bestellung_datum);
					$datensatz_bestellung_uhrzeit = ($datensatz->bestellung_uhrzeit);
					$datensatz_bestellung_bestellungswert = ($datensatz->bestellung_bestellungswert);
					$datensatz_bestellung_payment_id = ($datensatz->paypal_payment_id);
					$datensatz_bestellung_token = ($datensatz->paypal_token);
					$datensatz_bestellung_payer_id = ($datensatz->paypal_payer_id);
			?>
				<tr>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_id; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bezahlungsart_bezeichnung; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_datum; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo $datensatz_bestellung_uhrzeit; ?> 
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($datensatz_bestellung_bestellungswert, 2, ',', '.')."€"; ?> 
					</td>
				</tr>
			<?php
					
				};
			?>
			<tbody>
		</table>
	</div>
	<div style="overflow-x:auto;">			
		<table class='table table-dark'>
			<thead>
				<tr>
					<th style="width: 20%;"> Artikelnummer </th>
					<th style="width: 20%;"> Artikelbezeichnung </th>
					<th style="width: 20%;"> Stückpreis </th>
					<th style="width: 20%;"> Anzahl </th>
					<th style="width: 20%;"> Gesamtpreis </th>
				</tr>
			</thead>
			<tbody>
			<?php	    
				$abfrage = "SELECT * FROM bestellung
							INNER JOIN bestellung_hat_artikel
							ON bestellung_hat_artikel.bestellung_id = bestellung.bestellung_id
							INNER JOIN artikel
							ON bestellung_hat_artikel.artikel_id = artikel.artikel_id
							INNER JOIN artikel_hat_attribut
							ON artikel_hat_attribut.artikel_id = artikel.artikel_id
							INNER JOIN attribut
							ON artikel_hat_attribut.attribut_id = attribut.attribut_id
							WHERE bestellung.paypal_payment_id = '".$_GET['paymentId']."' 
							AND attribut.attribut_bezeichnung = 'Preis'
							ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object())
				{
					$datensatz_artikel_id = ($datensatz->artikel_id);
					$datensatz_artikel_bezeichnung = ($datensatz->artikel_bezeichnung);
					$datensatz_attribut_wert = ($datensatz->attribut_wert);
					$datensatz_bestellung_hat_artikel_menge = ($datensatz->bestellung_hat_artikel_menge);
				
					$subtotal += floatval(str_replace(",", ".", $datensatz_attribut_wert))*$datensatz_bestellung_hat_artikel_menge;
					$datensatz_attribut_wert = floatval(str_replace(",", ".", $datensatz_attribut_wert));
				?>
				<tr>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_id; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_artikel_bezeichnung; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format(floatval($datensatz_attribut_wert), 2, ',', '.')."€"; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo $datensatz_bestellung_hat_artikel_menge; ?>
					</td>
					<td style='padding-left: 25px;'>
						<?php echo number_format(floatval($datensatz_attribut_wert*$datensatz_bestellung_hat_artikel_menge), 2, ',', '.')."€"; ?>
					</td>
				</tr>
			<?php
				}
			?>
			</tbody>
		</table>
	</div>
	<br/>
	<div style="overflow-x:auto;">			
		<table class="table table-dark">
			<thead>
				<tr>
					<th style="width: 20%;"> Gesamtbetrag </th>
					<th style="width: 20%;"> Mehrwertsteuer (<?php echo number_format($taxset * 100, 1, ',', '.')."%"; ?>) </th>
					<th style="width: 20%;"> Versandkosten </th>
					<th style="width: 40%;"> Gesamtbetrag inkl. Mehrwertsteuer und Versandkosten </th>
					<th style="width: 0%;"> </th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($subtotal, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format(floatval($subtotal*$taxset), 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format($shippingset, 2, ',', '.')."€"; ?>
					</td>
					<td style="padding-left: 25px;"> 
						<?php echo number_format(floatval($subtotal*(1+$taxset)+$shippingset), 2, ',', '.')."€"; ?>
					</td>
					<td>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br/>
	<?php
		$abfrage = "SELECT * FROM bestellung
					WHERE bestellung.paypal_payment_id = '".$_GET['paymentId']."';";
					
		$datenbank_ergebnis = $verbindung->query( $abfrage );
								
		while($datensatz = $datenbank_ergebnis->fetch_object())
		{
			$datensatz_bestellung_bezahlung_erfolgreich = ($datensatz->bestellung_bezahlung_erfolgreich);
		}
		
		if ($datensatz_bestellung_bezahlung_erfolgreich == 0)
		{
	?>
	<div class="alert alert-info" role="info" align="center"> 
		Sie können ihre Bestellung in unserem Shop mit der folgenden Adresse abholen.
	</div>
	<!-- Adresse in Tabelle dargestellt-->
	<div style="overflow-x:auto;">		
		<table class="table table-dark">
			<thead>
				<tr>
					<th style="width: 20%;"> Adresse </th>
					<th style="width: 20%;"> Straße </th>
					<th style="width: 20%;"> Wohnort </th>
					<th style="width: 40%;"> Land </th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						HeidiSnowBoards-Shop
					</td>
					<td>
						Musterstraße 1
					</td>
					<td>
						90598 Musterstadt
					</td>
					<td>
						Deutschland
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
			}
			else
			{
	?>
	<div class="alert alert-success" role="success" align="center"> 
		Die Abholung und Bezahlung ist erfolgt!
	</div>
	<?php
			}
		} 
		else 
		{
			echo("Transaktion abgebrochen!");  
			//exit;
		}
	?>

</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>