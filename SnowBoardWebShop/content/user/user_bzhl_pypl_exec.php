﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Bezahlung
	</h2>
	
	<?php
		//error_reporting(E_ALL);
		//ini_set("display_errors", "on");
		//ini_set("display_startip_errors", "on");
		
		if (isset($_SESSION['warenkorb']) == true)
		{
			$abfrage_1 = "SELECT * FROM (artikel 
					  INNER JOIN artikel_hat_attribut 
					  ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
					  INNER JOIN attribut
					  ON artikel_hat_attribut.attribut_id = attribut.attribut_id
					  WHERE artikel.artikel_id IN (";
			
			foreach($_SESSION['warenkorb'] as $id => $value) { 
				$abfrage_1 .= $id.","; 
			} 
						
			$abfrage_1 = substr($abfrage_1, 0, -1);
			$abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
							ORDER BY artikel.artikel_bezeichnung ASC"; 
						
			$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
									
			while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
			{
				$datensatz_artikel_id = ($datensatz_1->artikel_id);
				$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
				$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
							
				$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
				$totalprice += $subtotal;
			}
		}
		else
		{
			$abfrage = "SELECT * FROM bestellung 
						WHERE paypal_payment_id = '".$_GET['paymentId']."';";
			$datenbank_ergebnis = $verbindung->query($abfrage);
			
			while ($datensatz = $datenbank_ergebnis->fetch_object()){
				$datensatz_bestellunsgwert = ($datensatz->bestellung_bestellungswert);
			};
			
			if ($datensatz_bestellunsgwert == "")
			{
				$totalprice = 0;
			}
			else
			{
				$totalprice = $datensatz_bestellunsgwert;
			}
		}
		
		$subTotal = $totalprice;
		$taxset = 0.19;
		$shippingset = 9.99;

		require __DIR__  . '/PayPal-PHP-SDK/autoload.php';

		$apiContext = new \PayPal\Rest\ApiContext(
				new \PayPal\Auth\OAuthTokenCredential(
					'AcZLno0TV4fEwsBfFLCaFMA41r0O80JXyUcCrSXMDkBt0wR8fWvUbkSS6KURYv-f1HwMuIRGeYSZ7fFd',     // ClientID
					'EGCLBx0t8oYDNUX49Q4odqieAkt6I4CUY9AeCEMOFkFe80tlH54dEbVoBPtfjWRUd7KyvW16ZGxhrNAQ'      // ClientSecret
				)
		);

		/*use PayPal\Api\Amount;
		use PayPal\Api\Details;
		use PayPal\Api\Payment;
		use PayPal\Api\PaymentExecution;
		use PayPal\Api\Transaction;*/
		
		// Prüfen, ob Kunde die Zahlung genehmigt hat
		if (isset($_GET['success']) && $_GET['success'] == 'true' && $totalprice != 0) 
		{
			$abfrage = "SELECT COUNT(*) AS anzahl FROM bestellung 
						WHERE paypal_payment_id = '".$_GET['paymentId']."';";
			$datenbank_ergebnis = $verbindung->query($abfrage);

			while ($datensatz = $datenbank_ergebnis->fetch_object()){
				$datensatz_anzahl = ($datensatz->anzahl);
			};
			
			if ($datensatz_anzahl > 0)
			{
				$abfrage = "SELECT * FROM bestellung WHERE paypal_payment_id = '".$_GET['paymentId']."';";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_kunde_id = ($datensatz->kunde_id);
				};
			}
			else
			{
				$abfrage = "SELECT * FROM kunde WHERE benutzer_id = '".$_SESSION['benutzer_id']."' LIMIT 1;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_kunde_id = ($datensatz->kunde_id);
				};
			}
			
			if ($datensatz_kunde_id > 0 && $datensatz_anzahl == 0
				&& isset($_GET['paymentId']) == true && $_GET['paymentId'] != ""
				&& isset($_GET['token']) == true && $_GET['token'] != ""
				&& isset($_GET['PayerID']) == true && $_GET['PayerID'] != "")
			{		
				$datum = date("Y-m-d");
				$uhrzeit = date("H:i:sa");
				
				$speichern = "INSERT INTO bestellung
							  (kunde_id, bezahlungsart_id, bestellung_bestellungswert,
							   bestellung_datum, bestellung_uhrzeit, 
							   paypal_payment_id, paypal_token, paypal_payer_id)
							  VALUES
							  ('".$datensatz_kunde_id."', '1', '".$totalprice."', '".$datum."', '".$uhrzeit."',
							   '".$_GET['paymentId']."', '".$_GET['token']."', '".$_GET['PayerID']."');";
						
				$verbindung->query($speichern);
				
				$insert_into_bestellung = true;
			}
			else
			{
				$insert_into_bestellung = false;
			}
			
			/*
			Übergeben der PaymentID an payment::get (in der GET-Variablen an diese Seite).
			Erneute Angabe des Payment Objektes
			 */
			$paymentId = $_GET['paymentId'];
			$payment = PayPal\Api\Payment::get($paymentId, $apiContext);
			
		    // Erstellung des Execution-Objekts (für die durchführung der Zahlung benötig)
			$execution = new PayPal\Api\PaymentExecution();
			$execution->setPayerId($_GET['PayerID']);
			
			/*
				Amount ist Teil des Transaktionsobjekts, das hier neu erstellt wird.
				Nachdem das Transaktionsobjekt aufgebaut wurde, wird es ebenfalls dem 
				Execution-Objekt hinzugefügt.
			*/
			$transaction = new PayPal\Api\Transaction();
			$amount = new PayPal\Api\Amount();
			$details = new PayPal\Api\Details();

			$details->setShipping($shippingset)
					->setTax($subTotal*$taxset)
					->setSubtotal($subTotal);

			$amount->setCurrency('EUR');
			$amount->setTotal($subTotal + ($subTotal * $taxset) + $shippingset);
			$amount->setDetails($details);
			$transaction->setAmount($amount);
			$execution->addTransaction($transaction);
			
			try 
			{
				// Finalisierung der Zahlung und veranschaulichung:
				$result = $payment->execute($execution, $apiContext);   //Zahlung ausführen
			
				if ($insert_into_bestellung == true)
				{
					echo "Die Bezahlung wird ausgeführt ...";
					
					$speichern = "UPDATE bestellung SET bestellung_bezahlung_erfolgreich = 1
								  WHERE paypal_payment_id = '".$_GET['paymentId']."';";
						
					$verbindung->query($speichern);
					
					unset($_SESSION['warenkorb']);
					echo '<script type="text/javascript">location.reload(true);</script>';
				}
	?>
		<h3> <u> Bestellung: </u> </h3>
		<style>
			table thead tr th {
				width: 20%;
			}
		</style>
		<div style="overflow-x:auto;">
			<table class="table table-dark">
				<thead>
					<tr>
						<th> Bestellungsnummer </th>
						<th> Bezahlungsart </th>
						<th> Bestellungsdatum </th>
						<th> Bestellungsuhrzeit </th>
						<th> Bestellwert </th>
					</tr>
				</thead>
				<tbody>
			<?php
				$abfrage = "SELECT * FROM bestellung
							INNER JOIN kunde
							ON bestellung.kunde_id = kunde.kunde_id
							INNER JOIN bezahlungsart
							ON bestellung.bezahlungsart_id = bezahlungsart.bezahlungsart_id
							WHERE kunde.benutzer_id = '".$datensatz_kunde_id."'
							AND bestellung.paypal_payment_id = '".$_GET['paymentId']."'
							ORDER BY bestellung.bestellung_datum, bestellung.bestellung_uhrzeit ASC;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_bestellung_id = ($datensatz->bestellung_id);
					$datensatz_bezahlungsart_bezeichnung = ($datensatz->bezahlungsart_bezeichnung);
					$datensatz_bestellung_datum = ($datensatz->bestellung_datum);
					$datensatz_bestellung_uhrzeit = ($datensatz->bestellung_uhrzeit);
					$datensatz_bestellung_bestellungswert = ($datensatz->bestellung_bestellungswert);
					$datensatz_bestellung_payment_id = ($datensatz->paypal_payment_id);
					$datensatz_bestellung_token = ($datensatz->paypal_token);
					$datensatz_bestellung_payer_id = ($datensatz->paypal_payer_id);
			?>
					<tr>
						<td style="padding-left: 25px;"> 
							<?php echo $datensatz_bestellung_id; ?> 
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $datensatz_bezahlungsart_bezeichnung; ?> 
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $datensatz_bestellung_datum; ?> 
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $datensatz_bestellung_uhrzeit; ?> 
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format($datensatz_bestellung_bestellungswert, 2, ',', '.')."€"; ?> 
						</td>
					</tr>
			<?php
					
				};
			?>
				<tbody>
			</table>
		</div>
		<div style="overflow-x:auto;">
			<table class="table table-dark">
				<thead>
					<tr>
						<th> Artikelnummer </th>
						<th> Artikelbezeichnung </th>
						<th> Stückpreis </th>
						<th> Anzahl </th>
						<th> Gesamtpreis </th>
					</tr>
				</thead>
				<tbody>
		<?php
					// Dekodieren des JSON-Strings $payment
					$decoded = json_decode($payment);

					//Auslesen des Item-Array und speichern in Variable
					$items = $decoded->transactions[0]->item_list->items;

					foreach($items as $item)
					{
		?>
					<tr>
						<td style="padding-left: 25px;"> 
							<?php echo $item->sku; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $item->name; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format(($item->price), 2, ',', '.')."€"; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $item->quantity; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format(($item->price)*($item->quantity), 2, ',', '.')."€"; ?>
						</td>
					</tr>
		<?php
					}
		?>
				</tbody>
			</table>
		</div>
	<?php
				//erneutes Abfragen der von Paypal definierten Werte
				$ppsubtotal = $decoded->transactions[0]->related_resources[0]->sale->amount->details->subtotal;
				$tax = $decoded->transactions[0]->related_resources[0]->sale->amount->details->tax;
				$shipping = $decoded->transactions[0]->related_resources[0]->sale->amount->details->shipping;
				$total = $decoded->transactions[0]->related_resources[0]->sale->amount->total;
				$taxset = ((($total - $shipping) / $ppsubtotal) - 1);
	?>
		<div style="overflow-x:auto;">
			<table class="table table-dark">
				<thead>
					<tr>
						<th> Gesamtbetrag </th>
						<th> Mehrwertsteuer (<?php echo number_format($taxset * 100, 1, ',', '.')."%"; ?>) </th>
						<th> Versandkosten </th>
						<th style="width: 40%;"> Gesamtbetrag inkl. Mehrwertsteuer und Versandkosten </th>
						<th style="width: 0%;"> </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="padding-left: 25px;"> 
							<?php echo number_format($ppsubtotal, 2, ',', '.')."€"; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format($tax, 2, ',', '.')."€"; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format($shipping, 2, ',', '.')."€"; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo number_format($total, 2, ',', '.')."€"; ?>
						</td>
						<td>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php  
				$recipient_name = $decoded->payer->payer_info->shipping_address->recipient_name;
				$address_street = $decoded->payer->payer_info->shipping_address->line1;
				$address_city = $decoded->payer->payer_info->shipping_address->city;
				$address_postal = $decoded->payer->payer_info->shipping_address->postal_code;
				$address_county = $decoded->payer->payer_info->shipping_address->country_code;
				$address_state = $decoded->payer->payer_info->shipping_address->state;
				$payment_method = $decoded->payer->payment_method;
				$payer_email = $decoded->payer->payer_info->email;
				$pay_id = $decoded->id;
	?>
		<div style="overflow-x:auto;">	
			<table class="table table-dark">
				<thead>
					<tr>
						<th> Empfänger </th>
						<th> Straße </th>
						<th> Wohnort </th>
						<th style="width: 40%;"> Land </th>
						<th style="width: 0%;"> </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="padding-left: 25px;"> 
							<?php echo $recipient_name." <br>(".$payer_email.")"; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $address_street; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $address_postal." ".$address_city; ?>
						</td>
						<td style="padding-left: 25px;"> 
							<?php echo $address_county." - ".$address_state; ?>
						</td>
						<td>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	<?php
				try 
				{
					$payment = PayPal\Api\Payment::get($paymentId, $apiContext);
				} 
				catch (Exception $ex) 
				{
					exit(1);
				}
			} 
			catch (Exception $ex) 
			{
				exit(1);
			}
		
			return $payment;
		} 
		else 
		{
			echo("Transaktion abgebrochen!");  
			//exit;
		}
	?>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>