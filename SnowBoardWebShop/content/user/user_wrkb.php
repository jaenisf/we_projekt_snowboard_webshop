﻿<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/user/user_wrkb.css"/>

<!-- Warenkorb -->
<article id="" class="">
	<h2>
		Warenkorb
	</h2>
	<?php
		if(isset($_POST['submit_aktualisieren'])){ 
			foreach($_POST['menge'] as $key => $value) { 
				if($value == 0)
				{ 
					unset($_SESSION['warenkorb'][$key]); 
				}
				else
				{
					$_SESSION['warenkorb'][$key]['menge'] = $value; 
				}
			}
			
			header("Location: index.php?page=user_wrkb");
		}
		
		if(isset($_POST['submit_bezahlen'])){ 
			header("Location: index.php?page=user_bzhl");
		}
	?>
	<form id="warenkorb" method="post" action="index.php?page=user_wrkb" style="text-align: center;"> 
		<style>
			th, td {
				width: 200px;
				text-align: center;
			}
		</style>
		<div style="overflow-x:auto;">
			<table border="1" style="margin: 0 auto;"> 
				<tr style="background-color: lightgray; height: 35px;"> 
					<th>
						Artikelbezeichnung
					</th> 
					<th>
						Anzahl
					</th> 
					<th>
						Stückpreis
					</th> 
					<th>
						Gesamtpreis
					</th>
				</tr>
				<?php
					$totalprice = 0;
					
					if(isset($_SESSION['warenkorb']) && count($_SESSION['warenkorb']) > 0)
					{
						//Lesen der Daten für die Daten aus dem Warenkorb	
						$abfrage_1 = "SELECT * FROM (artikel 
									INNER JOIN artikel_hat_attribut 
									ON artikel.artikel_id = artikel_hat_attribut.artikel_id)
									INNER JOIN attribut
									ON artikel_hat_attribut.attribut_id = attribut.attribut_id
									WHERE artikel.artikel_id IN (";
						
						
						foreach($_SESSION['warenkorb'] as $id => $value) { 
							$abfrage_1 .= $id.","; 
						} 
						
						$abfrage_1 = substr($abfrage_1, 0, -1);
						$abfrage_1 .= ") AND attribut.attribut_bezeichnung = 'Preis'
										ORDER BY artikel.artikel_bezeichnung ASC"; 
						
						$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
									
						while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
						{
							$datensatz_artikel_id = ($datensatz_1->artikel_id);
							$datensatz_artikel_bezeichnung = ($datensatz_1->artikel_bezeichnung);
							$datensatz_attribut_wert = ($datensatz_1->attribut_wert);
							
							$subtotal = floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert));
							$totalprice += $subtotal;
				?> 
				<tr> 
					<td>
						<?php echo $datensatz_artikel_bezeichnung; ?>
					</td>
					<td>
						<!--onchange="document.getElementById('warenkorb').submit();"-->
						<input id="" type="number" name="menge[<?php echo $datensatz_artikel_id; ?>]" size="5" 
							min="0" value="<?php echo $_SESSION['warenkorb'][$datensatz_artikel_id]['menge']; ?>"
							style="margin-left: 25%; margin-right: 25%; margin-top: 5%; margin-bottom: 5%;
									width: 50%; text-align: center; border-radius: 5px; border: 1px solid gray;"
						/>
					</td> 
					<td>
						<?php echo number_format(str_replace(",", ".", $datensatz_attribut_wert), 2, ',', '.')."€"; ?> 
					</td> 
					<td>
						<?php echo number_format((floatval($_SESSION['warenkorb'][$datensatz_artikel_id]['menge'])*floatval(str_replace(",", ".", $datensatz_attribut_wert))), 2, ',', '.')."€"; ?> 
					</td>
				</tr>
				<?php
						}
				?>
				<tr style="background-color: lightgray; height: 35px;"> 
					<th colspan="4">
						Gesamtpreis aller Produkte
					</th> 
				</tr>
				<tr style="height: 35px;"> 
					<td colspan="4">
						<b style="font-size: 16pt;">
							<?php echo number_format($totalprice, 2, ',', '.')."€"; ?>
						</b>
					</td> 
				</tr>
				<tr style="background-color: lightgray; height: 35px;"> 
					<th colspan="4">
						inkl. 19% Mehrwertsteuer
					</th> 
				</tr>
				<tr style="height: 35px;"> 
					<td colspan="4">
						<b style="font-size: 16pt;">
							<?php echo number_format(0.19*$totalprice, 2, ',', '.')."€"; ?>
						</b>
					</td> 
				</tr>
				<tr style="background-color: lightgray; height: 35px;"> 
					<th colspan="4">
						zzgl. Versandkosten
					</th> 
				</tr>
				<tr style="height: 35px;"> 
					<td colspan="4">
						<b style="font-size: 16pt;">
							9.99€
						</b>
					</td> 
				</tr>
				<?php 
					$totalprice *= 1.19; 
					$totalprice += 9.99; 
				?> 
				<tr style="background-color: lightgray; height: 35px;"> 
					<th colspan="4">
						Endpreis inkl. 19% Mehrwertsteuer und Versandkosten
					</th> 
				</tr>
				<tr style="height: 35px;"> 
					<td colspan="4">
						<b style="font-size: 16pt;">
							<?php echo number_format($totalprice, 2, ',', '.')."€"; ?>
						</b>
					</td> 
				</tr>
				<?php
					}
					else
					{
				?>
				<tr> 
					<th colspan="4" style="background-color: lightorange; height: 35px;">
						Es befinden sich keine Artikel im Warenkorb!
					</th> 
				</tr>
				<?php
					}
				?>
			</table>
		</div>
		<?php
			if(isset($_SESSION['warenkorb']) && count($_SESSION['warenkorb']) > 0)
			{
		?>
		<br/>
		Falls Sie ein Produkt aus dem Warenkorb entfernen wollen, setzen Sie die Anzahl des Produktes auf 0.
		<br/>
		<br/>

		<button id="wrkb "type="submit" name="submit_aktualisieren" 
				style="border: 2px solid black; border-radius: 5px; padding: 5px 10px; width: 200px;">
			Warenkorb aktualisieren
		</button>
		
		<button id="wrkb" type="submit" name="submit_bezahlen" 
				style="border: 2px solid black; border-radius: 5px; padding: 5px 10px; width: 200px;">
			Bezahlen
		</button>
		<!--<br/>
		<hr style="border: 1px solid black; margin-left: 20%; margin-right: 20%;">-->
		<!-- 
			Email ID: sb-bexq11663833@personal.example.com 
			System Generated Password: Ei/5[kP>
		-->
		<!--<div id="paypal-button-container"></div>
		<script src="https://www.paypal.com/sdk/js?client-id=AcZLno0TV4fEwsBfFLCaFMA41r0O80JXyUcCrSXMDkBt0wR8fWvUbkSS6KURYv-f1HwMuIRGeYSZ7fFd&currency=EUR" data-sdk-integration-source="button-factory"></script>
		<script>
		  paypal.Buttons({
			  style: {
				  shape: 'pill',
				  color: 'silver',
				  layout: 'vertical',
				  label: 'pay',
				  
			  },
			  createOrder: function(data, actions) {
				  return actions.order.create({
					  purchase_units: [{
						  amount: {
							  value: <?php echo $totalprice; ?>
						  },
						  description: 'Bestellung - HeidiSnowBoards'
					  }]
				  });
			  },
			  onApprove: function(data, actions) {
				  return actions.order.capture().then(function(details) {
					  alert('Transaction completed by ' + details.payer.name.given_name + '!');
				  });
			  }
		  }).render('#paypal-button-container');
		</script>-->
		<?php
			}
		?>
	</form>
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>