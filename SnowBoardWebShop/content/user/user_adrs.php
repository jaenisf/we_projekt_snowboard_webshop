<?php
	require('content/anme/check_require_anme_beginn.php');
	
	if ($access == true)
	{
?>

<!-- CSS --->
<link rel="stylesheet" type="text/css" href="css/shop/shop_vwtg.css"/>

<!-- Content-Bereich -->
<article id="" class="">
	<h2>
		Adressen bearbeiten
	</h2>
	<p>
	</p>
	
	<!-- Formular-Bereich -->
	<div id="verwaltungsraster">
	
		<!-- Ausgabe bei erfolgreicher Speicherung -->
		<div id="success" style="display: none;">
			<fieldset id='verwaltungselement'>
				<div class='alert alert-success' role='alert' id='' style="margin-bottom: 0px; text-align: center;"> Erfolgreich gespeichert! </div>		
			</fieldset>
		</div>
		
		<!-- Ausgabe bei falscher Einagbe -->
		<div id="information" style="display: none;">
			<fieldset id='verwaltungselement'>
				<div class='alert alert-danger' role='alert' id='verwaltungselementbezeichnung' style="margin-bottom: 0px;">
				</div>		
			</fieldset>
		</div>
		
		<!-- Neue Adresse hinzufügen -->
		<form method='post' action="index.php?page=user_adrs" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_1'])) 
				{
					$adresse_bezeichnung = $_POST['adresse_bezeichnung'];
					$adresse_straße = $_POST['adresse_straße'];
					$adresse_hausnummer = $_POST['adresse_hausnummer'];
					$adresse_postleitzahl = $_POST['adresse_postleitzahl'];
					$adresse_ortschaft = $_POST['adresse_ortschaft'];
					$adresse_land = $_POST['adresse_land'];
					
					$speichern = "INSERT INTO benutzer_adresse
								  (benutzer_id, benutzer_adresse_bezeichnung,
								   benutzer_adresse_straße, benutzer_adresse_hausnummer,
								   benutzer_adresse_postleitzahl, benutzer_adresse_ortschaft,
								   benutzer_adresse_land)
								  VALUES
								  ('".$benutzer_id."', '".$adresse_bezeichnung."',
								   '".$adresse_straße."', '".$adresse_hausnummer."',
								   '".$adresse_postleitzahl."', '".$adresse_ortschaft."',
								   '".$adresse_land."');";
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- HTML-Formular - Neue Adresse hinzufügen -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Neue Adresse hinzufügen
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Adressbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="adresse_bezeichnung" value="" minlength="3" maxlength="22" style='width: 170px;' required>
							</td>
							<td>
								Straße:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="adresse_straße" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Hausnummer:
							</td>
							<td> 
								<input type="text" id="text" name="adresse_hausnummer" value="" minlength="0" maxlength="22" style='width: 100px;' required>
							</td>
							<td>
								Postleitzahl:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="adresse_postleitzahl" value="" minlength="5" maxlength="5" style='width: 100px;' required>
							</td>
							<td>
								Wohnort:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="adresse_ortschaft" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Land:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="text" name="adresse_land" value="" minlength="0" maxlength="22" style='width: 170px;' required>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Hinzufügen" id="speichern" name="speichern_1">	
			</fieldset>
		</form>
	
		<!-- Adresse ändern -->
		<form method='post' action="index.php?page=user_adrs" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php
				
				if (isset($_POST['speichern_2'])) 
				{
					$adresse = $_POST['adresse'];
					$adresse_bezeichnung = $_POST['adresse_bezeichnung'];
					$adresse_straße = $_POST['adresse_straße'];
					$adresse_hausnummer = $_POST['adresse_hausnummer'];
					$adresse_postleitzahl = $_POST['adresse_postleitzahl'];
					$adresse_ortschaft = $_POST['adresse_ortschaft'];
					$adresse_land = $_POST['adresse_land'];
					
					$speichern = "UPDATE benutzer_adresse
								  SET benutzer_adresse_bezeichnung = '".$adresse_bezeichnung."',
									  benutzer_adresse_straße = '".$adresse_straße."',
									  benutzer_adresse_hausnummer = '".$adresse_hausnummer."',
									  benutzer_adresse_postleitzahl = '".$adresse_postleitzahl."',
									  benutzer_adresse_ortschaft = '".$adresse_ortschaft."',
									  benutzer_adresse_land = '".$adresse_land."'
								  WHERE benutzer_adresse_id = '".$adresse."';";
					
					$verbindung->query($speichern);
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>	
			<?php			
				};	
			?>
			
			<!-- Erstellung einer Adressenliste -->	
			<?php
				$adresse_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM benutzer_adresse 
							WHERE benutzer_id = '".$benutzer_id."'
							ORDER BY benutzer_adresse_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_benutzer_adresse_id = ($datensatz->benutzer_adresse_id);
					$datensatz_benutzer_adresse_bezeichnung = ($datensatz->benutzer_adresse_bezeichnung);
									
					$adresse_liste .= "<option value='".$datensatz_benutzer_adresse_id."'>".$datensatz_benutzer_adresse_bezeichnung."</option>";
				};
			?>
			
			<!-- Ermittlung der in Frage kommenden Attribute -->
			<script type='text/javascript'>
				function load_adresse_ändern() {
					var x = document.getElementById("adresse_adresse_ändern").selectedIndex;
					var adresse = document.getElementById("adresse_adresse_ändern")[x].value;
					
					$.ajax({
						type: "POST",
						url: "content/user/load_adresse.php",
						data: "benutzer_adresse_id=" + adresse,
						success: function(adresse)
							{
								adresse = adresse.trim();
								
								if (adresse == "")
								{
									document.getElementById("adresse_bezeichnung_adresse_ändern").value = "";
									document.getElementById("adresse_straße_adresse_ändern").value = "";
									document.getElementById("adresse_hausnummer_adresse_ändern").value = "";
									document.getElementById("adresse_postleitzahl_adresse_ändern").value = "";
									document.getElementById("adresse_ortschaft_adresse_ändern").value = "";
									document.getElementById("adresse_land_adresse_ändern").value = "";
								}
								else
								{
									var a = adresse.split("|");
									
									document.getElementById("adresse_bezeichnung_adresse_ändern").value = a[1];
									document.getElementById("adresse_straße_adresse_ändern").value = a[2];
									document.getElementById("adresse_hausnummer_adresse_ändern").value = a[3];
									document.getElementById("adresse_postleitzahl_adresse_ändern").value = a[4];
									document.getElementById("adresse_ortschaft_adresse_ändern").value = a[5];
									document.getElementById("adresse_land_adresse_ändern").value = a[6];
								}
							}
					});
				};
			</script>
			
			<!-- HTML-Formular - Adresse ändern -->
			<fieldset id='verwaltungselement'>
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Adresse ändern
				</div>
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Adresse:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='adresse' id='adresse_adresse_ändern' size='' onchange="load_adresse_ändern()" required>
									<?php echo $adresse_liste; ?>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								Adressbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="adresse_bezeichnung_adresse_ändern" name="adresse_bezeichnung" value="" minlength="3" maxlength="22" style='width: 170px;' required>
							</td>
							<td>
								Straße:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="adresse_straße_adresse_ändern" name="adresse_straße" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Hausnummer:
							</td>
							<td> 
								<input type="text" id="adresse_hausnummer_adresse_ändern" name="adresse_hausnummer" value="" minlength="0" maxlength="22" style='width: 100px;' required>
							</td>
							<td>
								Postleitzahl:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="adresse_postleitzahl_adresse_ändern" name="adresse_postleitzahl" value="" minlength="5" maxlength="5" style='width: 100px;' required>
							</td>
							<td>
								Wohnort:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="adresse_ortschaft_adresse_ändern" name="adresse_ortschaft" value="" minlength="0" maxlength="22" required>
							</td>
							<td>
								Land:
							</td>
							<td style='padding-right: 25px;'> 
								<input type="text" id="adresse_land_adresse_ändern" name="adresse_land" value="" minlength="0" maxlength="22" style='width: 170px;' required>
							</td>
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Ändern" id="speichern" name="speichern_2">
			</fieldset>
		</form>
	
		<!-- Adresse entfernen -->
		<form method='post' action="index.php?page=user_adrs" >
			
			<!-- Speichern der Formulardaten, nach dem Abschicken -->
			<?php	
				if (isset($_POST['speichern_3']))
				{	
					$adresse = $_POST['adresse_bezeichnung'];
				
					$speichern = "DELETE FROM benutzer_adresse WHERE benutzer_adresse_id = '".$adresse."';";
					$verbindung->query($speichern);
					
					header("Location: index.php?page=user_adrs");
			?>
				<script>
					document.getElementById('success').style.display='';
				</script>
			<?php
				};	
			?>
			
			<!-- Erstellung einer Adressenliste -->	
			<?php
				$adresse_liste = "<option value='' selected> </option>";
				
				$abfrage = "SELECT * FROM benutzer_adresse 
							WHERE benutzer_id = '".$benutzer_id."'
							ORDER BY benutzer_adresse_bezeichnung;";
				$datenbank_ergebnis = $verbindung->query($abfrage);

				while ($datensatz = $datenbank_ergebnis->fetch_object()){
					$datensatz_benutzer_adresse_id = ($datensatz->benutzer_adresse_id);
					$datensatz_benutzer_adresse_bezeichnung = ($datensatz->benutzer_adresse_bezeichnung);
									
					$adresse_liste .= "<option value='".$datensatz_benutzer_adresse_id."'>".$datensatz_benutzer_adresse_bezeichnung."</option>";
				};
			?>
			
			<!-- HTML-Formular - Adresse entfernen -->
			<fieldset id='verwaltungselement'>
			
				<div class='alert alert-warning' role='alert' id='verwaltungselementbezeichnung'>
					Adresse entfernen
				</div>	
				<table id='verwaltungselementinhalt' style="margin-left: auto; margin-right: auto; width: auto;">
					<tbody>
						<tr>
							<td>
								Adressbezeichnung:
							</td>
							<td style='padding-right: 25px;'> 
								<select name='adresse_bezeichnung' id='adresse_bezeichnung_entfernen' size='' required>
									<?php echo $adresse_liste; ?>
								</select>
							</td>						
						</tr>
					</tbody>
				</table>
				<hr>
				<input type="submit" value="Entfernen" id="speichern" name="speichern_3">			
			</fieldset>
		</form>
	</div>
	
</article>

<?php
	}
	
	require('content/anme/check_require_anme_end.php');
?>