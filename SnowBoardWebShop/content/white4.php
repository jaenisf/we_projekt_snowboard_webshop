﻿<?php
	require('content/anme/check_require_anme_beginn.php');
?>

<!-- Nicht die notwendigen Berechtigungen -->
<div class="alert alert-danger" role="alert" align="center">
	Sie besitzen nicht die notwendigen Berechtigungen, um diesen Inhalt sehen zu dürfen!
</div>