<?php
	date_default_timezone_set("Europe/Berlin");
	$timestamp = time();
	$datum = date("d.m.Y",$timestamp);
	$uhrzeit = date("H:i:s",$timestamp);

	$benutzer_id = $_SESSION['benutzer_id'];

	//  Abfrage, ob Route mit Rolle für Benutzer erlaubt
	$abfrage_1 = "SELECT COUNT(*) AS anzahl FROM route 
				  INNER JOIN route_für_rolle 
				  ON route.route_id = route_für_rolle.route_id 
				  WHERE route.route_zielpunkt = '".$_GET['page']."';";
	$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
				
	while($datensatz_1 = $datenbank_ergebnis_1->fetch_object())
	{
		$datensatz_anzahl = ($datensatz_1->anzahl);
	}
	
	if ($datensatz_anzahl > 0)
	{
		//Abfrage der route_für_rolle für die übergebene Seite
		$abfrage_2 = "SELECT * FROM route_für_rolle 
						  INNER JOIN route
						  ON route.route_id = route_für_rolle.route_id 
						  INNER JOIN rolle 
						  ON route_für_rolle.rolle_id = rolle.rolle_id 
						  WHERE route.route_zielpunkt = '".$_GET['page']."';";
		$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
						
		while($datensatz_2 = $datenbank_ergebnis_2->fetch_object()) {
			$datensatz_route_id = ($datensatz_2->route_id);
			$datensatz_rolle_id = ($datensatz_2->rolle_id);
			$datensatz_rolle_bezeichnung = ($datensatz_2->rolle_bezeichnung);
			
			$abfrage_3 = "SELECT COUNT(*) AS anzahl FROM INFORMATION_SCHEMA.TABLES 
						  WHERE TABLE_SCHEMA = (SELECT database()) 
						  AND TABLE_NAME = '".$datensatz_rolle_bezeichnung."';";
			$datenbank_ergebnis_3 = $verbindung->query( $abfrage_3 );
							
			while($datensatz_3 = $datenbank_ergebnis_3->fetch_object()) {
				$datensatz_anzahl = ($datensatz_3->anzahl);
			}
			
			$boolean = false;
			
			if ($datensatz_anzahl > 0)
			{
				$abfrage_4 = "SELECT COUNT(*) AS anzahl FROM ".$datensatz_rolle_bezeichnung." 
							  WHERE benutzer_id = '".$benutzer_id."';";
				$datenbank_ergebnis_4 = $verbindung->query( $abfrage_4 );
								
				while($datensatz_4 = $datenbank_ergebnis_4->fetch_object()) {
					$datensatz_anzahl = ($datensatz_4->anzahl);
				}
				
				if ($datensatz_anzahl > 0)
				{
					$boolean = true;
				}
				else
				{
					$boolean = false;
				}
			}
			else
			{
				$boolean = true;
			}
		}
	}
	else
	{
		$boolean = true;
	}
	
	if ($boolean == true)
	{
		// Weiterleitung zur Route
		
		// Abfrage, ob eingebene Route vorhanden ist
		// wenn ja --> Route wird abgerufen
		// wenn nein --> Default-Route wird abgerufen
		
		$abfrage_1 = "SELECT COUNT(*) AS anzahl FROM route WHERE route_zielpunkt = '".$_GET['page']."';";
		$datenbank_ergebnis_1 = $verbindung->query( $abfrage_1 );
				
		while($datensatz_1 = $datenbank_ergebnis_1->fetch_object()) {
			$datensatz_anzahl = ($datensatz_1->anzahl);
				
			if ($datensatz_anzahl > 0) {
				$route_zielpunkt = $_GET['page'];
			} else {
				$route_zielpunkt = "white3";
			};
		};
	}
	else
	{
		// entsprechende Error-Route
		$route_zielpunkt = "white4";
	}
	
	// ermitteln der Routenparameter
	// Titelergänzung und Seiteninhalt werden gesetzt
	
	$abfrage_2 = "SELECT * FROM route WHERE route_zielpunkt = '".$route_zielpunkt."';";
	$datenbank_ergebnis_2 = $verbindung->query( $abfrage_2 );
			
	while($datensatz_2 = $datenbank_ergebnis_2->fetch_object()) {
		$datensatz_route_id = ($datensatz_2->route_id);
		$datensatz_route_beschreibung = ($datensatz_2->route_beschreibung);
		$datensatz_route_zielpunkt = ($datensatz_2->route_zielpunkt);
		$datensatz_route_zielpunkt_bezugsquelle = ($datensatz_2->route_zielpunkt_bezugsquelle);
		$datensatz_route_titelergänzung = ($datensatz_2->route_zielpunkt_titelergänzung);
		
		$titelergänzung = $datensatz_route_titelergänzung;
		$seiteninhalt = $datensatz_route_zielpunkt_bezugsquelle;
	};
?>