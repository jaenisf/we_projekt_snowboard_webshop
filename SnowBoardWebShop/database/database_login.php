<?php
	// *********** Zugangsdaten ********** //
		$host = "localhost";   				// *** Servername *** //
		$db_name = "snowboard-webshop";	// *** Datenbankname *** //
		$db_user = "root";   				// *** Benuter mit Zugangsrechten *** //
		$db_passwort = "";   				// *** Datenbankpasswort *** //
	
	// *********** Verbindungsaufbau mit der Datenbank ********** //
		$verbindung = new mysqli($host, $db_user, $db_passwort, $db_name);		// *** Variable zur Anmeldung/Verbindung mit der Datenbank definieren (beinhaltet Zugangsdaten) *** //		
			
	// *********** Prüfung, ob der Verbindungsaufbau mit der Datenbank erfolgreich war ********** //
		$weiter = true;
		$verbindung_fehlgeschlagen = "Die Verbindung zur Datenbank konnte nicht aufgebaut werden. <br>";   		// *** Meldung, wenn Verbindungsaufbau fehlgeschlagen *** //
		$verbindungs_error =  "'.mysqli_connect_errno().'('.mysqli_connect_errno().') <br>')";
		$verbindung_erfolgreich = "Der Verbindungsaufbau mit der Datenbank war erfolgreich. <br>";	 			// *** Meldung, wenn Verbindungsaufbau erfolgreich *** // 
				
		if (mysqli_connect_errno()) {								 // *** if-Abfrage, ob Verbindungsaufgabu erfolgreich *** //
			die ($verbindung_fehlgeschlagen.$verbindung_error);
			$weiter = false;
		}
		else { 
		};		
?>