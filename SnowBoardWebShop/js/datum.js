function datum () {
			
	var Jetzt = new Date();
	var Tag = Jetzt.getDate();
	var Monat = Jetzt.getMonth() + 1;   // Monatszählung beginnt bei 0, deshalb +1
	var Jahr = Jetzt.getFullYear();
	var Stunden = Jetzt.getHours();
	var Minuten = Jetzt.getMinutes();
	var Sekunden = Jetzt.getSeconds();
	var NachVoll = ((Minuten < 10) ? ":0" : ":");
					
	if (Tag < 10) {
		Tag = "0" + Tag;
	};
	
	if (Monat < 10){
		Monat = "0" + Monat;
	};
					
	document.getElementById("datum").innerHTML = Tag + "." + Monat + "." + Jahr;			
}
			
window.setInterval("datum()", 1000)