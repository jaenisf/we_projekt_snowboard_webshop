function uhrzeit () {
				
	var Jetzt = new Date();
	var Tag = Jetzt.getDate();
	var Monat = Jetzt.getMonth() + 1;   // Monatszählung beginnt bei 0, deshalb +1
	var Jahr = Jetzt.getFullYear();
	var Stunden = Jetzt.getHours();
	var Minuten = Jetzt.getMinutes();
	var Sekunden = Jetzt.getSeconds();
	var NachVoll = ((Minuten < 10) ? ":0" : ":");
					
	if (Stunden < 10) {
		Stunden = "0" +Stunden;
	}

 /* if (Minuten < 10){				werden schon so formatiert
		Minuten = "0" +Minuten;
	} */
	
	if (Sekunden < 10){
		Sekunden = "0" +Sekunden;
	}
				
	document.getElementById("uhrzeit").innerHTML = Stunden + NachVoll + Minuten + ":" + Sekunden;
				
}
				
window.setInterval("uhrzeit()", 1000)