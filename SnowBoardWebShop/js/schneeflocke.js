const canvas = document.getElementById('logo');
const ctx = canvas.getContext('2d');

const maxlevel = 3;
const branches = 3;
const spread = 0.9;
const sides = 6;
const length = canvas.width/3;

ctx.translate (canvas.width / 2, canvas.height / 2);
ctx.strokeStyle = '#fff';
ctx.lineWidth = 3;
const angle = Math.PI * 2 * spread;

function draw(level){
	if(level > maxlevel) return;
  
  ctx.beginPath();
  ctx.moveTo(0,0);
  ctx.lineTo(length,0);
  ctx.stroke();
   
   for( let i = 1; i < branches; i++){
    ctx.save();
    ctx.translate( length * i / ( branches + 1 ), 0);
    ctx.scale(0.5, 0.5);
    ctx.save();
    
    ctx.rotate(angle);
    draw(level + 1);
    ctx.restore();
    ctx.save();
    
    ctx.rotate( -angle);
    draw(level + 1);
    ctx.restore();
    
    ctx.restore();
   }
}

function logo() {

 ctx.clearRect(- canvas.width / 2, - canvas.height / 2 , canvas.width, canvas.height);
 ctx.rotate(-0.005);
 for(let j = 0; j < sides; j++){
    draw(0);
    ctx.rotate(Math.PI * 2 / sides);
  }
  
  window.requestAnimationFrame(logo);
}

function init() {
	window.requestAnimationFrame(logo)
}

init();